<?php
/*
 +=====================================================================+
 | NinjaFirewall (WP+ Edition)                                         |
 |                                                                     |
 | (c) NinTechNet - http://nintechnet.com/                             |
 +=====================================================================+ i18n+ / sa
*/

if (! defined( 'NFW_ENGINE_VERSION' ) ) { die( 'Forbidden' ); }

if ( ( is_multisite() ) && (! current_user_can( 'manage_network' ) ) ) {
	return;
}

// Set this to 1 if you don't want to receive a welcome email:
if (! defined('DONOTEMAIL') ) {
	define('DONOTEMAIL', 0);
}

// Force errors display during the installation:
@error_reporting(-1);
@ini_set('display_errors', '1');


if ( isset( $_POST["select_mode"] ) ) {
	if ( $_POST["select_mode"] == "wpwaf" ) {
		$_SESSION['waf_mode'] = "wpwaf";
	} elseif ( $_POST["select_mode"] == "fullwaf" ) {
		$_SESSION['waf_mode'] = "fullwaf";
	}
}
require( __DIR__ . '/lib/install_wpwaf.php' );
require( __DIR__ . '/lib/install_fullwaf.php' );


if ( empty( $_REQUEST['nfw_act'] ) ) {
	nfw_welcome();
	return;
}

if ( $_REQUEST['nfw_act'] == 'create_log_dir' ) {
	if ( empty($_POST['nfwnonce']) || ! wp_verify_nonce($_POST['nfwnonce'], 'create_log_dir') ) {
		wp_nonce_ays('create_log_dir');
	}
	nfw_create_log_dir();
	return;
}

if ( $_REQUEST['nfw_act'] == 'chk_license' ) {
	if ( empty($_POST['nfwnonce']) || ! wp_verify_nonce($_POST['nfwnonce'], 'chk_license') ) {
		wp_nonce_ays('chk_license');
	}
	nfw_chk_license();
	return;
}

/* ================================================================== */
// WordPress WAF mode:

if ( $_REQUEST['nfw_act'] == 'save_changes_wpwaf' ) {
	if ( empty($_POST['nfwnonce']) || ! wp_verify_nonce($_POST['nfwnonce'], 'save_changes_wpwaf') ) {
		wp_nonce_ays('save_changes_wpwaf');
	}
	nfw_save_changes_wpwaf();
	return;
}

/* ================================================================== */
// Full WAF mode:

if ( $_REQUEST['nfw_act'] == 'presave' ) {
	if ( empty($_POST['nfwnonce']) || ! wp_verify_nonce($_POST['nfwnonce'], 'presave') ) {
		wp_nonce_ays('presave');
	}
	nfw_presave();

} elseif ( $_REQUEST['nfw_act'] == 'integration' ) {
	if ( empty($_POST['nfwnonce']) || ! wp_verify_nonce($_POST['nfwnonce'], 'integration') ) {
		wp_nonce_ays('integration');
	}
	nfw_integration();

} elseif ( $_REQUEST['nfw_act'] == 'postsave' ) {
	if ( empty($_POST['nfwnonce']) || ! wp_verify_nonce($_POST['nfwnonce'], 'postsave') ) {
		wp_nonce_ays('postsave');
	}
	nfw_postsave();

}

return;

/* ================================================================== */

function nfw_welcome() {

	if ( isset($_SESSION['abspath']) ) {
		unset($_SESSION['abspath']);
	}
	if ( isset($_SESSION['http_server']) ) {
		unset($_SESSION['http_server']);
	}
	if ( isset($_SESSION['php_ini_type']) ) {
		unset($_SESSION['php_ini_type']);
	}
	if (isset($_SESSION['nfw_lic']) ) {
		unset($_SESSION['nfw_lic']);
	}
	if (isset($_SESSION['email_install']) ) {
		unset($_SESSION['email_install']);
	}
	if (isset($_SESSION['default_conf']) ) {
		unset($_SESSION['default_conf']);
	}
	if (isset($_SESSION['waf_mode']) ) {
		unset($_SESSION['waf_mode']);
	}
	if (isset($_SESSION['wp_config']) ) {
		unset($_SESSION['wp_config']);
	}

	$_SESSION['nfw_goodguy'] = true;

?>
<div class="wrap">
	<div style="width:33px;height:33px;background-image:url(<?php echo plugins_url() ?>/nfwplus/images/ninjafirewall_32.png);background-repeat:no-repeat;background-position:0 0;margin:7px 5px 0 0;float:left;"></div>
	<h1>NinjaFirewall (<font color=#21759B>WP+</font> Edition)</h1>
	<?php
		if (file_exists( dirname(plugin_dir_path(__FILE__)) . '/ninjafirewall') ) {
		echo '<br /><div class="error settings-error"><p>' . sprintf( __('Error: You have a copy of NinjaFirewall (%s) installed.<br />Please uninstall it completely before attempting to install NinjaFirewall (<font color=#21759B>WP+</font> Edition).', 'nfwplus'), 'WP Edition' ) . '</p></div></div></div></div></div></div></body></html>';
		exit;
	}
	?>

	<p><?php _e('Thank you for using NinjaFirewall', 'nfwplus') ?> (<font color=#21759B>WP+</font> Edition).  <?php _e('This installer will help you to make the setup process as quick and easy as possible.', 'nfwplus') ?></p>

	<p><?php _e('Although NinjaFirewall looks like a regular security plugin, it is not. It can be installed and configured from the WordPress admin console, but it is a stand-alone Web Application Firewall that sits in front of WordPress.', 'nfwplus') ?> <?php _e('It can run in two different modes: <b>Full WAF</b> or <b>WordPress WAF</b> modes.', 'nfwplus') ?></p>

	<h3><?php _e('Full WAF mode', 'nfwplus') ?></h3>

	<p><?php _e('In <b>Full WAF</b> mode, NinjaFirewall will hook, scan, reject or sanitise any HTTP and HTTPS request sent to a PHP script before it reaches WordPress, its plugins or even the database. All scripts located inside the blog installation directories and sub-directories will be protected, including those that aren\'t part of the WordPress package. Even encoded PHP scripts (e.g., ionCube), potential backdoors and shell scripts (e.g., c99, r57) will be filtered by NinjaFirewall.', 'nfwplus') ?>
	<br />
	<?php printf( __('That makes it a true firewall and gives you the highest possible level of protection: <a href="%s" title="%s">security without compromise</a>.', 'nfwplus'), 'https://blog.nintechnet.com/introduction-to-ninjafirewall-filtering-engine/', 'An introduction to NinjaFirewall filtering engine.') ?>
	<br />
	<?php printf( __('To run NinjaFirewall in <b>Full WAF</b> mode, your server must allow the use of the <code>auto_prepend_file</code> PHP directive. It is required to instruct the PHP interpreter to load the firewall before WordPress or any other script. Most of the time it works right out of the box, or may require <a href="%s" title="%s">some very little tweaks</a>. But in a few cases, mostly because of some shared hosting plans restrictions, it may simply not work at all.','nfwplus'), 'https://blog.nintechnet.com/troubleshoot-ninjafirewall-installation-problems/', 'Troubleshoot NinjaFirewall installation problems.') ?></p>

	<h3><?php _e('WordPress WAF mode', 'nfwplus') ?></h3>

	<p><?php _e('The <b>WordPress WAF</b> mode requires to load NinjaFirewall via the WordPress wp-config.php script. This process makes it easy to setup and the installation will always be successful, regardless of your hosting plan restrictions.', 'nfwplus') ?> <?php _e('NinjaFirewall will still load before WordPress, its plugins and the database and will run as fast as the <b>Full WAF</b> mode.', 'nfwplus') ?>
	<br />
	<?php _e('However, the downside of this mode is that NinjaFirewall will be able to hook and filter HTTP requests sent to WordPress only. A few features such as File Guard, the URL Access Control and Web Filter (WP+ Edition only) will be limited.', 'ninjafirewall') ?>
	<br />
	<?php _e('Despite being less powerful than the <b>Full WAF</b> mode, it still offers a level of protection and performance higher than any other security plugin.', 'nfwplus') ?></p>


	<h3><?php _e('Installation', 'nfwplus') ?></h3>

	<p><?php _e('We recommend to select the <b>Full WAF</b> mode option first. If it fails, this installer will let you switch to the <b>WordPress WAF</b> mode easily.', 'nfwplus' ) ?></p>

	<form method="post">

		<p><label><input type="radio" name="select_mode" value="fullwaf" checked="checked" /><strong><?php _e('Full WAF mode (recommended)', 'nfwplus') ?></strong></label></p>

		<p><label><input type="radio" name="select_mode" value="wpwaf" /><strong><?php _e('WordPress WAF mode', 'nfwplus') ?></strong></label></p>

		<p><input class="button-primary" type="submit" name="nextstep" value="<?php _e('Next Step', 'nfwplus') ?> &#187;" /></p>

		<input type="hidden" name="nfw_act" value="create_log_dir" />
		<?php wp_nonce_field('create_log_dir', 'nfwnonce', 0); ?>

	</form>
</div>
<?php

}

/* ================================================================== */

function nfw_create_log_dir() {

	// We need to create our log & cache folder in the wp-content
	// directory or return an error right away if we cannot :
	if (! is_writable(NFW_LOG_DIR) ) {
		$err = sprintf( __('NinjaFirewall cannot create its <code>nfwlog/</code>log and cache folder; please make sure that the <code>%s</code> directory is writable', 'nfwplus'), htmlspecialchars(NFW_LOG_DIR) );
	} else {
		if (! file_exists(NFW_LOG_DIR . '/nfwlog') ) {
			mkdir( NFW_LOG_DIR . '/nfwlog', 0755);
		}
		if (! file_exists(NFW_LOG_DIR . '/nfwlog/cache') ) {
			mkdir( NFW_LOG_DIR . '/nfwlog/cache', 0755);
		}

		$deny_rules = "<Files \"*\">
	<IfModule mod_version.c>
		<IfVersion < 2.4>
			Order Deny,Allow
			Deny from All
		</IfVersion>
		<IfVersion >= 2.4>
			Require all denied
		</IfVersion>
	</IfModule>
	<IfModule !mod_version.c>
		<IfModule !mod_authz_core.c>
			Order Deny,Allow
			Deny from All
		</IfModule>
		<IfModule mod_authz_core.c>
			Require all denied
		</IfModule>
	</IfModule>
</Files>";

		touch( NFW_LOG_DIR . '/nfwlog/index.html' );
		touch( NFW_LOG_DIR . '/nfwlog/cache/index.html' );
		@file_put_contents(NFW_LOG_DIR . '/nfwlog/.htaccess', $deny_rules, LOCK_EX);
		@file_put_contents(NFW_LOG_DIR . '/nfwlog/cache/.htaccess', $deny_rules, LOCK_EX);
		@file_put_contents(NFW_LOG_DIR . '/nfwlog/readme.txt', __("This is NinjaFirewall's logs, loader and cache directory. DO NOT alter or remove it as long as NinjaFirewall is running!", 'nfwplus'), LOCK_EX);

		// Return if we are going to run in "WordPress WAF" mode:
		if ( $_SESSION['waf_mode'] == "wpwaf" ) {
			if ( empty( $_SESSION['nfw_lic'] ) ) {
				nfw_license();
			} else {
				nfw_integration_wpwaf();
			}
			return;
		}

		// Firewall loader:
		$loader = "<?php
// ===============================================================//
// NinjaFirewall's loader.                                        //
// DO NOT alter or remove it as long as NinjaFirewall is running! //
// ===============================================================//
if ( file_exists('" . plugin_dir_path(__FILE__) . 'lib/firewall.php' . "') ) {
	@include('" . plugin_dir_path(__FILE__) . 'lib/firewall.php' . "');
}
// EOF
";
		file_put_contents(NFW_LOG_DIR . '/nfwlog/ninjafirewall.php', $loader, LOCK_EX);
	}
	if ( empty($err) ) {
		if ( empty( $_SESSION['nfw_lic'] ) ) {
			nfw_license();
		} else {
			nfw_get_abspath();
		}
		return;
	}
	echo '
<div class="wrap">
	<div style="width:33px;height:33px;background-image:url(' . plugins_url() . '/nfwplus/images/ninjafirewall_32.png);background-repeat:no-repeat;background-position:0 0;margin:7px 5px 0 0;float:left;"></div>
	<h1>NinjaFirewall (<font color=#21759B>WP+</font> Edition)</h1>
	<br />
	 <div class="error settings-error"><p>' . $err . '</p></div>

	<br />
	<br />
	<form method="post">
		<p><input class="button-primary" type="submit" name="Save" value="' . __('Try again', 'nfwplus') . ' &gt;&gt;" /></p>
		<input type="hidden" name="nfw_act" value="logdir" />'. wp_nonce_field('create_log_dir', 'nfwnonce', 0) .'
	</form>
</div>';

}

/* ================================================================== */

function welcome_email() {

	if ( empty($_SESSION['email_install']) ) {
		// We send an email to the admin (or super admin) with some details
		// about how to undo the changes if the site crashed after applying
		// those changes :
		if ( $recipient = get_option('admin_email') ) {
			$subject = '[NinjaFirewall] ' . __('Quick Start, FAQ & Troubleshooting Guide', 'nfwplus');
			$message = __('Hi,', 'nfwplus') . "\n\n";

			$message.= __('This is NinjaFirewall\'s installer. Below are some helpful info and links you may consider reading before using NinjaFirewall.', 'nfwplus') . "\n\n";

			$message.= '1) ' . __('Troubleshooting:', 'nfwplus') . "\n";
			$message.= 'http://nintechnet.com/ninjafirewall/wp-edition/help/?troubleshooting ' . "\n\n";

			$message.= __('-Locked out of your site / Fatal error / WordPress crash?', 'nfwplus') . "\n";
			$message.= __('-Failed installation ("Error: the firewall is not loaded")?', 'nfwplus') . "\n";
			$message.= __('-Blank page after INSTALLING NinjaFirewall?', 'nfwplus') . "\n";
			$message.= __('-Blank page after UNINSTALLING NinjaFirewall?', 'nfwplus') . "\n";
			$message.= __('-500 Internal Server Error?', 'nfwplus') . "\n";
			$message.= __('-"Cannot connect to WordPress database" error message?', 'nfwplus') . "\n";
			$message.= __('-How to disable NinjaFirewall?', 'nfwplus') . "\n";
			$message.= __('-Lost password (brute-force protection)?', 'nfwplus') . "\n";
			$message.= __('-Blocked visitors (see below)?', 'nfwplus') . "\n\n";

			$message.= '2) ' .__('-NinjaFirewall (WP+ Edition) troubleshooter script', 'nfwplus') . "\n";
			$message.= 'http://nintechnet.com/share/wp-check.txt ' . "\n\n";
			$message.=  __('-Rename this file to "wp-check.php".', 'nfwplus') . "\n";
			$message.=  __('-Upload it into your WordPress root folder.', 'nfwplus') . "\n";
			$message.=  __('-Goto http://YOUR WEBSITE/wp-check.php.', 'nfwplus') . "\n";
			$message.=  __('-Delete it afterwards.', 'nfwplus') . "\n\n";


			$message.= '3) '. __('FAQ:', 'nfwplus') . "\n";
			$message.= 'http://nintechnet.com/ninjafirewall/wp-edition/help/?faq ' . "\n\n";

			$message.= __('-Why is NinjaFirewall different from other security plugins for WordPress?', 'nfwplus') . "\n";
			$message.= __('-Do I need root privileges to install NinjaFirewall?', 'nfwplus') . "\n";
			$message.= __('-Does it work with Nginx?', 'nfwplus') . "\n";
			$message.= __('-Do I need to alter my PHP scripts?', 'nfwplus') . "\n";
			$message.= __('-Will NinjaFirewall detect the correct IP of my visitors if I am behind a CDN service like Cloudflare or Incapsula?', 'nfwplus') . "\n";
			$message.= __('-I moved my wp-config.php file to another directory. Will it work with NinjaFirewall?', 'nfwplus') . "\n";
			$message.= __('-Will it slow down my site?', 'nfwplus') . "\n";
			$message.= __('-Is there a Microsoft Windows version?', 'nfwplus') . "\n";
			$message.= __('-Can I add/write my own security rules?', 'nfwplus') . "\n";
			$message.= __('-Can I migrate my site(s) with NinjaFirewall installed?', 'nfwplus') . "\n\n";


			$message.= '4) '. __('Must Read:', 'nfwplus') . "\n\n";

			$message.= __('-An introduction to NinjaFirewall filtering engine:', 'nfwplus') . "\n";
			$message.= 'https://blog.nintechnet.com/introduction-to-ninjafirewall-filtering-engine/ ' . "\n\n";

			$message.= __('-Testing NinjaFirewall without blocking your visitors:', 'nfwplus') . "\n";
			$message.= 'https://blog.nintechnet.com/testing-ninjafirewall-without-blocking-your-visitors/ ' . "\n\n";

			$message.= __('-Add your own code to the firewall: the ".htninja" file:', 'nfwplus') . "\n";
			$message.= 'https://nintechnet.com/ninjafirewall/wp-edition/help/?htninja ' . "\n\n";

			$message.= __('-Restricting access to NinjaFirewall settings:', 'nfwplus') . "\n";
			$message.= 'https://blog.nintechnet.com/restricting-access-to-ninjafirewall-wp-edition-settings/ ' . "\n\n";

			$message.= __('-Upgrading to PHP 7 with NinjaFirewall installed:', 'nfwplus') . "\n";
			$message.= 'https://blog.nintechnet.com/upgrading-to-php-7-with-ninjafirewall-installed/ ' . "\n\n";

			$message.= __('-Keep your blog protected against the latest vulnerabilities:', 'nfwplus') . "\n";
			$message.= 'https://blog.nintechnet.com/ninjafirewall-wpwp-introduces-automatic-updates-for-security-rules ' . "\n\n";

			$message.= __('-NinjaFirewall Referral Program:', 'nfwplus') . "\n";
			$message.= 'https://nintechnet.com/referral/ ' . "\n\n";

			$message.= '5) '. __('Help & Support Links:', 'nfwplus') . "\n\n";

			$message.= __('-Each page of NinjaFirewall includes a contextual help: click on the "Help" menu tab located in the upper right corner of the corresponding page.', 'nfwplus') . "\n";
			$message.= __('-Online documentation is also available here:', 'nfwplus'). ' http://nintechnet.com/ninjafirewall/wp-edition/doc/ ' . "\n";
			$message.= __('-Updates info are available via Twitter:', 'nfwplus') .' https://twitter.com/nintechnet ' . "\n";
			$message.= __('-Support and Helpdesk:', 'nfwplus') .' https://nintechnet.com/helpdesk/ ' . "\n\n";

			$message.= 'NinjaFirewall (WP+ Edition) - http://ninjafirewall.com/ ' . "\n\n";

			if (! DONOTEMAIL ) {
				wp_mail( $recipient, $subject, $message );
				$_SESSION['email_install'] = $recipient;
			}
		}
	}
}

/* ================================================================== */

function nfw_firewalltest() {
	?>
<div class="wrap">
	<div style="width:33px;height:33px;background-image:url(<?php echo plugins_url() ?>/ninjafirewall/images/ninjafirewall_32.png);background-repeat:no-repeat;background-position:0 0;margin:7px 5px 0 0;float:left;"></div>
	<h1>NinjaFirewall (<font color=#21759B>WP+</font> Edition)</h1>

	<?php
	if (! defined('NFW_STATUS') || NFW_STATUS != 21 ) {
		// The firewall is not loaded:
		echo '<div class="error settings-error"><p>'. __('Error: The firewall is not loaded.', 'nfwplus'). '</p></div>
		<h3>'. __('Suggestions:', 'nfwplus'). '</h3>
		<ul>';
		if ($_SESSION['http_server'] == 1) {
			// User choosed Apache/mod_php instead of CGI/FCGI:
			echo '<li>&#8729; '. __('You selected <code>Apache + PHP module</code> as your HTTP server and PHP SAPI. Maybe your HTTP server is <code>Apache + CGI/FastCGI</code>?', 'nfwplus'). '
			<br />
			'. __('You can click the "Go Back" button and try to select another HTTP server type.', 'nfwplus'). '</li><br /><br />';

		} elseif( $_SESSION['http_server'] == 4 ) {
			// LiteSpeed "AllowOverride" issue?
			echo '<li>&#8729; '. __('You have selected LiteSpeed as your HTTP server. Did you enable the "AllowOverride" directive from its admin panel? Make sure it is enabled, restart LiteSpeed and then, click the "Test Again" button below.', 'ninjafirewall'). '</li>
				<form method="POST">
					<input type="submit" class="button-secondary" value="'. __('Test Again', 'ninjafirewall'). '" />
					<input type="hidden" name="nfw_act" value="postsave" />
					<input type="hidden" name="makechange" value="usr" />
					<input type="hidden" name="nfw_firstrun" value="1" />'. wp_nonce_field('postsave', 'nfwnonce', 0) .'
				</form><br />';

		} else {

			if ($_SESSION['php_ini_type'] == 2) {
				// Very likely a PHP INI issue:
				echo '<li>&#8729; '. __('You have selected <code>.user.ini</code> as your PHP initialization file. Unlike <code>php.ini</code>, <code>.user.ini</code> files are not reloaded immediately by PHP, but every five minutes. If this is your own server, restart Apache (or PHP-FPM if applicable) to force PHP to reload it, otherwise please <strong>wait up to five minutes</strong> and then, click the "Test Again" button below.', 'nfwplus'). '</li>
				<form method="POST">
					<input type="submit" class="button-secondary" value="'. __('Test Again', 'nfwplus'). '" />
					<input type="hidden" name="nfw_act" value="postsave" />
					<input type="hidden" name="makechange" value="usr" />
					<input type="hidden" name="nfw_firstrun" value="1" />'. wp_nonce_field('postsave', 'nfwnonce', 0) .'
				</form><br /><br />';
			}
			if ($_SESSION['http_server'] == 2) {
				if ( preg_match('/apache/i', PHP_SAPI) ) {
					// User choosed Apache/CGI instead of mod_php:
					echo '<li>&#8729; '. __('You selected <code>Apache + CGI/FastCGI</code> as your HTTP server and PHP SAPI. Maybe your HTTP server is <code>Apache + PHP module</code>?', 'nfwplus'). '
					<br />
					'. __('You can click the "Go Back" button and try to select another HTTP server type.', 'nfwplus'). '</li><br />';
				}
			}
			echo '<li>&#8729; '. __('Maybe you did not select the correct PHP INI ?', 'nfwplus'). '
			<br />
			'. __('You can click the "Go Back" button and try to select another one.', 'nfwplus'). '</li>';
		}
		// Reload the page?
		echo '<form method="POST">
		<p><input type="submit" class="button-secondary" value="&#171; '. __('Go Back', 'nfwplus'). '" /></p>
		<input type="hidden" name="abspath" value="' . $_SESSION['abspath'] . '" />
		<input type="hidden" name="nfw_act" value="presave" />
		<input type="hidden" name="nfw_firstrun" value="1" />'. wp_nonce_field('presave', 'nfwnonce', 0) .'
		</form>
		<br />
			<li>&#8729; '. sprintf( __('If none of the above suggestions work, you can still install NinjaFirewall in %s mode by clicking the button below. Setup is easy and will always work.', 'nfwplus'), '<a href="https://blog.nintechnet.com/full_waf-vs-wordpress_waf/">WordPress WAF</a>' ) . '</li>
				<form method="post">
					<input type="hidden" name="select_mode" value="wpwaf" />
					<input type="hidden" name="nfw_act" value="create_log_dir" />
					' . wp_nonce_field('create_log_dir', 'nfwnonce', 0) . '
					<p><input class="button-secondary" type="submit" name="nextstep" value="' . __('Switch to the WordPress WAF mode installer &#187;', 'nfwplus') . '" /></p>
				</form>
		</ul>
		<br />
		<h3>'. __('Need help? Check our blog:', 'nfwplus'). ' <a href="https://blog.nintechnet.com/troubleshoot-ninjafirewall-installation-problems/" target="_blank">Troubleshoot NinjaFirewall installation problems</a>.</h3>
</div>';
	}
}
/* ================================================================== */

function nfw_ini_data() {

	if (! defined('HTACCESS_BEGIN') ) {
		define( 'HTACCESS_BEGIN', '# BEGIN NinjaFirewall' );
		define( 'HTACCESS_DATA',  '<IfModule mod_php' . PHP_MAJOR_VERSION . '.c>' . "\n" .
									     '   php_value auto_prepend_file ' . NFW_LOG_DIR . '/nfwlog/ninjafirewall.php' . "\n" .
									     '</IfModule>');
		define( 'LITESPEED_DATA', 'php_value auto_prepend_file ' . NFW_LOG_DIR . '/nfwlog/ninjafirewall.php');
		define( 'SUPHP_DATA',     '<IfModule mod_suphp.c>' . "\n" .
									     '   suPHP_ConfigPath ' . rtrim($_SESSION['abspath'], '/') . "\n" .
									     '</IfModule>');
		define( 'HTACCESS_END',   '# END NinjaFirewall' );
		define( 'PHPINI_BEGIN',   '; BEGIN NinjaFirewall' );
		define( 'PHPINI_DATA',    'auto_prepend_file = ' . NFW_LOG_DIR . '/nfwlog/ninjafirewall.php' );
		define( 'PHPINI_END',     '; END NinjaFirewall' );
	}
}

/* ================================================================== */

function nfw_wpconfig_data() {

	if (! defined('WP_CONFIG_BEGIN') ) {
		define( 'WP_CONFIG_BEGIN', '// BEGIN NinjaFirewall' );
		define( 'WP_CONFIG_DATA',
			'if ( file_exists("' . plugin_dir_path( __FILE__ ) . 'lib/firewall.php' . '") && ! defined("NFW_STATUS") ) {' . "\n" .
			'   @include_once("' . plugin_dir_path( __FILE__ ) . 'lib/firewall.php' . '");' . "\n" .
			'   define("NFW_WPWAF", 1);' . "\n" .
			'}' );
		define( 'WP_CONFIG_END', '// END NinjaFirewall' );
	}

}

/* ================================================================== */

function nfw_default_conf() {

	$nfw_rules = array();

	// Populate our options :
	$nfw_options = array(
		// Options
		'logo'				=> plugins_url() . '/nfwplus/images/ninjafirewall_75.png',
		'enabled'			=> 1,
		'shmop'				=> 0,
		'ret_code'			=> 403,
		'blocked_msg'		=> base64_encode(NFW_DEFAULT_MSG),
		'debug'				=> 0,
		// Firewall Policies
		'scan_protocol'	=> 3,
		'uploads'			=> 0,
		'sanitise_fn'		=> 0,
		'upload_maxsize'	=> 1048576,
		'get_scan'			=> 1,
		'get_sanitise'		=> 0,
		'post_scan'			=> 1,
		'post_sanitise'	=> 0,
		'request_sanitise'=> 0,
		'cookies_scan'		=> 1,
		'cookies_sanitise'=> 0,
		'ua_scan'			=> 1,
		'ua_sanitise'		=> 1,
		'referer_scan'		=> 0,
		'referer_sanitise'=> 1,
		'referer_post'		=> 0,
		'no_host_ip'		=> 0,
		'allow_local_ip'	=> 0,
		'php_errors'		=> 1,
		'php_self'			=> 1,
		'php_path_t'		=> 1,
		'php_path_i'		=> 1,
		'wp_dir'				=> '/wp-admin/(?:css|images|includes|js)/|' .
									'/wp-includes/(?:(?:css|images|js(?!/tinymce/wp-tinymce\.php)|theme-compat)/|[^/]+\.php)|' .
									'/'. basename(WP_CONTENT_DIR) .'/(?:uploads|blogs\.dir)/',
		'no_post_themes'	=> 0,
		'force_ssl'			=> 0,
		'disallow_edit'	=> 0,
		'disallow_mods'	=> 0,
		'post_b64'			=> 1,
		'no_xmlrpc'			=> 0,
		// v1.7 :
		'no_xmlrpc_multi'	=> 0,
		// v3.3.2
		'no_xmlrpc_pingback'=> 0,

		'enum_archives'	=> 0,
		'enum_login'		=> 0,
		// Notifications
		'a_0' 				=> 1,
		'a_11' 				=> 1,
		'a_12' 				=> 1,
		'a_13' 				=> 0,
		'a_14' 				=> 0,
		'a_15' 				=> 1,
		'a_16' 				=> 0,
		'a_21' 				=> 1,
		'a_22' 				=> 1,
		'a_23' 				=> 0,
		'a_24' 				=> 0,
		'a_31' 				=> 1,
		// v1.1.3:
		'a_41' 				=> 1,
		// v1.1.4:
		'a_51' 				=> 1,
		'sched_scan'		=> 0,
		'report_scan'		=> 0,
		// v1.7 (daily report cronjob):
		'a_52' 				=> 1,
		// v3.4:
		'a_53' 				=> 1,

		'alert_email'	 	=> get_option('admin_email'),
		'alert_sa_only'	=> 1,
		// Network
		'nt_show_status'	=> 1,
		// Access control
		'ac_role'			=> '10000',
		'ac_ip'				=> 1,
		'ac_ip_2'			=> 0,
		'allow_local_ip'	=> 0,
		'ac_method'			=> 'GETPOSTHEADPUTDELETEPATCH',
		'ac_geoip'			=> 0,
		'ac_geoip_db'		=> 1,
		'ac_geoip_db2'		=> '',
		'ac_geoip_cn'		=> '',
		'ac_geo_url'		=>	'',
		'ac_geoip_ninja'	=> 0,
		'ac_allow_ip'		=> 0,
		'ac_block_ip'		=> 0,
		'ac_rl_on'			=> 0,
		'ac_rl_conn'		=> 10,
		'ac_rl_time'		=> 30,
		'ac_rl_intv'		=> 5,
		'ac_bl_url'			=> 0,
		'ac_wl_url' 		=> 0,
		'ac_bl_bot'			=> NFW_BOT_LIST,
		'ac_geoip_log'		=> 1,
		'ac_allow_ip_log'	=> 0,
		'ac_block_ip_log'	=> 1,
		'ac_rl_log'			=> 1,
		'ac_bl_url_log'	=> 1,
		'ac_bl_bot_log'	=> 1,
		'ac_wl_url_log' 	=> 0,
		// Web filter
		'wf_enable'			=> 0,
		'wf_pattern' 		=> 0,
		'wf_case'			=> 0,
		'wf_alert'			=> '30',
		'wf_attach'			=> 1,
		// Anti-spam
		'as_enable'			=> 0,
		'as_level'			=> 1,
		'as_comment'		=> 1,
		'as_register'		=> 0,
		'as_salt'			=> wp_generate_password(),
		'as_field'			=> wp_generate_password( mt_rand(5, 12), FALSE),
		'as_field_2'		=> wp_generate_password( mt_rand(5, 12), FALSE),
		// Log
		'logging'			=> 1,
		'log_rotate'		=> 1,
		'log_maxsize'		=> 2 * 1048576,
		'log_line'			=>	1500,
		// v1.0.2
		// File Guard :
		'fg_enable'			=>	0,
		'fg_mtime'			=>	10,
		'fg_exclude'		=>	'',
		// v3.2
		// Anti-Malware :
		'malware_dir'		=> htmlspecialchars( rtrim( ABSPATH, '/\\ ' ) ),
		'malware_symlink'	=> 1,
		'malware_timestamp'	=> 7,
		'malware_size'		=> 2048,
		// Updates :
		'enable_updates'	=>	1,
		'sched_updates'	=>	1,
		'notify_updates'	=>	1,
		// v3.3
		// Centralized Logging:
		'clogs_enable'		=>	0,
		'clogs_pubkey'		=>	'',
	);
	// v1.1.1 :
	// Some compatibility checks:
	// 1. header_register_callback(): requires PHP >=5.4
	// 2. headers_list() and header_remove(): some hosts may disable them.
	if ( function_exists('header_register_callback') && function_exists('headers_list') && function_exists('header_remove') ) {
		// We enable X-XSS-Protection:
		$nfw_options['response_headers'] = '00010000';
	}

	// Fetch the latest rules from the WordPress repo :
	define('NFUPDATESDO', 2);
	@nf_sub_updates();

	if (! $nfw_rules = @unserialize(NFW_RULES) ) {
		$err_msg = '<p><strong>'. __('Error: The installer cannot download the security rules from wordpress.org website.', 'nfwplus') . '</strong></p>';
		$err_msg.= '<ol><li>'. __('The server may be temporarily down or you may have network connectivity problems? Please try again in a few minutes.', 'nfwplus') . '</li>';
		$err_msg.= '<li>'. __('NinjaFirewall downloads its rules over an HTTPS secure connection. Maybe your server does not support SSL? You can force NinjaFirewall to use a non-secure HTTP connection by adding the following directive to your <strong>wp-config.php</strong> file:', 'nfwplus') . '<p><code>define("NFW_DONT_USE_SSL", 1);</code></p></li></ol>';
		exit("<br /><div class='error notice is-dismissible'>{$err_msg}</div></div></div></div></div></body></html>");
	}

	// Save engine and rules versions :
	$nfw_options['engine_version'] = NFW_ENGINE_VERSION;
	$nfw_options['rules_version']  = NFW_NEWRULES_VERSION; // downloaded rules

	// Add the correct DOCUMENT_ROOT :
	if ( strlen( $_SERVER['DOCUMENT_ROOT'] ) > 5 ) {
		$nfw_rules[NFW_DOC_ROOT]['cha'][1]['wha'] = str_replace( '/', '/[./]*', $_SERVER['DOCUMENT_ROOT'] );
	} elseif ( strlen( getenv( 'DOCUMENT_ROOT' ) ) > 5 ) {
		$nfw_rules[NFW_DOC_ROOT]['cha'][1]['wha'] = str_replace( '/', '/[./]*', getenv( 'DOCUMENT_ROOT' ) );
	} else {
		$nfw_rules[NFW_DOC_ROOT]['ena']  = 0;
	}

	list( $nfw_options['lic'], $nfw_options['lic_exp'] ) = explode('::', $_SESSION['nfw_lic']);

	// The WP+ Edition does not need rule #531 :
	if ( isset($nfw_rules[531])) {
		unset($nfw_rules[531]);
	}

	// Save to the DB :
	nfw_update_option( 'nfw_options', $nfw_options);
	nfw_update_option( 'nfw_rules', $nfw_rules);

	// Remove any potential scheduled cron job (in case of a re-installation) :
	if ( wp_next_scheduled( 'nfwgccron' ) ) {
		wp_clear_scheduled_hook( 'nfwgccron' );
	}
	if ( wp_next_scheduled('nfscanevent') ) {
		wp_clear_scheduled_hook('nfscanevent');
	}
	if ( wp_next_scheduled('nfsecupdates') ) {
		wp_clear_scheduled_hook('nfsecupdates');
	}
	// Clear old daily report...
	if ( wp_next_scheduled('nfdailyreport') ) {
		wp_clear_scheduled_hook('nfdailyreport');
	}
	// and recreate a new one by default :
	nfw_get_blogtimezone();
	wp_schedule_event( strtotime( date('Y-m-d 00:00:05', strtotime("+1 day")) ), 'daily', 'nfdailyreport');
	// Add security rules updates cron:
	wp_schedule_event( time() + 3600, 'hourly', 'nfsecupdates');
	// Garbage collector:
	wp_schedule_event( time() + 1800, 'hourly', 'nfwgccron' );

	$_SESSION['default_conf'] = 1;

}

/* ================================================================== */

function nfw_license( $err = '' ) {

	echo '<script>function chckfld(){
if (!document.lic_check.new_lic.value){alert("'. esc_js( __( 'Please enter your license.', 'nfwplus' ) ) .'");document.lic_check.new_lic.focus();return false;}}</script>
<div class="wrap">
	<div style="width:33px;height:33px;background-image:url(' . plugins_url() . '/nfwplus/images/ninjafirewall_32.png);background-repeat:no-repeat;background-position:0 0;margin:7px 5px 0 0;float:left;"></div>
	<h1>NinjaFirewall (<font color=#21759B>WP+</font> Edition)</h1>
	<br />';
	if ( $err ) {
		echo '<div class="error settings-error"><p><strong>'. __('Error:', 'nfwplus') .' </strong>' . $err . '.
		<br />' .
		sprintf( __('Please <a href="%s">contact our support</a> with the above error message.', 'nfwplus'), 'https://nintechnet.com/helpdesk/' ) . '</p></div>';
	}
	echo '<br />
	<h3>'. __('License', 'nfwplus') .'</h3>
	<form method="post" name="lic_check" onSubmit="return chckfld();">
	<table class="form-table">
		<tr>
			<th scope="row">'. __('Enter your license and click on the save button', 'nfwplus') .'</th>
			<td width="20">&nbsp;</td>
			<td align="left">
				<input type="text" autocomplete="off" value="" maxlength="500" size="50" name="new_lic">
				<p>'. __('Don\'t have a license yet?', 'nfwplus') .' <a href="http://nintechnet.com/ninjafirewall/wp-edition/" target="_blank">'. __('Click here to get one', 'nfwplus') .'</a>.</p>
			</td>
		</tr>
	</table>
	<p><input class="button-primary" type="submit" name="Save" value="'. __('Save License', 'nfwplus') .' »" /></p>
	<input type="hidden" name="nfw_act" value="chk_license" />'. wp_nonce_field('chk_license', 'nfwnonce', 0) .'
	</form>
</div>';

}
/* ================================================================== */

function nfw_chk_license() {

	if (empty($_POST['new_lic']) ) {
		nfw_license( __('Please enter your license', 'nfwplus') );
		exit;
	}
	// Use stripslashes() to prevent WordPress from escaping the variable:
	$_POST['new_lic'] = stripslashes( trim( $_POST['new_lic']) );

	$nfw_res['exp'] = "2020-01-01";
	$_SESSION['nfw_lic'] = $_POST['new_lic'] . '::' . $nfw_res['exp'];
	if ( $_SESSION['waf_mode'] == "fullwaf" ) {
		nfw_get_abspath();
	} else {
		nfw_integration_wpwaf();
	}
	return;
	
	
	if ( is_multisite() ) {
		$nfw_site_url = rtrim( strtolower( network_site_url('','http')), '/' );
	} else {
		$nfw_site_url = rtrim( strtolower(site_url('','http')), '/' );
	}
	global $wp_version;

	$request_string = array(
		'body' 	=> array(
			'action' => 'checklicense',
			'host'	=> strtolower( $_SERVER['HTTP_HOST'] ),
			'name'	=> strtolower( $_SERVER['SERVER_NAME'] ),
			'lic' 	=> $_POST['new_lic'],
			'ver'		=> NFW_ENGINE_VERSION
		),
		'user-agent' => 'WordPress/' . $wp_version . '; ' . $nfw_site_url
	);

	if ( defined( 'NFW_DONT_USE_SSL' ) ) {
		$proto = "http";
	} else {
		$proto = "https";
	}
	$res = wp_remote_post( "{$proto}://wordpress.nintechnet.com/index.php", $request_string);

	if (! is_wp_error($res) ) {
		if ( $res['response']['code'] == 200 ) {
			$nfw_res = unserialize( $res['body'] );
			if (! empty($nfw_res['exp']) && preg_match('/^\d{4}-\d{2}-\d{2}$/', $nfw_res['exp']) ) {
				if ( $nfw_res['exp'] < date('Y-m-d', strtotime("-1 day")) ){
					nfw_license( __('This license has expired and is no longer valid.', 'nfwplus') );
					exit;
				}
			} elseif (! empty($nfw_res['ret']) ) {
				if ( $nfw_res['ret'] > 9 && $nfw_res['ret'] < 20 ) {
					nfw_license( __('Your license is not valid', 'nfwplus'). ' (#'. htmlspecialchars( $nfw_res['ret'] ) . ')' );
					exit;
				} else {
					nfw_license( __('An unknown error occurred while connecting to NinjaFirewall servers. Please try again in a few minutes', 'nfwplus'). '.');
					exit;
				}
			} else {
				nfw_license( __('An error occurred while connecting to NinjaFirewall servers. Please try again in a few minutes', 'nfwplus'). ' (#1)');
				exit;
			}
		} else {
			nfw_license( __('An error occurred while connecting to NinjaFirewall servers. Please try again in a few minutes', 'nfwplus'). ' (#2)');
			exit;
		}
	} else {
		nfw_license( __('An error occurred while connecting to NinjaFirewall servers. Please try again in a few minutes', 'nfwplus'). ' (#3)');
		exit;
	}
	$_SESSION['nfw_lic'] = $_POST['new_lic'] . '::' . $nfw_res['exp'];

	if ( $_SESSION['waf_mode'] == "fullwaf" ) {
		nfw_get_abspath();
	} else {
		nfw_integration_wpwaf();
	}

}

/* ================================================================== */
// EOF //
