<?php
/*
 +=====================================================================+
 | NinjaFirewall (WP+ Edition)                                         |
 |                                                                     |
 | (c) NinTechNet - http://nintechnet.com/                             |
 +=====================================================================+ i18n+ / sa
*/

if (! defined( 'NFW_ENGINE_VERSION' ) ) { die( 'Forbidden' ); }

// Block immediately if user is not allowed :
nf_not_allowed( 'block', __LINE__ );

$nfw_options = nfw_get_option( 'nfw_options' );

echo '<script>
function removeOptions(box) {
	selectbox = document.nfwwebfilter.elements[box+"_out[]"];
	if (! selectbox.options.length) {
		alert("' . esc_js( __('Your list is empty.', 'nfwplus') ) . '");
		return false;
	}
	var i; var num=0;
	for ( i = selectbox.options.length-1; i>=0; i--) {
		if (selectbox.options[i].selected) {
			selectbox.remove(i);
			num++;
		}
	}
	if (! num) {
		alert("' . esc_js( __('Select one or more value to delete.', 'nfwplus') ) . '");
	}
}
function addOption( selectbox, value, box) {
	if (! value.value) {
		alert("' . esc_js( __('Enter a value!', 'nfwplus') ) . '");
		value.focus();
		return false;
	}
	selectbox = document.nfwwebfilter.elements[box+"_out[]"];
	for ( i = selectbox.options.length-1; i >= 0; i-- ) {
		if ( selectbox.options[i].value == value.value ) {
			alert("[" + selectbox.options[i].value + "] ' .
			esc_js( __('is already in your list.', 'nfwplus') ) . '");
			value.select();
			return false;
		}
	}
	if ( box == "strings" ) {
		if ( value.value.length < 4 || value.value.length > 150 ) {
			alert("' .
			esc_js( __('The length of the string must be between 4 to 150 characters.', 'nfwplus') ) . '");
			value.focus();
			value.select();
			return false;
		}
		invalid = new RegExp (/\|/);
		if (value.value.match(invalid)) {
			alert("' .
			 esc_js( __('The \'|\' character is not allowed.', 'nfwplus') ) . '");
			value.focus();
			value.select();
			return false;
		}
	}
	var optn = document.createElement("OPTION");
	optn.text = value.value;
	optn.value = value.value;
	optn.title = value.value;
	selectbox.options.add(optn, selectbox.options[0]);
	value.value = "";
	value.focus();
}
function toogle_table(off) {
	if ( off == 1 ) {
		jQuery("#table-div").slideDown(500);
	} else if ( off == 2 ) {
		jQuery("#table-div").slideUp(500);
	}
	return;
}
function check_fields() {
	// select all option lists
	var a = 0;
	for ( i = document.nfwwebfilter.elements["strings_out[]"].options.length-1; i >= 0; i-- ) {
		document.nfwwebfilter.elements["strings_out[]"].options[i].selected = true;
		a++;
	}
	if (a==0 && document.getElementById("wfenable").checked) {
		alert("' .
		esc_js( __('Enter at least one keyword or disable the Web Filter.', 'nfwplus') ) . '");
		document.nfwwebfilter.strings_in.focus();
		return false;
	}
	return true;
}
function kw_valid() {
	var e = document.nfwwebfilter.strings_in;
	if ( e.value.match(/[^\x20-\x7e\x80-\xff]/) ) {
		alert("' . esc_js( __('Invalid character.', 'nfwplus') ) . '");
		e.value = e.value.replace(/[^\x20-\x7e\x80-\xff]/g,"");
		return false;
	}
}
</script>
<div class="wrap">
	<div style="width:33px;height:33px;background-image:url( ' . plugins_url() . '/nfwplus/images/ninjafirewall_32.png);background-repeat:no-repeat;background-position:0 0;margin:7px 5px 0 0;float:left;"></div>
	<h1>'. __('Web Filter', 'nfwplus'). '</h1>';

// Saved ?
if ( isset( $_POST['nfw_options']) ) {
	if ( empty($_POST['nfwnonce']) || ! wp_verify_nonce($_POST['nfwnonce'], 'webfilter_save') ) {
		wp_nonce_ays('webfilter_save');
	}
	nf_sub_webfilter_save();
	$nfw_options = nfw_get_option( 'nfw_options' );
	echo '<div class="updated notice is-dismissible"><p>'. __('Your changes have been saved.', 'nfwplus'). '</p></div>';
}

if ( empty($nfw_options['wf_enable']) ) {
	$nfw_options['wf_enable'] = 0;
} else {
	$nfw_options['wf_enable'] = 1;
}
if ( empty($nfw_options['wf_case']) ) {
	$nfw_options['wf_case'] = 0;
} else {
	$nfw_options['wf_case'] = 1;
}
if ( empty($nfw_options['wf_alert']) || ! preg_match('/^(1?5|30|60|180|360|720|1440)$/', $nfw_options['wf_alert']) ) {
	$nfw_options['wf_alert'] = 30;
}
if ( empty($nfw_options['wf_attach']) ) {
	$nfw_options['wf_attach'] = 0;
} else {
	$nfw_options['wf_attach'] = 1;
}

if ( defined('NFW_WPWAF') ) {
	?>
	<div class="notice-warning notice is-dismissible">
		<p><?php printf( __('You are running NinjaFirewall in <i>WordPress WAF</i> mode. The %s feature will be limited to a few WordPress files only (e.g., index.php, wp-login.php, xmlrpc.php, admin-ajax.php, wp-load.php etc). If you want it to apply to any PHP script, you will need to run NinjaFirewall in %s mode.', 'nfwplus'), 'Web Filter', '<a href="https://blog.nintechnet.com/full_waf-vs-wordpress_waf/">Full WAF</a>') ?></p>
	</div>
	<?php
}

?>
<br />
<form method="post" name="nfwwebfilter" onSubmit="return check_fields();">
	<?php wp_nonce_field('webfilter_save', 'nfwnonce', 0); ?>
	<table class="form-table">
		<tr style="background-color:#F9F9F9;border: solid 1px #DFDFDF;">
			<th scope="row"><?php _e('Enable Web Filter', 'nfwplus') ?></th>
			<td align="left">
			<label><input type="radio" id="wfenable" name="nfw_options[wf_enable]" value="1"<?php checked($nfw_options['wf_enable'], 1) ?> onclick="toogle_table(1);">&nbsp;<?php _e('Yes', 'nfwplus') ?></label>
			</td>
			<td align="left">
			<label><input type="radio" name="nfw_options[wf_enable]" value="0"<?php checked($nfw_options['wf_enable'], 0) ?> onclick="toogle_table(2);">&nbsp;<?php _e('No (default)', 'nfwplus') ?></label>
			</td>
		</tr>
	</table>

	<br />
<div id="table-div"<?php echo $nfw_options['wf_enable'] == 1 ? '' : ' style="display:none"' ?>>
	<table class="form-table" id="wf_table">
		<tr valign="top">
			<th scope="row"><?php _e('Search HTML page for keywords', 'nfwplus') ?></th>
			<td width="20">&nbsp;</td>
			<td align="left" style="vertical-align:top;">
				<input type="text" id="substring" name="strings_in" value="" maxlength="150" class="small-text code" style="width:200px;" placeholder="<?php _e('e.g.', 'nfwplus') ?> Viagra <?php _e('or', 'nfwplus') ?> &lt;iframe" onkeyup="kw_valid();" />
				<br />
				<span class="description"><?php _e('Min. 4, max. 150 characters.', 'nfwplus') ?></span>
				<br /><br /><br />
				<select name="nfw_options[wf_strings]" style="width:200px" onChange="document.getElementById('substring').value=this.value;">
					<option value=""><?php _e('Suggested keywords...', 'nfwplus') ?></option>
					<optgroup label="<?php _e('=== HTML / CSS', 'nfwplus') ?>"></optgroup>
					<option value="<iframe" title="<iframe">&lt;iframe</option>
					<option value="display:none" title="display:none">display:none</option>
					<option value="'hidden'" title="'hidden'">'hidden'</option>
					<option value='http-equiv="refresh"' title='http-equiv="refresh"'>http-equiv="refresh"</option>
					<option value="style.display" title="style.display">style.display</option>
					<option value="multipart/form-data" title="multipart/form-data">multipart/form-data</option>
					<optgroup label="<?php _e('=== JAVASCRIPT', 'nfwplus') ?>"></optgroup>
					<?php
					echo '
					<option value="%u'.'00" title="%u'.'00">%u'.'00</option>
					<option value="\u'.'00" title="\u'.'00">\u'.'00</option>
					<option value=".appendChild" title=".appendChild">.appendChild</option>
					<option value="ActiveX'.'Object" title="Active'.'XObject">Act'.'iveXObject</option>
					<option value="encodeURIComponent" title="encodeURIComponent">encodeURIComponent</option>
					<option value="ev'.'al(" title="eva'.'l(">ev'.'al(</option>
					<option value=".replace" title=".replace">.replace</option>
					<option value="unescape" title="unescape">unescape</option>
					';
					?>
					<optgroup label="<?php _e('=== ERRORS', 'nfwplus') ?>"></optgroup>
					<option value="Fatal error:" title="Fatal error:">Fatal error:</option>
					<option value="Parse error:" title="Parse error:">Parse error:</option>
					<option value="<title>404 Not Found" title="<title>404 Not Found">&lt;title>404 Not Found</option>
					<option value="You have an error in your SQL syntax" title="You have an error in your SQL syntax">You have an error in your SQL syntax</option>
					<optgroup label="<?php _e('=== SHELL SCRIPTS', 'nfwplus') ?>"></optgroup>
					<option value="<?php echo htmlspecialchars($_SERVER["DOCUMENT_ROOT"]) ?>" title="<?php echo htmlspecialchars($_SERVER["DOCUMENT_ROOT"]) ?>"><?php echo htmlspecialchars($_SERVER["DOCUMENT_ROOT"]) ?></option>
					<?php
					echo '
					<option value="Hac'.'ked by" title="Hac'.'ked by">Ha'.'cked by</option>
					<option value="<title>php'.'info()" title="<title>ph'.'pinfo()">&lt;title>php'.'info()</option>
					<option value="Directory List" title="Directory List">Directory List</option>
					<option value="FTP brute" title="FTP brute">FTP brute</option>
					<option value="Run command" title="Run command">Run command</option>
					<option value="Dump database" title="Dump database">Dump database</option>
					<option value="File'.'sMan" title="File'.'sMan">File'.'sMan</option>
					<option value="Self remove" title="Self remove">Self remove</option>
					<option value="una'.'me -a" title="una'.'me -a">unam'.'e -a</option>
					<option value="c99mads'.'hell" title="c99m'.'adshell">c99m'.'adshell</option>
					<option value="r57'.'shell" title="r57'.'shell">r57sh'.'ell</option>
					<option value="c99s'.'hell" title="c99'.'shell">c99s'.'hell</option>
					<option value="Open_basedir" title="Open_basedir">Open_basedir</option>
					<option value="phpMiniA'.'dmin" title="phpMi'.'niAdmin">phpM'.'iniAdmin</option>
					<option value="<title>Login - Adm'.'iner" title="<title>Login - Ad'.'miner">&lt;title>Login - Adm'.'iner</option>
					';
					?>
				</select>
				<br />
				<br />
				<br />
				<label><input type="radio" id="wf_case_off" name="nfw_options[wf_case]" value="0" <?php checked($nfw_options['wf_case'], 0)?> />&nbsp;<?php _e('Case sensitive (default)', 'nfwplus') ?></label>
				<br />
				<label><input type="radio" name="nfw_options[wf_case]" value="1" <?php checked($nfw_options['wf_case'], 1)?> />&nbsp;<?php _e('Case insensitive', 'nfwplus') ?></label>
			</td>
			<td align="left" style="vertical-align:top;">
				<input type="button" class="button-secondary" value="<?php _e('Add', 'nfwplus') ?> &#187;" onclick="addOption(this.form.strings_out, this.form.strings_in, 'strings')" />
				<br /><br />
				<input type="button" class="button-secondary" value="&#171; <?php _e('Remove', 'nfwplus') ?>" onclick="removeOptions('strings')" />
			</td>
			<td align="left" style="vertical-align:top;">
			<?php _e('Keywords to search:', 'nfwplus') ?>
			<br />
			<select multiple="multiple" size="8" class="small-text code" name="strings_out[]" style="width:180px;height:200px;">
			<?php
			if (empty($nfw_options['wf_pattern']) ) {
				$nfw_options['wf_pattern'] = '';
			} else {
				$strings =  explode('|',  preg_replace( '/\\\([`.\\\+*?\[^\]$(){}=!<>|:-])/', '$1',  $nfw_options['wf_pattern'] ));
				if ( $strings ) {
					sort( $strings );
					foreach ($strings as $string) {
						if ( $string ) {
							echo '<option title="' . htmlspecialchars( $string ) . '" value="' . htmlspecialchars($string) . '">' . htmlspecialchars( $string ) . '</option>';
						}
					}
				}
			}
			?>
			</select>
			</td>
		</tr>
	</table>
	<br />
	<table class="form-table" id="wf_table2">
		<tr valign="top">
			<th scope="row"><?php _e('Email Alerts', 'nfwplus') ?></th>
			<td width="20">&nbsp;</td>
			<td align="left" style="vertical-align:middle;">
			<?php
				printf(
					__('Do not send me more than one email alert in a %s interval', 'nfwplus'),
				'<select name="nfw_options[wf_alert]">
					<option value="5"'. selected($nfw_options['wf_alert'], 5, 0) .'>'. __('5-minute', 'nfwplus') .'</option>
					<option value="15"'. selected($nfw_options['wf_alert'], 15, 0) .'>'. __('15-minute', 'nfwplus') .'</option>
					<option value="30"'. selected($nfw_options['wf_alert'], 30, 0) .'>'. __('30-minute', 'nfwplus') .'</option>
					<option value="60"'. selected($nfw_options['wf_alert'], 60, 0) .'>'. __('1-hour', 'nfwplus') .'</option>
					<option value="180"'. selected($nfw_options['wf_alert'],180, 0) .'>'. __('3-hour', 'nfwplus') .'</option>
					<option value="360"'. selected($nfw_options['wf_alert'], 360, 0) .'>'. __('6-hour', 'nfwplus') .'</option>
					<option value="720"'. selected($nfw_options['wf_alert'], 720, 0) .'>'. __('12-hour', 'nfwplus') .'</option>
					<option value="1440"'. selected($nfw_options['wf_alert'], 1440, 0) .'>'. __('24-hour', 'nfwplus') .'</option>
				</select>' );
			?>
			<br />
			<span class="description"><?php _e('Clicking the "Save Web Filter options" button below will reset the current timer.', 'nfwplus') ?></span>
			<br />
			<br />
			<label><input type="checkbox" name="nfw_options[wf_attach]" <?php checked($nfw_options['wf_attach'], 1); ?>>&nbsp;<?php _e('Attach the HTML page output to the email alert.', 'nfwplus') ?></label>
			</td>
		</tr>
	</table>
	<br />
	<input class="button-primary" type="submit" name="Save" value="<?php _e('Save Web Filter options', 'nfwplus') ?>" />
</form>

</div>
<?php

/* ================================================================== */

function nf_sub_webfilter_save() {

	global $nfw_options;

	// Disable or enable the web filter ?
	if ( empty( $_POST['nfw_options']['wf_enable']) ) {
		$nfw_options['wf_enable'] = 0;
	} else {
		$nfw_options['wf_enable'] = 1;
	}

	// Strings to search for :
	$tmp_pattern = '';
	if (! empty( $_POST['strings_out'] ) ) {
		@sort( $_POST['strings_out'] );
		foreach ( $_POST['strings_out'] as $pattern ) {
			if ( strlen($pattern) < 4 || strlen($pattern) > 150 ) {
				continue;
			}
			if (preg_match('/\|/', $pattern) ) {
				continue;
			}
			// Prevent WordPress from adding slashes :
			$tmp_pattern .= stripslashes( htmlspecialchars_decode( $pattern ) ). '|';
		}
		$nfw_options['wf_pattern'] = rtrim($tmp_pattern, '|' );
	} else {
		$nfw_options['wf_pattern'] = 0;
	}

	// Cases :
	if ( empty( $_POST['nfw_options']['wf_case']) ) {
		// Default
		$nfw_options['wf_case'] = 0;
	} else {
		$nfw_options['wf_case'] = 1;
	}

	// Alert throttling :
	if ( empty($_POST['nfw_options']['wf_alert']) || ! preg_match('/^(1?5|30|60|180|360|720|1440)$/', $_POST['nfw_options']['wf_alert']) ) {
		$nfw_options['wf_alert'] = 30;
	} else {
		$nfw_options['wf_alert'] = $_POST['nfw_options']['wf_alert'];
	}

	// Attachment :
	if ( isset( $_POST['nfw_options']['wf_attach']) ) {
		$nfw_options['wf_attach'] = 1;
	} else {
		$nfw_options['wf_attach'] = 0;
	}

	// Clear alert timer :
	if ( file_exists( NFW_LOG_DIR . '/nfwlog/cache/wf_timer.php') ) {
		unlink( NFW_LOG_DIR . '/nfwlog/cache/wf_timer.php');
	}

	// Save the path to wp-content dir for the firewall nfw_webfilter() function :
	if ( defined('NFW_LOG_DIR') ) {
		$nfw_options['wp_content'] = NFW_LOG_DIR;
	} else {
		$nfw_options['wp_content'] = WP_CONTENT_DIR;
	}

	// Update :
	nfw_update_option( 'nfw_options', $nfw_options );

}
/* ================================================================== */
// EOF
