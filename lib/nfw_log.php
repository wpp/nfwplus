<?php
/*
 +=====================================================================+
 | NinjaFirewall (WP+ Edition)                                         |
 |                                                                     |
 | (c) NinTechNet - http://nintechnet.com/                             |
 +=====================================================================+
 | REVISION: 2016-09-01 17:39:01                                       |
 +=====================================================================+ i18n- / sa
*/

if (! defined( 'NFW_ENGINE_VERSION' ) ) { die( 'Forbidden' ); }

$nfw_options = nfw_get_option( 'nfw_options' );

if (! empty($nfw_options['debug']) ) {
	$num_incident = '0000000';
	$loglevel = 7;
	$http_ret_code = '200';
// Create a random incident number :
} else {
	$num_incident = mt_rand(1000000, 9000000);
	$http_ret_code = $nfw_options['ret_code'];
}

// Return if logging is disabled :
if (empty($nfw_options['logging']) ) {
	return;
}

if (strlen($logdata) > 200) { $logdata = mb_substr($logdata, 0, 200, 'utf-8') . '...'; }
$res = '';
$string = str_split($logdata);
foreach ( $string as $char ) {
	// Allow only ASCII printable characters :
	if ( ( ord($char) < 32 ) || ( ord($char) > 126 ) ) {
		$res .= '%' . bin2hex($char);
	} else {
		$res .= $char;
	}
}
nfw_get_blogtimezone();

$cur_month = date('Y-m');
$log_dir = NFW_LOG_DIR . '/nfwlog/';
$stat_file = $log_dir . 'stats_' . $cur_month . '.php';
$log_file = $log_dir . 'firewall_' . $cur_month . '.php';

// Update stats :
if ( file_exists( $stat_file ) ) {
	$nfw_stat = file_get_contents( $stat_file, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES );
} else {
	$nfw_stat = '0:0:0:0:0:0:0:0:0:0';
}
$nfw_stat_arr = explode(':', $nfw_stat . ':');
$nfw_stat_arr[$loglevel]++;
@file_put_contents( $stat_file, $nfw_stat_arr[0] . ':' . $nfw_stat_arr[1] . ':' .
	$nfw_stat_arr[2] . ':' . $nfw_stat_arr[3] . ':' . $nfw_stat_arr[4] . ':' .
	$nfw_stat_arr[5] . ':' . $nfw_stat_arr[6] . ':' . $nfw_stat_arr[7] . ':' .
	$nfw_stat_arr[8] . ':' . $nfw_stat_arr[9], LOCK_EX);


// Check whether we should rotate the log :
if (! empty($nfw_options['log_rotate']) && @ctype_digit($nfw_options['log_maxsize']) ) {
	if ( file_exists($log_file) ) {
		$log_stat = stat($log_file);
		if ( $log_stat['size'] > $nfw_options['log_maxsize']) {
			// Rotate it :
			$log_ext = 1;
			while ( file_exists($log_file . '.' . sprintf('%02d', $log_ext) ) ) {
				$log_ext++;
			}
			rename($log_file, $log_file . '.' . sprintf('%02d', $log_ext));
		}
	}
}

// if $loglevel == 4, we don't log/need SCRIPT_NAME, IP and method :
if ( $loglevel == 4 ) {
	$SCRIPT_NAME = '-';
	$REQUEST_METHOD = 'N/A';
	$REMOTE_ADDR = '0.0.0.0';
	$loglevel = 6;
} else {
	$SCRIPT_NAME = $_SERVER['SCRIPT_NAME'];
	$REQUEST_METHOD = $_SERVER['REQUEST_METHOD'];
	$REMOTE_ADDR = NFW_REMOTE_ADDR;
}

if (! file_exists($log_file) ) {
	$tmp = '<?php exit; ?>' . "\n";
} else {
	$tmp = '';
}

// Which encoding to use?
if ( defined('NFW_LOG_ENCODING') ) {
	if ( NFW_LOG_ENCODING == 'b64' ) {
		$encoding = '[b64:' . base64_encode( $res ) . ']';
	} elseif ( NFW_LOG_ENCODING == 'none' ) {
		$encoding = '[' . $res . ']';
	} else {
		$unp = unpack('H*', $res);
		$encoding = '[hex:' . array_shift( $unp )  . ']';
	}
} else {
	$unp = unpack('H*', $res);
	$encoding = '[hex:' . array_shift( $unp )  . ']';
}

@file_put_contents( $log_file,
	$tmp . '[' . time() . '] ' . '[0] ' .
	'[' . $_SERVER['SERVER_NAME'] . '] ' . '[#' . $num_incident . '] ' .
	'[' . $ruleid . '] ' .
	'[' . $loglevel . '] ' . '[' . $REMOTE_ADDR . '] ' .
	'[' . $http_ret_code . '] ' . '[' . $REQUEST_METHOD . '] ' .
	'[' . $SCRIPT_NAME . '] ' . '[' . $loginfo . '] ' .
	$encoding . "\n", FILE_APPEND | LOCK_EX);

/* ================================================================== */
// EOF
