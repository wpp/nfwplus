<?php
/*
 +=====================================================================+
 | NinjaFirewall (WP+ Edition)                                         |
 |                                                                     |
 | (c) NinTechNet - http://nintechnet.com/                             |
 +=====================================================================+ i18n+ / sa
*/

if (! defined( 'NFW_ENGINE_VERSION' ) ) { die( 'Forbidden' ); }

// Block immediately if user is not allowed :
nf_not_allowed( 'block', __LINE__ );

$geoip_dat = __DIR__ . '/share/GeoIP.dat';
$iso_csv = __DIR__ . '/share/iso3166.csv';

$nfw_options = nfw_get_option( 'nfw_options' );

echo '<script>
function removeOptions(box) {
	selectbox = document.ac_form.elements[box+"_out[]"];
	if (! selectbox.options.length) {
		alert("' . esc_js( __('Your list is empty.', 'nfwplus') ) . '");
		return false;
	}
	var i; var num=0;
	for ( i = selectbox.options.length-1; i>=0; i--) {
		if (selectbox.options[i].selected) {
			selectbox.remove(i);
			num++;
		}
	}
	if (! num) {
		alert("' . esc_js( __('Select one or more value to delete.', 'nfwplus') ) . '");
	}
}
function addOption( selectbox, value, box) {
	if (! value.value) {
		alert("' . esc_js( __('Enter a value.', 'nfwplus') ) . '");
		value.focus();
		return false;
	}
	selectbox = document.ac_form.elements[box+"_out[]"];
	for ( i = selectbox.options.length-1; i >= 0; i-- ) {
		if ( selectbox.options[i].value == value.value ) {
			alert("[" + selectbox.options[i].value + "] ' . esc_js( __('is already in your list.', 'nfwplus') ) . '");
			value.select();
			return false;
		}
	}
	if ( box == "bot" ) {
		valid = new RegExp (/^[a-zA-Z0-9\.\-_:\x20]+$/);
		if (! value.value.match(valid)) {
			alert("' . esc_js( __('Allowed characters are: a-z  0-9  . - _ : and space.', 'nfwplus') ) . '");
			value.focus();
			return false;
		}
	} else if ( box.match(/^ip/) ) {

		valid = new RegExp (/^[a-f0-9.:]{3,45}$/);
		if (! value.value.match(valid)) {
			alert("' . esc_js( __('IPs must contain:', 'nfwplus') ) . '\n' .
		 esc_js( __('-at least the first 3 characters.', 'nfwplus') ) . '\n' .
		 esc_js( __('-IPv4: digits [0-9] and dot [.] only.', 'nfwplus') ) . '\n' .
		 esc_js( __('-IPv6: digit [0-9], hex chars [a-f] and colon [:] only.', 'nfwplus') ) . '\n\n' .
		 esc_js( __('See contextual Help for more info.', 'nfwplus') ) . '");
			value.focus();
			value.select();
			return false;
		}
	} else if ( box.match(/^(url|geourl)/) ) {
		valid = new RegExp (/^[a-zA-Z0-9\.\-_\/\x20]+$/);
		if (! value.value.match(valid)) {
			alert("' . esc_js( __('Allowed characters are: a-z  0-9  . - _ / and space.', 'nfwplus') ) . '");
			value.focus();
			value.select();
			return false;
		}
	}
	var optn = document.createElement("OPTION");
	optn.text = value.value;
	optn.value = value.value;
	selectbox.options.add(optn, selectbox.options[0]);
	value.value = "";
	value.focus();
}
function default_bots() {
	if (! confirm("' . esc_js( __('The default list of bots will be restored. All changes you may have done will be lost. Go ahead?', 'nfwplus') ) . '")){
		return false;
	}
	var array = [';
$bots = explode('|', NFW_BOT_LIST);
krsort( $bots );
$tmp = '';
foreach ($bots as $bot) {
	$tmp .= "'" . ucwords( $bot ) . "', ";
}
echo rtrim($tmp , ', ');
echo '];
	var rejectbox = document.ac_form.elements["bot_out[]"];
	while(rejectbox.length > 0 ) {
		rejectbox.remove(0);
	}
	for (var i = 0; i < array.length; i++) {
		var optn = document.createElement("OPTION");
		optn.text = array[i];
		optn.value = array[i];
		rejectbox.options.add(optn, rejectbox.options[0]);
	}
}
function cn_list(in_out){
	if (in_out == 0) {
		var selectbox = document.ac_form.cn_in;
		var rejectbox = document.ac_form.elements["cn_out[]"];
	} else {
		var rejectbox = document.ac_form.cn_in;
		var selectbox = document.ac_form.elements["cn_out[]"];
	}
	if (! selectbox.options.length){
		alert("' . esc_js( __('Your list is empty.', 'nfwplus') ) . '");
		return false;
	}
	var i;
	var num=0;
	for ( i = selectbox.options.length-1; i>=0; i--) {
		if (selectbox.options[i].selected) {
			// add to rejected countries list :
			var optn = document.createElement("OPTION");
			optn.text = selectbox.options[i].text;
			optn.value = selectbox.options[i].value;
			rejectbox.options.add(optn, rejectbox.options[0]);
			// delete from the other list:
			selectbox.remove(i);
			num++;
		}
	}
	if (! num){
		if (in_out == 0) {
			alert("' . esc_js( __('Select a country from the [Available countries] list.', 'nfwplus') ) . '");
		} else {
			alert("' . esc_js( __('Select a country from the [Blocked countries] list.', 'nfwplus') ) . '");
		}
	}
}
function ac_toogle_table(tname, off) {
	if (off == 1) {
		jQuery("#"+ tname).slideUp(800);
	} else {
		jQuery("#"+ tname).slideDown(800);
	}
}
function ac_radio_toogle(on_off, rbutton) {
	var what = "nfw_options["+rbutton+"]";
	if (on_off) {
		document.ac_form.elements[what].disabled = false;
		document.ac_form.elements[what].focus();
	} else {
		document.ac_form.elements[what].disabled = true;
	}
}

function check_fields() {
	// at least one HTTP method must be selected:
	if ( document.ac_form.elements["nfw_options[ac_method_0]"].checked == false && document.ac_form.elements["nfw_options[ac_method_1]"].checked == false && document.ac_form.elements["nfw_options[ac_method_2]"].checked == false && document.ac_form.elements["nfw_options[ac_method_3]"].checked == false && document.ac_form.elements["nfw_options[ac_method_4]"].checked == false && document.ac_form.elements["nfw_options[ac_method_5]"].checked == false ) {
		alert("' . esc_js( __('Error: you must select at least one [ HTTP methods ].', 'nfwplus') ) . '");
		return false;
	}

	// GeoIP Source IP:
	if ( document.ac_form.elements["nfw_options[ac_ip]"][2].checked == true && document.ac_form.elements["nfw_options[ac_ip_2]"].value == "") {
		alert("' . esc_js( __('Please enter a value for the [ Source IP > Other ] field.', 'nfwplus') ) . '");
		return false;
	}';

	if ( empty($_SERVER['HTTP_X_FORWARDED_FOR']) ){
	echo '
		if ( document.ac_form.elements["nfw_options[ac_ip]"][1].checked == true ) {
			if (! confirm("' . esc_js( __('Your server does not seem to support the HTTP_X_FORWARDED_FOR variable. Go ahead?', 'nfwplus') ) . '") ) {
				document.ac_form.elements["nfw_options[ac_ip]"][1].focus();
				return false;
			}
		}';
	}
	echo '

	// GeoIP DB:
	if ( document.ac_form.elements["nfw_options[ac_geoip_db]"][1].checked == true && document.ac_form.elements["nfw_options[ac_geoip_db2]"].value == "") {
		alert("' . esc_js( __('Please enter a value for the [ Geolocation > Source > PHP Variable ] field.', 'nfwplus') ) . '");
		return false;
	}

	// Rate Limiting:
	if ( document.ac_form.elements["nfw_options[ac_rl_conn]"].value == 0) {
		alert("' . esc_js( __('Please enter the number of connections allowed for the [Rate Limiting] directive.', 'nfwplus') ) . '");
		document.ac_form.elements["nfw_options[ac_rl_conn]"].focus();
		return false;
	}

	// select all option lists
	for ( i = document.ac_form.elements["geourl_out[]"].options.length-1; i >= 0; i-- ) {
		document.ac_form.elements["geourl_out[]"].options[i].selected = true;
	}
	for ( i = document.ac_form.elements["cn_out[]"].options.length-1; i >= 0; i-- ) {
		document.ac_form.elements["cn_out[]"].options[i].selected = true;
	}
	for ( i = document.ac_form.elements["bot_out[]"].options.length-1; i >= 0; i-- ) {
		document.ac_form.elements["bot_out[]"].options[i].selected = true;
	}
	for ( i = document.ac_form.elements["ipallow_out[]"].options.length-1; i >= 0; i-- ) {
		document.ac_form.elements["ipallow_out[]"].options[i].selected = true;
	}
	for ( i = document.ac_form.elements["ipblock_out[]"].options.length-1; i >= 0; i-- ) {
		document.ac_form.elements["ipblock_out[]"].options[i].selected = true;
	}
	for ( i = document.ac_form.elements["urlblock_out[]"].options.length-1; i >= 0; i-- ) {
		document.ac_form.elements["urlblock_out[]"].options[i].selected = true;
	}
	for ( i = document.ac_form.elements["urlallow_out[]"].options.length-1; i >= 0; i-- ) {
		document.ac_form.elements["urlallow_out[]"].options[i].selected = true;
	}
	return true;
}
function is_number(id) {
	var e = document.getElementById(id);
	if (! e.value ) { return }
	if (! /^[1-9][0-9]{0,2}$/.test(e.value) ) {
		alert("' . esc_js( __('Please enter a number from 1 to 999.', 'nfwplus') ) . '");
		e.value = e.value.substring(0, e.value.length-1);
	}
}
function restore() {
   if (confirm("' . esc_js( __('All fields will be restored to their default values. Go ahead?', 'nfwplus') ) . '")){
      return true;
   }else{
		return false;
   }
}
</script>';

echo '<div class="wrap">
	<div style="width:33px;height:33px;background-image:url( ' . plugins_url() . '/nfwplus/images/ninjafirewall_32.png);background-repeat:no-repeat;background-position:0 0;margin:7px 5px 0 0;float:left;"></div>
	<h1>'. __('Access Control', 'nfwplus') . '</h1>';

// Saved options ?
if ( isset( $_POST['nfw_options']) ) {
	if ( empty($_POST['nfwnonce']) || ! wp_verify_nonce($_POST['nfwnonce'], 'ac_save') ) {
		wp_nonce_ays('ac_save');
	}
	if (! empty($_POST['save_acd']) ) {
		nf_sub_access_save();
		echo '<div class="updated notice is-dismissible"><p>'. __('Your changes have been saved.', 'nfwplus') . '</p></div>';
	} elseif (! empty($_POST['rest_acd']) ) {
		nf_sub_access_default();
		echo '<div class="updated notice is-dismissible"><p>'. __('Default values were restored.', 'nfwplus') . '</p></div>';
	} else {
		echo '<div class="error notice is-dismissible"><p>'. __('No action taken.', 'nfwplus') . '</p></div>';
	}
	$nfw_options = nfw_get_option( 'nfw_options' );
}
$err_msg = '';
if (! file_exists($iso_csv) ) {
	$err_msg = __('Error: Unable to find the ISO country code CSV file', 'nfwplus') . ' <code>' . $iso_csv . '</code>.<br />';
}
if (! file_exists($geoip_dat) ) {
	$err_msg .= __('Error: Unable to find the GeoIP database', 'nfwplus') . ' <code>' . $geoip_dat . '</code>.<br />';
}
if ( $err_msg ) {
	echo '<div class="error notice is-dismissible"><p>' . $err_msg . '</p></div></div>';
	return;
}

if ( empty( $nfw_options['ac_role'] ) ) {
	$nfw_options['ac_role'] = '10000';
}

if ( defined('NFW_WPWAF') ) {
	?>
	<div style="background:#fff;border-left:4px solid #fff;-webkit-box-shadow:0 1px 1px 0 rgba(0,0,0,.1);box-shadow:0 1px 1px 0 rgba(0,0,0,.1);margin:5px 0 15px;padding:1px 12px;border-left-color:orange;">
		<p><?php printf( __('You are running NinjaFirewall in <i>WordPress WAF</i> mode. All URL-based features such as <i>Geolocation</i> and <i>URL Access Control</i> will be limited to a few WordPress files only (e.g., index.php, wp-login.php, xmlrpc.php, admin-ajax.php, wp-load.php etc). If you want them to apply to any PHP script, you will need to run NinjaFirewall in %s mode.', 'nfwplus'), '<a href="https://blog.nintechnet.com/full_waf-vs-wordpress_waf/">Full WAF</a>') ?></p>
	</div>
	<?php
}

?>
<form name="ac_form" method="post" action="?page=nfsubaccess" onSubmit="return check_fields();">
	<?php wp_nonce_field('ac_save', 'nfwnonce', 0); ?>
	<h3><?php _e('Role-based Access Control', 'nfwplus') ?></h3>
	<table class="form-table">
		<tr>
			<th scope="row"><?php _e('Do not block the following users (must be logged in)', 'nfwplus') ?></th>
			<td width="20">&nbsp;</td>
			<td align="left">
				<p><label><input type="checkbox" name="nfw_options[ac_role_0]" value="1"<?php checked($nfw_options['ac_role'][0], 1) ?>>&nbsp;<?php _e('Admin/Super Admin (default)', 'nfwplus') ?></label></p>
				<p><label><input type="checkbox" name="nfw_options[ac_role_1]" value="1"<?php checked($nfw_options['ac_role'][1], 1) ?>>&nbsp;<?php _e('Editor', 'nfwplus') ?></label></p>
				<p><label><input type="checkbox" name="nfw_options[ac_role_2]" value="1"<?php checked($nfw_options['ac_role'][2], 1) ?>>&nbsp;<?php _e('Author', 'nfwplus') ?></label></p>
				<p><label><input type="checkbox" name="nfw_options[ac_role_3]" value="1"<?php checked($nfw_options['ac_role'][3], 1) ?>>&nbsp;<?php _e('Contributor', 'nfwplus') ?></label></p>
				<p><label><input type="checkbox" name="nfw_options[ac_role_4]" value="1"<?php checked($nfw_options['ac_role'][4], 1) ?>>&nbsp;<?php _e('Subscriber', 'nfwplus') ?></label></p>
			</td>
		</tr>
	</table>

	<a name="sourceip"></a>
	<br />
	<br />
	<?php
	if (empty( $nfw_options['ac_ip'] ) ) {
		$nfw_options['ac_ip'] = 1;
		$nfw_options['ac_ip_2'] = '';
	}
	if (empty($nfw_options['ac_ip_2']) ) {
		$nfw_options['ac_ip_2'] = '';
	}
	$warn_msg = '';
	?>
	<h3><?php _e('Source IP', 'nfwplus') ?></h3>
	<table class="form-table">
		<tr valign="top">
			<th scope="row"><?php _e('IP-based access control directives should use', 'nfwplus') ?></th>
			<td width="20">&nbsp;</td>
			<td align="left">
				<p><label><input type="radio" name="nfw_options[ac_ip]" value="1"<?php checked($nfw_options['ac_ip'], 1) ?> onclick="ac_radio_toogle(0,'ac_ip_2');" />&nbsp;<code>REMOTE_ADDR</code> (<?php echo htmlspecialchars($_SERVER['REMOTE_ADDR']) ?>)</label></p>
				<p><label><input type="radio" name="nfw_options[ac_ip]" value="2"<?php checked($nfw_options['ac_ip'], 2) ?> onclick="ac_radio_toogle(0,'ac_ip_2');" />&nbsp;<code>HTTP_X_FORWARDED_FOR</code><?php
				if (! empty($_SERVER['HTTP_X_FORWARDED_FOR']) ) {
					echo ' ('. htmlspecialchars($_SERVER['HTTP_X_FORWARDED_FOR']) .')';
				}
				?></label></p>
				<p><label><input type="radio" name="nfw_options[ac_ip]" value="3"<?php checked($nfw_options['ac_ip'], 3) ?> onclick="ac_radio_toogle(1,'ac_ip_2');" />&nbsp;<?php _e('Other', 'nfwplus') ?></label>&nbsp;<input class="small-text code" type="text" style="width:250px;" maxlength="30" name="nfw_options[ac_ip_2]" value="<?php echo htmlspecialchars($nfw_options['ac_ip_2']) ?>" onChange="javascript:this.value=this.value.toUpperCase();" placeholder="<?php _e('e.g.,', 'nfwplus') ?> HTTP_CLIENT_IP"<?php if ( empty($nfw_options['ac_ip_2']) ) { echo ' disabled';}?> /><?php
				if (! empty($_SERVER[$nfw_options['ac_ip_2']]) ) {
					echo ' ('. htmlspecialchars($_SERVER[$nfw_options['ac_ip_2']]) .')';
				}
				?></p>
			</td>
			<td align="left">
			<?php
			if ( $nfw_options['ac_ip'] == 3 && ! empty($nfw_options['ac_ip_2']) && empty($_SERVER[$nfw_options['ac_ip_2']]) ) {
				$warn_msg = htmlspecialchars($nfw_options['ac_ip_2']);
			} elseif ($nfw_options['ac_ip'] == 2 && empty($_SERVER['HTTP_X_FORWARDED_FOR']) ){
				$warn_msg = 'HTTP_X_FORWARDED_FOR';
			}
			if ($warn_msg) {
				echo '&nbsp;<img src="' . plugins_url() . '/nfwplus/images/icon_error_16.png" border="0" height="16" width="16">&nbsp;<span class="description">'.  sprintf(__('Your server does not seem to support %s', 'nfwplus'), '<code>' . $warn_msg . '</code>!') . '</span>';
			}
			?>
			</td>
		</tr>
	</table>
	<?php
	if ( empty( $nfw_options['allow_local_ip']) ) {
		$allow_local_ip = 0;
	} else {
		$allow_local_ip = 1;
	}
	?><br>

	<table class="form-table">
		<tr valign="top">
			<th scope="row"><?php _e('Scan traffic coming from localhost and private IP address spaces', 'nfwplus') ?></th>
			<td width="20">&nbsp;</td>
			<td align="left" width="120" style="vertical-align:top;">
				<label><input type="radio" name="nfw_options[allow_local_ip]" value="0"<?php checked($allow_local_ip, 0 ) ?>>&nbsp;<?php _e('Yes (default)', 'nfwplus') ?></label>
			</td>
			<td align="left" style="vertical-align:top;">
				<label><input type="radio" name="nfw_options[allow_local_ip]" value="1"<?php checked($allow_local_ip, 1 ) ?>>&nbsp;<?php _e('No', 'nfwplus') ?></label>
			</td>
		</tr>
	</table>

	<br />
	<br />
	<?php
	if ( empty($nfw_options['ac_method']) ) {
		$nfw_options['ac_method'] = 'GETPOSTHEADPUTDELETEPATCH';
	}
	?>
	<h3><?php _e('HTTP Methods', 'nfwplus') ?></h3>
	<table class="form-table">
		<tr valign="top">
			<th scope="row"><?php _e('All Access Control directives below should apply to the folowing HTTP methods', 'nfwplus') ?></th>
			<td width="20">&nbsp;</td>
			<td align="left">
				<p><label><input type="checkbox" name="nfw_options[ac_method_0]" value="1"<?php if ( strpos($nfw_options['ac_method'], 'GET' ) !== FALSE) { echo ' checked'; } ?>>&nbsp;<?php _e('<code>GET</code> (default)', 'nfwplus') ?></label></p>
				<p><label><input type="checkbox" name="nfw_options[ac_method_1]" value="1"<?php if ( strpos($nfw_options['ac_method'], 'POST' ) !== FALSE) { echo ' checked'; } ?>>&nbsp;<?php _e('<code>POST</code> (default)', 'nfwplus') ?></label></p>
				<p><label><input type="checkbox" name="nfw_options[ac_method_2]" value="1"<?php if ( strpos($nfw_options['ac_method'], 'HEAD' ) !== FALSE) { echo ' checked'; } ?>>&nbsp;<?php _e('<code>HEAD</code> (default)', 'nfwplus') ?></label></p>
				<p><label><input type="checkbox" name="nfw_options[ac_method_3]" value="1"<?php if ( strpos($nfw_options['ac_method'], 'PUT' ) !== FALSE) { echo ' checked'; } ?>>&nbsp;<?php _e('<code>PUT</code> (default)', 'nfwplus') ?></label></p>
				<p><label><input type="checkbox" name="nfw_options[ac_method_4]" value="1"<?php if ( strpos($nfw_options['ac_method'], 'DELETE' ) !== FALSE) { echo ' checked'; } ?>>&nbsp;<?php _e('<code>DELETE</code> (default)', 'nfwplus') ?></label></p>
				<p><label><input type="checkbox" name="nfw_options[ac_method_5]" value="1"<?php if ( strpos($nfw_options['ac_method'], 'PATCH' ) !== FALSE) { echo ' checked'; } ?>>&nbsp;<?php _e('<code>PATCH</code> (default)', 'nfwplus') ?></label></p>
			</td>
		</tr>
	</table>

	<br />
	<br />

	<?php
	if ( empty($nfw_options['ac_geoip']) ) {
		$nfw_options['ac_geoip'] = 0;
	}
	?>
	<h3><?php _e('Geolocation Access Control', 'nfwplus') ?></h3>
	<table class="form-table">
		<tr>
			<td>
				<table class="form-table">
					<tr style="background-color:#F9F9F9;border:solid 1px #DFDFDF;">
						<th scope="row"><?php _e('Enable Geolocation', 'nfwplus') ?></th>
						<td width="20">&nbsp;</td>
						<td align="left"><label><input type="radio" name="nfw_options[ac_geoip]" value="1" onclick="ac_toogle_table('geotable',0);"<?php checked($nfw_options['ac_geoip'], 1) ?>>&nbsp;<?php _e('Yes', 'nfwplus') ?></label></td>
						<td align="left"><label><input type="radio" name="nfw_options[ac_geoip]" value="0" onclick="ac_toogle_table('geotable',1);"<?php checked($nfw_options['ac_geoip'], 0) ?>>&nbsp;<?php _e('No', 'nfwplus') ?> (<?php _e('default', 'nfwplus') ?>)</label></td>
					</tr>
				</table>

				<br />
				<div id="geotable"<?php if (empty($nfw_options['ac_geoip'])) {echo ' style="display:none;"';} ?>>

				<?php
				if (empty($nfw_options['ac_geoip_db2']) ) {
					$nfw_options['ac_geoip_db2'] = '';
				}
				$no_db = $no_var = '';
				if ( empty($nfw_options['ac_geoip_db']) ) {
					$nfw_options['ac_geoip_db'] = 1;
				}

				if ($nfw_options['ac_geoip_db'] == 1) {
					if (! file_exists($geoip_dat) ) {
						$nfw_options['ac_geoip_db'] = 0;
						$no_db = '<br /><img src="' . plugins_url() . '/nfwplus/images/icon_error_16.png" border="0" height="16" width="16">&nbsp;<span class="description">'. __('GeoIP database not found!', 'nfwplus') .'</span>';
					}
				} elseif ( $nfw_options['ac_geoip_db'] == 2 ) {
					if (! empty($nfw_options['ac_geoip_db2']) && ( empty($_SERVER[$nfw_options['ac_geoip_db2']]) || ! preg_match('/^[A-Z0-9]{2}$/', $_SERVER[$nfw_options['ac_geoip_db2']]) ) ) {
						$no_var = '<br /><img src="' . plugins_url() . '/nfwplus/images/icon_error_16.png" border="0" height="16" width="16">&nbsp;<span class="description">'. __('Your server does not seem to support this variable!', 'nfwplus') .'</span>';
						$nfw_options['ac_geoip_db'] = 0;
					} elseif ( empty($nfw_options['ac_geoip_db2']) ) {
						$nfw_options['ac_geoip_db2'] = '';
						$nfw_options['ac_geoip_db'] = 0;
					}
				}
				?>
				<table class="form-table">
					<tr valign="top">
						<th scope="row"><?php _e('Retrieve ISO 3166 country code from', 'nfwplus') ?></th>
						<td width="20">&nbsp;</td>

						<td align="left" style="vertical-align:top;"><label><input <?php if ($no_db) { echo 'disabled ';}?>type="radio" name="nfw_options[ac_geoip_db]" value="1" onclick="ac_radio_toogle(0,'ac_geoip_db2');"<?php checked($nfw_options['ac_geoip_db'], 1) ?>>&nbsp;NinjaFirewall (<?php _e('default', 'nfwplus') ?>)</label><?php echo $no_db ?></td>

						<td align="left" style="vertical-align:top;"><label><input type="radio" name="nfw_options[ac_geoip_db]" value="2" onclick="ac_radio_toogle(1,'ac_geoip_db2');"<?php checked($nfw_options['ac_geoip_db'], 2) ?>>&nbsp;<?php _e('PHP Variable', 'nfwplus') ?> </label><input type="text" name="nfw_options[ac_geoip_db2]" value="<?php echo htmlspecialchars($nfw_options['ac_geoip_db2']) ?>" placeholder="<?php _e('e.g.,', 'nfwplus') ?> GEOIP_COUNTRY_CODE" style="width:220px;" onChange="javascript:this.value=this.value.toUpperCase();"<?php if ( empty($nfw_options['ac_geoip_db2']) ) { echo ' disabled';}?> class="small-text code" /><?php echo $no_var ?></td>

					</tr>
				</table>

				<br />

				<?php
				if (empty($nfw_options['ac_geoip_log']) ) {
					$nfw_options['ac_geoip_log'] = 0;
				} else {
					$nfw_options['ac_geoip_log'] = 1;
				}
				?>
				<table class="form-table" border=0>
					<tr>
						<th scope="row"><?php _e('Block visitors from the following countries', 'nfwplus') ?></th>
						<td width="20">&nbsp;</td>
						<td align="left" valign="top" style="vertical-align:top;"><?php _e('Available countries:', 'nfwplus') ?><br />
						<select multiple size="8" name="cn_in" class="small-text code" style="width:190px;height:200px;">
						<?php
						if ( empty($nfw_options['ac_geoip_cn']) ) {
							$nfw_options['ac_geoip_cn'] = '';
						}
						$blocked_cn = explode('|', $nfw_options['ac_geoip_cn']);
						$desc_cn = array();
						$csv_array = file($iso_csv, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
						foreach ($csv_array as $line) {
							if (preg_match( '/^(\w\w),"(.+?)"$/', $line, $match) ) {
								if (! in_array($match[1], $blocked_cn ) ) {
									echo '<option title="' . htmlspecialchars($match[2]) . '" value="' . htmlspecialchars($match[1]) . '">' . htmlspecialchars($match[1]) . ' ' . htmlspecialchars($match[2]) . '</option>';
								} else {
									$desc_cn[$match[1]] = $match[2];
								}
							}
						}
						?>
						</select>
						<br />
						<br />
						<label><input type="checkbox" name="nfw_options[ac_geoip_log]" <?php checked($nfw_options['ac_geoip_log'], 1); ?>>&nbsp;<?php _e('Log event', 'nfwplus') ?> (<?php _e('default', 'nfwplus') ?>)</label>
						</td>
						<td align="middle" style="vertical-align:top;">
						<input type="button" class="button-secondary" value="<?php _e('Block', 'nfwplus') ?> &#187;" onclick="cn_list(0)">
						<br />
						<br />
						<input type="button" class="button-secondary" value="&#171; <?php _e('Unblock', 'nfwplus') ?>" onclick="cn_list(1)">
						</td>
						<td align="left" style="vertical-align:top;"><?php _e('Blocked countries:', 'nfwplus') ?><br />
						<select multiple="multiple" size="8" name="cn_out[]" class="small-text code" style="width:190px;height:200px;">
						<?php
						$tmp_cn = 0;
						@sort( $blocked_cn );
						foreach ( $blocked_cn as $line ) {
							if ( $line ) {
								$tmp_cn++;
								echo '<option title="' . htmlspecialchars($desc_cn[$line]) . '" value="' . htmlspecialchars($line) . '">' . htmlspecialchars($line) . ' ' . htmlspecialchars($desc_cn[$line]) . '</option>';
							}
						}
						?>
						</select>
						<?php
						// Warn if no countries are blocked but Geolocation is enabled:
						if (! $tmp_cn && $nfw_options['ac_geoip'] && empty($nfw_options['ac_geoip_ninja']) ) {
							echo '<br /><img src="' . plugins_url() . '/nfwplus/images/icon_warn_16.png" border="0" height="16" width="16">&nbsp;<span class="description">'. __('Your list is empty!<br />If you do not use geolocation, do not forget to disable it because it could slow down your site.', 'nfwplus') .'</span>';
						}
						?>
						</td>
					</tr>
				</table>

				<br />

				<table class="form-table">
					<tr valign="top">
						<th scope="row">
							<?php _e('Geolocation should apply to the whole site or specific URLs only?', 'nfwplus') ?>
							<br>
							<br>
							<p class="description"><?php	_e('(leave it blank if you want geolocation to apply to the whole site)', 'nfwplus') ?></p>
						</th>
						<td width="20">&nbsp;</td>
						<td align="left" style="vertical-align:top;">
							<input type="text" name="geourl_in" value="" maxlength="45" class="small-text code" style="width:200px;" placeholder="<?php _e('e.g.,', 'nfwplus') ?> /wp-login.php" />
							<br />
							<span class="description"><?php _e('Full or partial case-sensitive URL.', 'nfwplus') ?></span>
						</td>
						<td align="left" style="vertical-align:top;">
							<input type="button" class="button-secondary" value="<?php _e('Block', 'nfwplus') ?> &#187;" onclick="addOption(this.form.geourl_out, this.form.geourl_in, 'geourl')" />
							<br /><br />
							<input type="button" class="button-secondary" value="&#171; <?php _e('Unblock', 'nfwplus') ?>" onclick="removeOptions('geourl')" />
						</td>
						<td align="left" style="vertical-align:top;">
						<?php
						_e('Blocked URLs:', 'nfwplus') ?>
						<br />
						<select multiple="multiple" size="8" class="small-text code" name="geourl_out[]" style="width:180px;height:200px;">
						<?php
						$urls = '';
						if ( empty($nfw_options['ac_geo_url']) ) {
							$nfw_options['ac_geo_url'] = '';
						} else {
							$urls =  explode('|',  preg_replace( '/\\\([`.\\\+*?\[^\]$(){}=!<>|:-])/', '$1', $nfw_options['ac_geo_url'] ));
							sort( $urls );
							foreach ($urls as $url) {
								if ( $url ) {
									echo '<option title="' . htmlspecialchars($url) . '" value="' . htmlspecialchars($url) . '">' . htmlspecialchars($url) . '</option>';
								}
							}
						}
						?>
						</select>
						</td>
					</tr>

				</table>

				<?php
				if ( empty($nfw_options['ac_geoip_ninja']) ) {
					$nfw_options['ac_geoip_ninja'] = 0;
				} else {
					$nfw_options['ac_geoip_ninja'] = 1;
				}
				?>

				<table class="form-table">
					<tr valign="top">
						<th scope="row"><?php _e('Add <code>NINJA_COUNTRY_CODE</code> to PHP headers?', 'nfwplus') ?></th>
						<td width="20">&nbsp;</td>
						<td align="left" style="vertical-align:top;"><label><input type="radio" name="nfw_options[ac_geoip_ninja]" value="1"<?php checked($nfw_options['ac_geoip_ninja'], 1) ?> />&nbsp;<?php _e('Yes', 'nfwplus') ?></label></td>
						<td align="left" style="vertical-align:top;"><label><input type="radio" name="nfw_options[ac_geoip_ninja]" value="0"<?php checked($nfw_options['ac_geoip_ninja'], 0) ?> />&nbsp; <?php _e('No (default)', 'nfwplus') ?></label></td>
					</tr>
				</table>

				</div>
			</td>
		</tr>
	</table>

	<br />
	<br />

	<?php
	if (empty($nfw_options['ac_allow_ip_log']) ) {
		$nfw_options['ac_allow_ip_log'] = 0;
	} else {
		$nfw_options['ac_allow_ip_log'] = 1;
	}
	?>
	<a name="ipaccess"></a>
	<h3><?php _e('IP Access Control', 'nfwplus') ?></h3>
	<table class="form-table">
		<tr valign="top">
			<th scope="row"><?php _e('Allow the following IP', 'nfwplus') ?></th>
			<td width="20">&nbsp;</td>
			<td align="left" style="vertical-align:top;">
				<input type="text" name="ipallow_in" value="" maxlength="45" class="small-text code" style="width:200px;" placeholder="<?php _e('e.g.,', 'nfwplus') ?> 1.2.3.4 <?php _e('or', 'nfwplus') ?> 1.2.3" onChange="javascript:this.value=this.value.toLowerCase();" />
				<br />
				<span class="description"><?php _e('Full or partial IPv4/IPv6 address.', 'nfwplus') ?></span>
				<br />
				<br />
				<label><input type="checkbox" name="nfw_options[ac_allow_ip_log]" <?php checked($nfw_options['ac_allow_ip_log'], 1); ?>>&nbsp;<?php _e('Log event', 'nfwplus') ?></label>
			</td>
			<td align="left" style="vertical-align:top;">
				<input type="button" class="button-secondary" value="<?php _e('Allow', 'nfwplus') ?> &#187;" onclick="addOption(this.form.ipallow_out, this.form.ipallow_in, 'ipallow')" />
				<br /><br />
				<input type="button" class="button-secondary" value="&#171; <?php _e('Discard', 'nfwplus') ?>" onclick="removeOptions('ipallow')" />
			</td>
			<td align="left" style="vertical-align:top;">
			<?php _e('Allowed IPs:', 'nfwplus') ?>
			<br />
			<select multiple="multiple" size="8" class="small-text code" name="ipallow_out[]" style="width:180px;height:200px;">
			<?php
			if (empty($nfw_options['ac_allow_ip']) ) {
				$nfw_options['ac_allow_ip'] = '';
			} else {
				$ips = unserialize($nfw_options['ac_allow_ip']);
				if ( $ips ) {
					natsort( $ips );
					foreach ($ips as $ip) {
						if ( $ip ) {
							echo '<option title="' . htmlspecialchars($ip) . '" value="' . htmlspecialchars($ip) . '">' . htmlspecialchars($ip) . '</option>';
						}
					}
				}
			}
			?>
			</select>
			</td>
		</tr>

		<?php
		if (empty($nfw_options['ac_block_ip_log']) ) {
			$nfw_options['ac_block_ip_log'] = 0;
		} else {
			$nfw_options['ac_block_ip_log'] = 1;
		}
		?>
		<tr valign="top">
			<th scope="row"><?php _e('Block the following IP', 'nfwplus') ?></th>
			<td width="20">&nbsp;</td>
			<td align="left" style="vertical-align:top;">
				<input type="text" name="ipblock_in" value="" maxlength="45" class="small-text code" style="width:200px;" placeholder="<?php _e('e.g.,', 'nfwplus') ?> 1.2.3.4 <?php _e('or', 'nfwplus') ?> 1.2.3" onChange="javascript:this.value=this.value.toLowerCase();" />
				<br />
				<span class="description"><?php _e('Full or partial IPv4/IPv6 address.', 'nfwplus') ?></span>
				<br />
				<br />
				<label><input type="checkbox" name="nfw_options[ac_block_ip_log]" <?php checked($nfw_options['ac_block_ip_log'], 1); ?>>&nbsp;<?php _e('Log event', 'nfwplus') ?> (<?php _e('default', 'nfwplus') ?>)</label>
			</td>
			<td align="left" style="vertical-align:top;">
				<input type="button" class="button-secondary" value="<?php _e('Block', 'nfwplus') ?> &#187;" onclick="addOption(this.form.ipblock_out, this.form.ipblock_in, 'ipblock')" />
				<br /><br />
				<input type="button" class="button-secondary" value="&#171; <?php _e('Unblock', 'nfwplus') ?>" onclick="removeOptions('ipblock')" />
			</td>
			<td align="left" style="vertical-align:top;">
			<?php _e('Blocked IPs:', 'nfwplus') ?>
			<br />
			<select multiple="multiple" size="8" class="small-text code" name="ipblock_out[]" style="width:180px;height:200px;">
			<?php
			if (empty($nfw_options['ac_block_ip']) ) {
				$nfw_options['ac_block_ip'] = '';
			} else {
				$ips = unserialize($nfw_options['ac_block_ip']);
				if ( $ips ) {
					natsort( $ips );
					foreach ($ips as $ip) {
						if ( $ip ) {
							echo '<option title="' . htmlspecialchars($ip) . '" value="' . htmlspecialchars($ip) . '">' . htmlspecialchars($ip) . '</option>';
						}
					}
				}
			}
			?>
			</select>
			</td>
		</tr>
	</table>
	<br />

	<?php
	if (empty($nfw_options['ac_rl_on']) ) {
		$nfw_options['ac_rl_on'] = 0;
	} else {
		$nfw_options['ac_rl_on'] = 1;
	}

	if ( empty($nfw_options['ac_rl_conn']) || ! preg_match('/^[1-9][0-9]{0,2}$/', $nfw_options['ac_rl_conn']) ) {
		$nfw_options['ac_rl_conn'] = 10;
		$nfw_options['ac_rl_intv'] = 5;
	}
	if ( empty($nfw_options['ac_rl_time']) || ! preg_match('/^\d{1,3}$/', $nfw_options['ac_rl_time']) ) {
		$nfw_options['ac_rl_time'] = 30;
	}
	if ( empty($nfw_options['ac_rl_intv']) || ! preg_match('/^(5|1[05]|30)$/', $nfw_options['ac_rl_intv']) ) {
		$nfw_options['ac_rl_conn'] = 10;
		$nfw_options['ac_rl_intv'] = 5;
	}
	if (empty($nfw_options['ac_rl_log']) ) {
		$nfw_options['ac_rl_log'] = 0;
	} else {
		$nfw_options['ac_rl_log'] = 1;
	}
	?>

	<table class="form-table">
		<tr valign="top">
			<th scope="row"><?php _e('Rate Limiting', 'nfwplus') ?></th>
			<td width="20">&nbsp;</td>
			<td align="left" style="vertical-align:middle;">
				<input type="radio" name="nfw_options[ac_rl_on]" value="1" onclick="ac_radio_toogle(1,'ac_rl_intv');ac_radio_toogle(1,'ac_rl_log');ac_radio_toogle(1,'ac_rl_conn');ac_radio_toogle(1,'ac_rl_time');" <?php checked($nfw_options['ac_rl_on'], 1); ?> />&nbsp;<?php
				printf(
					__('Block for %s seconds any IP<br />with more than %s connections<br />within a %s interval.', 'nfwplus'),
					'<input type="text" name="nfw_options[ac_rl_time]" value="'. $nfw_options['ac_rl_time'] .'" '. disabled($nfw_options['ac_rl_on'], 0, 0) .' id="acrltime" onKeyup="is_number(\'acrltime\');" size="2" maxlength="3" />',
					'<input type="text" id="acrlconn" name="nfw_options[ac_rl_conn]" value="'. $nfw_options['ac_rl_conn'] .'" size="2" maxlength="3" '. disabled($nfw_options['ac_rl_on'], 0, 0) .' onKeyup="is_number(\'acrlconn\');" />',
					'<select name="nfw_options[ac_rl_intv]" '. disabled($nfw_options['ac_rl_on'], 0, 0) .'><option value="5" '. selected($nfw_options['ac_rl_intv'], 5, 0) .'>'. __('5-second', 'nfwplus') .'</option><option value="10" '. selected($nfw_options['ac_rl_intv'], 10, 0) .'>'. __('10-second', 'nfwplus') .'</option><option value="15" '. selected($nfw_options['ac_rl_intv'], 15, 0) .'>'. __('15-second', 'nfwplus') .'</option><option value="30" '. selected($nfw_options['ac_rl_intv'], 30, 0) .'>'. __('30-second', 'nfwplus') .'</option></select>'
				); ?>
				<br />
				<br />
				<label><input type="checkbox" name="nfw_options[ac_rl_log]" <?php checked($nfw_options['ac_rl_log'], 1); ?> <?php disabled($nfw_options['ac_rl_on'], 0); ?>>&nbsp;<?php _e('Log event', 'nfwplus') ?> (<?php _e('default', 'nfwplus') ?>)</label>
			</td>
			<td align="left" style="vertical-align:top;padding-top:20px">
				<label><input type="radio" name="nfw_options[ac_rl_on]" value="0" onclick="ac_radio_toogle(0,'ac_rl_intv');ac_radio_toogle(0,'ac_rl_conn');ac_radio_toogle(0,'ac_rl_log');ac_radio_toogle(0,'ac_rl_time');" <?php checked($nfw_options['ac_rl_on'], 0); ?>/>&nbsp;<?php _e('Disabled (default)', 'nfwplus') ?></label>
			</td>
		</tr>
	</table>
	<br />
	<br />

	<?php
	if (empty($nfw_options['ac_bl_url_log']) ) {
		$nfw_options['ac_bl_url_log'] = 0;
	} else {
		$nfw_options['ac_bl_url_log'] = 1;
	}
	if (! isset($nfw_options['ac_wl_url_log']) ) {
		$nfw_options['ac_wl_url_log'] = 0;
	}
	?>
	<a name="urlaccess"></a>
	<h3><?php _e('URL Access Control', 'nfwplus') ?></h3>
	<table class="form-table">
		<tr valign="top">
			<th scope="row"><?php _e('Allow access to the following URL', 'nfwplus') ?> (<code>SCRIPT_NAME</code>)</th>
			<td width="20">&nbsp;</td>
			<td align="left" style="vertical-align:top;">
				<input type="text" name="url_allow_in" value="" class="small-text code" maxlength="150" style="width:200px;" placeholder="<?php _e('e.g.,', 'nfwplus') ?> /script.php" />
				<br />
				<span class="description"><?php _e('Full or partial case-sensitive URL.', 'nfwplus') ?></span>
				<br />
				<br />
				<label><input type="checkbox" name="nfw_options[ac_wl_url_log]" <?php checked($nfw_options['ac_wl_url_log'], 1); ?>>&nbsp;<?php _e('Log event', 'nfwplus') ?></label>
			</td>
			<td align="left" style="vertical-align:top;">
				<input type="button" class="button-secondary" value="<?php _e('Allow', 'nfwplus') ?> &#187;" onclick="addOption(this.form.urlallow_out, this.form.url_allow_in, 'urlallow')" />
				<br /><br />
				<input type="button" class="button-secondary" value="&#171; <?php _e('Discard', 'nfwplus') ?>" onclick="removeOptions('urlallow')" />
			</td>
			<td align="left" style="vertical-align:top;"><?php _e('Allowed URLs:', 'nfwplus') ?><br />
			<select multiple="multiple" size="8" class="small-text code" name="urlallow_out[]" style="width:180px;height:200px;">
			<?php
			$urls = '';
			if ( empty($nfw_options['ac_wl_url']) ) {
				$nfw_options['ac_wl_url'] = '';
			} else {
				$urls =  explode('|',  preg_replace( '/\\\([`.\\\+*?\[^\]$(){}=!<>|:-])/', '$1', $nfw_options['ac_wl_url'] ));
				sort( $urls );
				foreach ($urls as $url) {
					if ( $url ) {
						echo '<option title="' . htmlspecialchars($url) . '" value="' . htmlspecialchars($url) . '">' . htmlspecialchars($url) . '</option>';
					}
				}
			}
			?>
			</select>
			</td>
		</tr>


		<tr valign="top">
			<th scope="row"><?php _e('Block access to the following URL', 'nfwplus') ?> (<code>SCRIPT_NAME</code>)</th>
			<td width="20">&nbsp;</td>
			<td align="left" style="vertical-align:top;">
				<input type="text" name="url_block_in" value="" class="small-text code" maxlength="150" style="width:200px;" placeholder="<?php _e('e.g.,', 'nfwplus') ?> /script.php" />
				<br />
				<span class="description"><?php _e('Full or partial case-sensitive URL.', 'nfwplus') ?></span>
				<br />
				<br />
				<label><input type="checkbox" name="nfw_options[ac_bl_url_log]" <?php checked($nfw_options['ac_bl_url_log'], 1); ?>>&nbsp;<?php _e('Log event', 'nfwplus') ?> (<?php _e('default', 'nfwplus') ?>)</label>
			</td>
			<td align="left" style="vertical-align:top;">
				<input type="button" class="button-secondary" value="<?php _e('Block', 'nfwplus') ?> &#187;" onclick="addOption(this.form.urlblock_out, this.form.url_block_in, 'urlblock')" />
				<br /><br />
				<input type="button" class="button-secondary" value="&#171; <?php _e('Unblock', 'nfwplus') ?>" onclick="removeOptions('urlblock')" />
			</td>
			<td align="left" style="vertical-align:top;"><?php _e('Blocked URLs:', 'nfwplus') ?><br />
			<select multiple="multiple" size="8" class="small-text code" name="urlblock_out[]" style="width:180px;height:200px;">
			<?php
			$urls = '';
			if ( empty($nfw_options['ac_bl_url']) ) {
				$nfw_options['ac_bl_url'] = '';
			} else {
				$urls =  explode('|',  preg_replace( '/\\\([`.\\\+*?\[^\]$(){}=!<>|:-])/', '$1', $nfw_options['ac_bl_url'] ));
				sort( $urls );
				foreach ($urls as $url) {
					if ( $url ) {
						echo '<option title="' . htmlspecialchars($url) . '" value="' . htmlspecialchars($url) . '">' . htmlspecialchars($url) . '</option>';
					}
				}
			}
			?>
			</select>
			</td>
		</tr>
	</table>

	<br />
	<br />

	<?php
	if (empty($nfw_options['ac_bl_bot_log']) ) {
		$nfw_options['ac_bl_bot_log'] = 0;
	} else {
		$nfw_options['ac_bl_bot_log'] = 1;
	}
	?>

	<h3><?php _e('Bot Access Control', 'nfwplus') ?></h3>
	<table class="form-table">
		<tr valign="top">
			<th scope="row"><?php _e('Reject the following bots', 'nfwplus') ?> (<code>HTTP_USER_AGENT</code>)</th>
			<td width="20">&nbsp;</td>
			<td align="left" style="vertical-align:top;">
				<input type="text" name="bot_in" value="" class="small-text code" maxlength="50" style="width:200px;" placeholder="<?php _e('e.g.,', 'nfwplus') ?> Crawler" />
				<br />
				<span class="description"><?php _e('Case-insensitive string.', 'nfwplus') ?></span>
				<br />
				<br />
				<label><input type="checkbox" name="nfw_options[ac_bl_bot_log]" <?php checked($nfw_options['ac_bl_bot_log'], 1); ?>>&nbsp;<?php _e('Log event', 'nfwplus') ?> (<?php _e('default', 'nfwplus') ?>)</label>
			</td>
			<td align="left" style="vertical-align:top;">
				<input type="button" class="button-secondary" value="<?php _e('Block', 'nfwplus') ?> &#187;" onclick="addOption(this.form.bot_out, this.form.bot_in, 'bot')" />
				<br /><br />
				<input type="button" class="button-secondary" value="&#171; <?php _e('Unblock', 'nfwplus') ?>" onclick="removeOptions('bot')" />
			</td>
			<td align="left">
			<?php _e('Blocked bots:', 'nfwplus') ?>
			<br />
			<select multiple="multiple" size="8" class="small-text code" name="bot_out[]" style="width:180px;height:200px;">
			<?php
			$bots = '';
			if ( empty($nfw_options['ac_bl_bot']) ) {
				$nfw_options['ac_bl_bot'] = '';
			} else {
				$bots =  explode('|',  preg_replace( '/\\\([`.\\\+*?\[^\]$(){}=!<>|:-])/', '$1', $nfw_options['ac_bl_bot'] ));
				ksort( $bots );
				foreach ($bots as $bot) {
					if ( $bot ) {
						echo '<option title="' . htmlspecialchars( ucwords($bot) ) . '" value="' . htmlspecialchars( strtolower( $bot )) . '">' . htmlspecialchars( ucwords( $bot )) . '</option>';
					}
				}
			}
			?>
			</select>
			<br />
			 <a href="javascript:void(0)" onClick="default_bots();"><?php _e('Restore default list', 'nfwplus') ?></a>
			</td>
		</tr>
	</table>

	<br />
	<br />
	<input type="submit" class="button-primary" name="save_acd" value="<?php _e('Save Access Control directives', 'nfwplus') ?>" />
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<input class="button-secondary" type="submit" name="rest_acd" value="<?php _e('Restore Default Values', 'nfwplus') ?>" onclick="return restore();" />
</form>
</div>
<?php

/* ================================================================== */

function nf_sub_access_save(){

	// Save Access Control options

	global $nfw_options;

	// Role-based access control :
	$nfw_options['ac_role'] = '00000';
	if (! empty($_POST['nfw_options']['ac_role_0']) ) {
		$nfw_options['ac_role'][0] = 1;
	}
	if (! empty($_POST['nfw_options']['ac_role_1']) ) {
		$nfw_options['ac_role'][1] = 1;
	}
	if (! empty($_POST['nfw_options']['ac_role_2']) ) {
		$nfw_options['ac_role'][2] = 1;
	}
	if (! empty($_POST['nfw_options']['ac_role_3']) ) {
		$nfw_options['ac_role'][3] = 1;
	}
	if (! empty($_POST['nfw_options']['ac_role_4']) ) {
		$nfw_options['ac_role'][4] = 1;
	}
	// Enable/Disable good guy's flag :
	if ( nfw_is_goodguy(0) ) {
		$_SESSION['nfw_goodguy'] = true;
	} else {
		if ( isset( $_SESSION['nfw_goodguy'] ) ) {
			unset( $_SESSION['nfw_goodguy'] );
		}
	}

	// Source IP
	if ( (empty( $_POST['nfw_options']['ac_ip']) || ! preg_match('/^[123]$/', $_POST['nfw_options']['ac_ip']) ) ||
		( $_POST['nfw_options']['ac_ip'] == 3 && empty($_POST['nfw_options']['ac_ip_2']) ) ) {
		$nfw_options['ac_ip'] = 1;
		$nfw_options['ac_ip_2'] = 0;
	} else {
		$nfw_options['ac_ip'] = $_POST['nfw_options']['ac_ip'];
		if (! empty($_POST['nfw_options']['ac_ip_2']) ) {
			$nfw_options['ac_ip_2'] = trim( $_POST['nfw_options']['ac_ip_2'] );
		} else {
			$nfw_options['ac_ip_2'] = 0;
		}
	}
	// Scan server/local IPs ?
	if ( empty( $_POST['nfw_options']['allow_local_ip']) ) {
	// Default: yes
		$nfw_options['allow_local_ip'] = 0;
	} else {
		$nfw_options['allow_local_ip'] = 1;
	}

	// HTTP Methods:
	if ( empty($_POST['nfw_options']['ac_method_0']) &&
		empty($_POST['nfw_options']['ac_method_1']) &&
		empty($_POST['nfw_options']['ac_method_2']) &&
		empty($_POST['nfw_options']['ac_method_3']) &&
		empty($_POST['nfw_options']['ac_method_4']) &&
		empty($_POST['nfw_options']['ac_method_5']) ) {
		$nfw_options['ac_method'] = 'GETPOSTHEADPUTDELETEPATCH';
	} else {
		$nfw_options['ac_method'] = '';
		if (! empty($_POST['nfw_options']['ac_method_0']) ) {
			$nfw_options['ac_method'] .= 'GET';
		}
		if (! empty($_POST['nfw_options']['ac_method_1']) ) {
			$nfw_options['ac_method'] .= 'POST';
		}
		if (! empty($_POST['nfw_options']['ac_method_2']) ) {
			$nfw_options['ac_method'] .= 'HEAD';
		}
		if (! empty($_POST['nfw_options']['ac_method_3']) ) {
			$nfw_options['ac_method'] .= 'PUT';
		}
		if (! empty($_POST['nfw_options']['ac_method_4']) ) {
			$nfw_options['ac_method'] .= 'DELETE';
		}
		if (! empty($_POST['nfw_options']['ac_method_5']) ) {
			$nfw_options['ac_method'] .= 'PATCH';
		}
	}

	// Geolocation:
	// Enable?
	if ( empty( $_POST['nfw_options']['ac_geoip']) ) {
		// Default: no
		$nfw_options['ac_geoip'] = 0;
	} else {
		$nfw_options['ac_geoip'] = 1;
	}
	// DB to use:
	if (! @preg_match( '/^[12]$/', $_POST['nfw_options']['ac_geoip_db']) ) {
		$nfw_options['ac_geoip_db'] = 0;
		$nfw_options['ac_geoip_db2'] = '';
	} else {
		$nfw_options['ac_geoip_db'] = $_POST['nfw_options']['ac_geoip_db'];
		if ( empty($_POST['nfw_options']['ac_geoip_db2']) ) {
			$nfw_options['ac_geoip_db2'] = '';
		} else {
			$nfw_options['ac_geoip_db2'] = trim( $_POST['nfw_options']['ac_geoip_db2'] );
		}
	}
	// Countries to block:
	$nfw_options['ac_geoip_cn'] = '';
	if (! empty( $_POST['cn_out'] ) ) {
		foreach ( $_POST['cn_out'] as $cn ) {
			$nfw_options['ac_geoip_cn'] .= $cn . '|';
		}
		$nfw_options['ac_geoip_cn'] = rtrim( $nfw_options['ac_geoip_cn'], '|' );
	}
	// Geolocation (URLs):
	$tmp_geourl = '';
	if (! empty( $_POST['geourl_out'] ) ) {
		@sort( $_POST['geourl_out'] );
		foreach ( $_POST['geourl_out'] as $url ) {
			$tmp_geourl  .= preg_quote( trim ( $url ), '`') . '|';
		}
		$nfw_options['ac_geo_url'] = rtrim($tmp_geourl, '|' );
	} else {
		$nfw_options['ac_geo_url'] = 0;
	}


	// NINJA_COUNTRY_CODE:
	if ( empty( $_POST['nfw_options']['ac_geoip_ninja']) ) {
		// Default: no
		$nfw_options['ac_geoip_ninja'] = 0;
	} else {
		$nfw_options['ac_geoip_ninja'] = 1;
	}

	// Allow IPs :
	if (! empty( $_POST['ipallow_out'] ) ) {

		$tmp_ip = array();

		foreach ( $_POST['ipallow_out'] as $ip ) {
			if ( preg_match('/^[a-f0-9.:]{3,45}$/', $ip) ) {
				$tmp_ip[] = $ip;
			}
		}
		$nfw_options['ac_allow_ip'] = serialize($tmp_ip);

	} else {
		$nfw_options['ac_allow_ip'] = 0;
	}
	// Block IPs :
	if (! empty( $_POST['ipblock_out'] ) ) {

		$tmp_ip = array();

		foreach ( $_POST['ipblock_out'] as $ip ) {
			if ( preg_match('/^[a-f0-9.:]{3,45}$/', $ip) ) {
				$tmp_ip[] = $ip;
			}
		}
		$nfw_options['ac_block_ip'] = serialize($tmp_ip);
	} else {
		$nfw_options['ac_block_ip'] = 0;
	}

	// Rate Limiting :
	if (empty($_POST['nfw_options']['ac_rl_on']) ) {
		$nfw_options['ac_rl_on'] = 0;
	} else {
		$nfw_options['ac_rl_on'] = 1;
	}
	if ( empty($_POST['nfw_options']['ac_rl_time']) || ! preg_match('/^\d{1,3}$/', $_POST['nfw_options']['ac_rl_time']) ) {
		$nfw_options['ac_rl_time'] = 30;
	} else {
		$nfw_options['ac_rl_time'] = $_POST['nfw_options']['ac_rl_time'];
	}
	if ( empty($_POST['nfw_options']['ac_rl_conn']) || ! preg_match('/^[1-9][0-9]{0,2}$/', $_POST['nfw_options']['ac_rl_conn']) ) {
		$nfw_options['ac_rl_conn'] = 10;
		$nfw_options['ac_rl_intv'] = 5;
		$nfw_options['ac_rl_time'] = 30;
	} else {
		$nfw_options['ac_rl_conn'] = $_POST['nfw_options']['ac_rl_conn'];
	}
	if ( empty($_POST['nfw_options']['ac_rl_intv']) || ! preg_match('/^(5|1[05]|30)$/', $_POST['nfw_options']['ac_rl_intv']) ) {
		$nfw_options['ac_rl_conn'] = 10;
		$nfw_options['ac_rl_intv'] = 5;
		$nfw_options['ac_rl_time'] = 30;
	} else {
		$nfw_options['ac_rl_intv'] = $_POST['nfw_options']['ac_rl_intv'];
	}

	// Allowed URLs :
	$tmp_url = '';
	if (! empty( $_POST['urlallow_out'] ) ) {
		@sort( $_POST['urlallow_out'] );
		foreach ( $_POST['urlallow_out'] as $url ) {
			$tmp_url .= preg_quote( trim ( $url ), '`') . '|';
		}
		$nfw_options['ac_wl_url'] = rtrim($tmp_url, '|' );
	} else {
		$nfw_options['ac_wl_url'] = 0;
	}
	// Block URLs :
	$tmp_url = '';
	if (! empty( $_POST['urlblock_out'] ) ) {
		@sort( $_POST['urlblock_out'] );
		foreach ( $_POST['urlblock_out'] as $url ) {
			$tmp_url .= preg_quote( trim ( $url ), '`') . '|';
		}
		$nfw_options['ac_bl_url'] = rtrim( $tmp_url, '|' );
	} else {
		$nfw_options['ac_bl_url'] = 0;
	}

	// Block user-agents/bots :
	$tmp_bot = '';
	if (! empty( $_POST['bot_out'] ) ) {
		@sort( $_POST['bot_out'] );
		foreach ( $_POST['bot_out'] as $bot ) {
			$tmp_bot .= preg_quote( trim ( $bot ), '/') . '|';
		}
		$nfw_options['ac_bl_bot'] = rtrim( strtolower($tmp_bot), '|' );
	} else {
		$nfw_options['ac_bl_bot'] = 0;
	}

	// Log events :
	if ( isset( $_POST['nfw_options']['ac_geoip_log']) ) {
		$nfw_options['ac_geoip_log'] = 1;
	} else {
		$nfw_options['ac_geoip_log'] = 0;
	}
	if ( isset( $_POST['nfw_options']['ac_allow_ip_log']) ) {
		$nfw_options['ac_allow_ip_log'] = 1;
	} else {
		$nfw_options['ac_allow_ip_log'] = 0;
	}
	if ( isset( $_POST['nfw_options']['ac_block_ip_log']) ) {
		$nfw_options['ac_block_ip_log'] = 1;
	} else {
		$nfw_options['ac_block_ip_log'] = 0;
	}
	if ( isset( $_POST['nfw_options']['ac_rl_log']) ) {
		$nfw_options['ac_rl_log'] = 1;
	} else {
		if (empty($nfw_options['ac_rl_on']) ) {
			$nfw_options['ac_rl_log'] = 1;
		} else {
			$nfw_options['ac_rl_log'] = 0;
		}
	}
	if ( isset( $_POST['nfw_options']['ac_wl_url_log']) ) {
		$nfw_options['ac_wl_url_log'] = 1;
	} else {
		$nfw_options['ac_wl_url_log'] = 0;
	}
	if ( isset( $_POST['nfw_options']['ac_bl_url_log']) ) {
		$nfw_options['ac_bl_url_log'] = 1;
	} else {
		$nfw_options['ac_bl_url_log'] = 0;
	}
	if ( isset( $_POST['nfw_options']['ac_bl_bot_log']) ) {
		$nfw_options['ac_bl_bot_log'] = 1;
	} else {
		$nfw_options['ac_bl_bot_log'] = 0;
	}

	// Update :
	nfw_update_option( 'nfw_options', $nfw_options );

}
/* ================================================================== */

function nf_sub_access_default() {

	// Restore Access Control default values

	global $nfw_options;

	$_SESSION['nfw_goodguy'] = true;

	$nfw_options['ac_role'] = '10000';
	$nfw_options['ac_ip'] = 1;
	$nfw_options['ac_ip_2'] = 0;
	$nfw_options['allow_local_ip'] = 0;
	$nfw_options['ac_method'] = 'GETPOSTHEADPUTDELETEPATCH';
	$nfw_options['ac_geoip'] = 0;
	$nfw_options['ac_geoip_db'] = 1;
	$nfw_options['ac_geoip_db2'] = '';
	$nfw_options['ac_geoip_cn'] = '';
	$nfw_options['ac_geo_url'] = '';
	$nfw_options['ac_geoip_ninja'] = 0;

	$nfw_options['ac_allow_ip'] = 0;
	$nfw_options['ac_block_ip'] = 0;
	$nfw_options['ac_rl_on'] = 0;
	$nfw_options['ac_rl_time'] = 30;
	$nfw_options['ac_rl_conn'] = 10;
	$nfw_options['ac_rl_intv'] = 5;
	$nfw_options['ac_bl_url'] = 0;
	$nfw_options['ac_bl_bot'] = NFW_BOT_LIST;
	$nfw_options['ac_geoip_log'] = 1;
	$nfw_options['ac_allow_ip_log'] = 0;
	$nfw_options['ac_block_ip_log'] = 1;
	$nfw_options['ac_rl_log'] = 1;
	$nfw_options['ac_wl_url_log'] = 0;
	$nfw_options['ac_bl_url_log'] = 1;
	$nfw_options['ac_bl_bot_log'] = 1;

	// Update :
	nfw_update_option( 'nfw_options', $nfw_options );

}

/* ================================================================== */
// EOF
