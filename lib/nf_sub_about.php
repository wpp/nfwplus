<?php
/*
 +=====================================================================+
 | NinjaFirewall (WP+ Edition)                                         |
 |                                                                     |
 | (c) NinTechNet - http://nintechnet.com/                             |
 +=====================================================================+ i18n+ / sa
*/

if (! defined( 'NFW_ENGINE_VERSION' ) ) { die( 'Forbidden' ); }

// Block immediately if user is not allowed :
nf_not_allowed( 'block', __LINE__ );

if ( $data = @file_get_contents( dirname( plugin_dir_path(__FILE__) ) . '/readme.txt' ) ) {
	$what = '== Changelog ==';
	$pos_start = strpos( $data, $what );
	$changelog = substr( $data, $pos_start + strlen( $what ) + 1 );
} else {
	$changelog = __('Error : cannot find changelog :(', 'nfwplus');
}

echo '
<script>
function show_table(table_id) {
	var av_table = [11, 12, 13, 14];
	for (var i = 0; i < av_table.length; i++) {
		if ( table_id == av_table[i] ) {
			jQuery("#" + table_id).slideDown();
		} else {
			jQuery("#" + av_table[i]).slideUp();
		}
	};
}
</script>
<div class="wrap">
	<div style="width:33px;height:33px;background-image:url( ' . plugins_url() . '/nfwplus/images/ninjafirewall_32.png);background-repeat:no-repeat;background-position:0 0;margin:7px 5px 0 0;float:left;" title="NinTechNet"></div>
<h1>' . __('About', 'nfwplus') .'</h1>
	<br />
	<center>';
?>
	<table border="0" width="80%" style="padding:10px;-moz-box-shadow:-3px 5px 5px #999;-webkit-box-shadow:-3px 5px 5px #999;box-shadow:-3px 5px 5px #999;background-color:#749BBB;border:1px solid #638DB0;color:#fff;border-radius:6px">
		<tr>
			<td style="text-align:center">
				<font style="font-size: 2em; font-weight: bold;">NinjaFirewall (WP+ Edition) v<?php echo NFW_ENGINE_VERSION ?></font>
				<br />
				<font onContextMenu="nfw_eg();return false;">&copy;</font> <?php echo date( 'Y' ) ?> <a href="http://nintechnet.com/" target="_blank" title="The Ninja Technologies Network" style="color:#fcdc25"><strong>NinTechNet</strong></a>
				<br />
				The Ninja Technologies Network
				<br />&nbsp;
			</td>
		</tr>
		<tr style="text-align:center">
			<td width="100%">
				<table width="100%" border="0">
					<tr>
						<td style="width:33.3333%">
							<font style="font-size: 1.5em; font-weight: bold;">NinjaFirewall</font>
							<p><?php _e('Web Application Firewall<br />for PHP and WordPress.', 'nfwplus') ?></p>
							<i style="border-radius:20%;display:inline-block;height:150px;vertical-align:middle;width:150px;border:5px solid #FFF;box-shadow: -2px 3px 3px #999 inset;background:transparent url('<?php echo plugins_url() ?>/nfwplus/images/logo_pro_80.png') no-repeat scroll center center;background-color:#F8F8F8;"></i>
							<p><a href="http://nintechnet.com/ninjafirewall/" class="button-primary" style="color:#FFF;background-color:#449D44;border-color:#398439;text-shadow:none"><?php _e('Free Download', 'nfwplus') ?></a></p>
						</td>
						<td style="width:33.3333%">
							<font style="font-size: 1.5em; font-weight: bold;">NinjaMonitoring</font>
							<p><?php _e('Website Monitoring<br />for just $4.99/month.', 'nfwplus') ?></p>
							<i style="border-radius:20%;display:inline-block;height:150px;vertical-align:middle;width:150px;border:5px solid #FFF;box-shadow: -2px 3px 3px #999 inset;background:transparent url('<?php echo plugins_url() ?>/nfwplus/images/logo_nm_80.png') no-repeat scroll center center;background-color:#F8F8F8;"></i>
							<p><a href="http://nintechnet.com/ninjamonitoring/" class="button-primary" style="color:#FFF;background-color:#EC971F;border-color:#D58512;text-shadow:none"><?php _e('7-Day Free Trial', 'nfwplus') ?></a></p>
						</td>
						<td style="width:33.3333%">
							<font style="font-size: 1.5em; font-weight: bold;">NinjaRecovery</font>
							<p><?php _e('Malware removal<br />and hack recovery.', 'nfwplus') ?></p>
							<i style="border-radius:20%;display:inline-block;height:150px;vertical-align:middle;width:150px;border:5px solid #FFF;box-shadow: -2px 3px 3px #999 inset;background:transparent url('<?php echo plugins_url() ?>/nfwplus/images/logo_nr_80.png') no-repeat scroll center center;background-color:#F8F8F8;"></i>
							<p><a href="http://nintechnet.com/ninjarecovery/" class="button-primary" style="color:#FFF;background-color:#C9302C;border-color:#AC2925;text-shadow:none"><?php _e('Clean Your Site!', 'nfwplus') ?></a></p>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
<?php
	echo '
		<br />
		<br />
		<input class="button-secondary" type="button" value="' . __('Changelog', 'nfwplus') . '" onclick="show_table(12);">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input class="button-primary" type="button" value="' . __('Spread the word!', 'nfwplus') . '" onclick="show_table(11);">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input class="button-secondary" type="button" value="' . __('Referral Program', 'nfwplus') . '" onclick="show_table(14);">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input class="button-secondary" type="button" value="' . __('System Info', 'nfwplus') . '" onclick="show_table(13);">
		<br />
		<br />

		<div id="11">
			<table style="text-align:justify;border:2px #749BBB solid;padding:6px;border-radius:4px" border="0" width="500">
				<tr style="text-align:center">
					<td><a href="http://www.facebook.com/sharer.php?u=https://nintechnet.com/" target="_blank"><img src="' . plugins_url() . '/nfwplus/images/facebook.png" width="90" height="90" style="border: 0px solid #DFDFDF;padding:0px;-moz-box-shadow:-3px 5px 5px #999;-webkit-box-shadow:-3px 5px 5px #999;box-shadow:-3px 5px 5px #999;background-color:#FCFCFC;"></a></td>
					<td><a href="https://plus.google.com/share?url=https://nintechnet.com/" target="_blank"><img src="' . plugins_url() . '/nfwplus/images/google.png" width="90" height="90" style="border: 0px solid #DFDFDF;padding:0px;-moz-box-shadow:-3px 5px 5px #999;-webkit-box-shadow:-3px 5px 5px #999;box-shadow:-3px 5px 5px #999;background-color:#FCFCFC;"></a></td>
					<td><a href="http://twitter.com/share?text=NinjaFirewall&url=https://nintechnet.com/" target="_blank"><img src="' . plugins_url() . '/nfwplus/images/twitter.png" width="90" height="90" style="border: 0px solid #DFDFDF;padding:0px;-moz-box-shadow:-3px 5px 5px #999;-webkit-box-shadow:-3px 5px 5px #999;box-shadow:-3px 5px 5px #999;background-color:#FCFCFC;"></a></td>
					<td><a href="https://twitter.com/nintechnet"><img border="0" src="'. plugins_url() .'/nfwplus/images/twitter_ntn.png" width="116" height="28" target="_blank"></a></td>
				</tr>
			</table>
		</div>

		<div id="12" style="display:none;">
			<table width="500">
				<tr>
					<td>
						<textarea class="small-text code" cols="60" rows="8" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">' . htmlspecialchars($changelog) . '</textarea>
					</td>
				</tr>
			</table>
		</div>

		<div id="13" style="display:none;">
			<table id="13" border="0" style="text-align:justify;border:2px #749BBB solid;padding:6px;border-radius:4px" width="500">
				<tr valign="top"><td width="47%;" align="right">REMOTE_ADDR</td><td width="3%">&nbsp;</td><td width="50%" align="left">' . htmlspecialchars($_SERVER['REMOTE_ADDR']) . '</td></tr>
				<tr valign="top"><td width="47%;" align="right">SERVER_ADDR</td><td width="3%">&nbsp;</td><td width="50%" align="left">' . htmlspecialchars($_SERVER['SERVER_ADDR']) . '</td></tr>
				<tr valign="top"><td width="47%;" align="right">SERVER_NAME</td><td width="3%">&nbsp;</td><td width="50%" align="left">' . htmlspecialchars($_SERVER['SERVER_NAME']) . '</td></tr>
				<tr valign="top"><td width="47%;" align="right">HTTP_HOST</td><td width="3%">&nbsp;</td><td width="50%" align="left">' . htmlspecialchars($_SERVER['HTTP_HOST']) . '</td></tr>';

if ( PHP_VERSION ) {
	echo '<tr valign="top"><td width="47%;" align="right">' . __('PHP version', 'nfwplus') . '</td><td width="3%">&nbsp;</td><td width="50%" align="left">'. PHP_VERSION . ' (';
	if ( defined('HHVM_VERSION') ) {
		echo 'HHVM';
	} else {
		echo strtoupper(PHP_SAPI);
	}
	echo ')</td></tr>';
}
if ( $_SERVER['SERVER_SOFTWARE'] ) {
	echo '<tr valign="top"><td width="47%;" align="right">' . __('HTTP server', 'nfwplus') . '</td><td width="3%">&nbsp;</td><td width="50%" align="left">' . htmlspecialchars($_SERVER['SERVER_SOFTWARE']) . '</td></tr>';
}
if ( PHP_OS ) {
	echo '<tr valign="top"><td width="47%;" align="right">' . __('Operating System', 'nfwplus') . '</td><td width="3%">&nbsp;</td><td width="50%" align="left">' . PHP_OS . '</td></tr>';
}
if ( $load = sys_getloadavg() ) {
	echo '<tr valign="top"><td width="47%;" align="right">' . __('Load Average', 'nfwplus') . '</td><td width="3%">&nbsp;</td><td width="50%" align="left">' . $load[0] . ', '. $load[1] . ', '. $load[2] . '</td></tr>';
}
if (! preg_match( '/^win/i', PHP_OS ) ) {
	$MemTotal = $MemFree = $Buffers = $Cached = 0;
	$data = @explode( "\n", `cat /proc/meminfo` );
	foreach ( $data as $line ) {
		if ( preg_match( '/^MemTotal:\s+?(\d+)\s/', $line, $match ) ) {
			$MemTotal = $match[1] / 1024;
		} elseif ( preg_match( '/^MemFree:\s+?(\d+)\s/', $line, $match ) ) {
			$MemFree = $match[1];
		} elseif ( preg_match( '/^Buffers:\s+?(\d+)\s/', $line, $match ) ) {
			$Buffers = $match[1];
		} elseif ( preg_match( '/^Cached:\s+?(\d+)\s/', $line, $match ) ) {
			$Cached = $match[1];
		}
	}
	$free = ( $MemFree + $Buffers + $Cached ) / 1024;
	if ( $free ) {
		echo '<tr valign="top"><td width="47%;" align="right">' . __('RAM', 'nfwplus') . '</td><td width="3%">&nbsp;</td><td width="50%" align="left">' . number_format( $free ) . ' ' . __('MB free', 'nfwplus') . ' / '. number_format( $MemTotal ) . ' ' . __('MB total', 'nfwplus') . '</td></tr>';
	}

	$cpu = array_filter( @explode( "\n", `egrep 'model name|cpu cores' /proc/cpuinfo` ) );
	if (! empty( $cpu[0] ) ) {
		$cpu_tot = count( $cpu ) / 2;
		$core_tot = array_pop( $cpu );
		$core_tot = preg_replace( '/^.+(\d+)/', '$1', $core_tot );
		echo '<tr valign="top"><td width="47%;" align="right">' . _n('Processor', 'Processors', $cpu_tot, 'nfwplus') . '</td><td width="3%">&nbsp;</td><td width="50%" align="left">' . $cpu_tot .' ('. _n('CPU core:', 'CPU cores:', $core_tot, 'nfwplus') .' '. $core_tot . ')</td></tr>';
		echo '<tr valign="top"><td width="47%;" align="right">' . __('CPU model', 'nfwplus') . '</td><td width="3%">&nbsp;</td><td width="50%" align="left">' . str_replace ("model name\t:", '', htmlspecialchars($cpu[0])) . '</td></tr>';
	}
}

echo '
			</table>
		</div>

		<div id="14" style="display:none;">
			<table style="text-align:justify;border:2px #749BBB solid;padding:6px;border-radius:4px" width="500">
				<tr>
					<td>
						' . sprintf(__('By joining our NinjaFirewall Referral Program you can earn up to %s for every payment made by a user who signs up using your personal referral link.', 'nfwplus'), '20%') .
						'<p>' . sprintf(__('For more info and subscription, please check our <a href="%s">Referral Program page</a>.', 'nfwplus'), 'http://nintechnet.com/referral/') . '</p>
					</td>
				</tr>
			</table>
		</div>

	</center>
</div>';
/* ================================================================== */
// EOF
