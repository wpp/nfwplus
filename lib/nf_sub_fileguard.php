<?php
/*
 +=====================================================================+
 | NinjaFirewall (WP+ Edition)                                         |
 |                                                                     |
 | (c) NinTechNet - http://nintechnet.com/                             |
 +=====================================================================+
 | REVISION: 2016-05-12 20:05:35                                       |
 +=====================================================================+ i18n+ / sa
*/

if (! defined( 'NFW_ENGINE_VERSION' ) ) { die( 'Forbidden' ); }

// Block immediately if user is not allowed :
nf_not_allowed( 'block', __LINE__ );

$nfw_options = nfw_get_option( 'nfw_options' );

?>
<script>
function toggle_table(off) {
	if ( off == 1 ) {
		jQuery("#fg_table").slideDown();
	} else if ( off == 2 ) {
		jQuery("#fg_table").slideUp();
	}
	return;
}
function is_number(id) {
	var e = document.getElementById(id);
	if (! e.value ) { return }
	if (! /^[1-9][0-9]?$/.test(e.value) ) {
		alert("<?php echo esc_js( __('Please enter a number from 1 to 99.', 'nfwplus') ) ?>");
		e.value = e.value.substring(0, e.value.length-1);
	}
}
function check_fields() {
	if (! document.nfwfilefuard.elements["nfw_options[fg_mtime]"]){
		alert("<?php echo esc_js( __('Please enter a number from 1 to 99.', 'nfwplus') ) ?>");
		return false;
	}
	return true;
}
</script>

<div class="wrap">
	<div style="width:33px;height:33px;background-image:url(<?php echo plugins_url() ?>/nfwplus/images/ninjafirewall_32.png);background-repeat:no-repeat;background-position:0 0;margin:7px 5px 0 0;float:left;"></div>
	<h1><?php _e('File Guard', 'nfwplus') ?></h1>
<?php
if ( defined('NFW_WPWAF') ) {
	?>
	<div class="notice-warning notice is-dismissible">
		<p><?php printf( __('You are running NinjaFirewall in <i>WordPress WAF</i> mode. The %s feature will be limited to a few WordPress files only (e.g., index.php, wp-login.php, xmlrpc.php, admin-ajax.php, wp-load.php etc). If you want it to apply to any PHP script, you will need to run NinjaFirewall in %s mode.', 'nfwplus'), 'File Guard', '<a href="https://blog.nintechnet.com/full_waf-vs-wordpress_waf/">Full WAF</a>') ?></p>
	</div>
	<?php
}

// Ensure cache folder is writable :
if (! is_writable( NFW_LOG_DIR . '/nfwlog/cache/') ) {
	sprintf( __('The cache directory %s is not writable. Please change its permissions (0777 or equivalent).', 'nfwplus'), '('. htmlspecialchars(NFW_LOG_DIR) . '/nfwlog/cache/)' ) . '</p></div>';
}

// Saved ?
if ( isset( $_POST['nfw_options']) ) {
	if ( empty($_POST['nfwnonce']) || ! wp_verify_nonce($_POST['nfwnonce'], 'fileguard_save') ) {
		wp_nonce_ays('fileguard_save');
	}
	nf_sub_fileguard_save();
	$nfw_options = nfw_get_option( 'nfw_options' );
	echo '<div class="updated notice is-dismissible"><p>' . __('Your changes have been saved.', 'nfwplus') .'</p></div>';
}

if ( empty($nfw_options['fg_enable']) ) {
	$nfw_options['fg_enable'] = 0;
} else {
	$nfw_options['fg_enable'] = 1;
}
if ( empty($nfw_options['fg_mtime']) || ! preg_match('/^[1-9][0-9]?$/', $nfw_options['fg_mtime']) ) {
	$nfw_options['fg_mtime'] = 10;
}
if ( empty($nfw_options['fg_exclude']) ) {
	$fg_exclude = '';
} else {
	$tmp = str_replace('|', ',', $nfw_options['fg_exclude']);
	$fg_exclude = preg_replace( '/\\\([`.\\/\\\+*?\[^\]$(){}=!<>:-])/', '$1', $tmp );
}
?>
<br />
<form method="post" name="nfwfilefuard" onSubmit="return check_fields();">
	<?php wp_nonce_field('fileguard_save', 'nfwnonce', 0); ?>
	<table class="form-table">
		<tr style="background-color:#F9F9F9;border: solid 1px #DFDFDF;">
			<th scope="row"><?php _e('Enable File Guard', 'nfwplus') ?></th>
			<td align="left">
			<label><input type="radio" id="fgenable" name="nfw_options[fg_enable]" value="1"<?php checked($nfw_options['fg_enable'], 1) ?> onclick="toggle_table(1);">&nbsp;<?php _e('Yes (recommended)', 'nfwplus') ?></label>
			</td>
			<td align="left">
			<label><input type="radio" name="nfw_options[fg_enable]" value="0"<?php checked($nfw_options['fg_enable'], 0) ?> onclick="toggle_table(2);">&nbsp;<?php _e('No', 'nfwplus') ?></label>
			</td>
		</tr>
	</table>

	<br />
	<div id="fg_table"<?php echo $nfw_options['fg_enable'] == 1 ? '' : ' style="display:none"' ?>>
		<table class="form-table" border="0">
			<tr valign="top">
				<th scope="row"><?php _e('Real-time detection', 'nfwplus') ?></th>
				<td align="left">
				<?php
					printf( __('Monitor file activity and send an alert when someone is accessing a PHP script that was modified or created less than %s hour(s) ago.', 'nfwplus'), '<input maxlength="2" size="2" value="'. $nfw_options['fg_mtime'] .'" name="nfw_options[fg_mtime]" id="mtime" onkeyup="is_number(\'mtime\')" type="text" />');
				?>
				</td>
			</tr>
			<tr>
				<th scope="row"><?php _e('Exclude the following files/folders (optional)', 'nfwplus') ?></th>
				<td align="left"><input class="large-text" type="text" maxlength="255" name="nfw_options[fg_exclude]" value="<?php echo htmlspecialchars( $fg_exclude ); ?>" placeholder="<?php _e('e.g.,', 'nfwplus') ?> /foo/bar/cache/ <?php _e('or', 'nfwplus') ?> /cache/" /><br /><span class="description"><?php _e('Full or partial case-sensitive string(s), max. 255 characters. Multiple values must be comma-separated', 'nfwplus') ?> (<code>,</code>).</span></td>
			</tr>
		</table>
	</div>
	<br />
	<input class="button-primary" type="submit" name="Save" value="<?php _e('Save File Guard options', 'nfwplus') ?>" />
</form>
</div>
<?php

/* ================================================================== */

function nf_sub_fileguard_save() {

	// Block immediately if user is not allowed :
	nf_not_allowed( 'block', __LINE__ );

	$nfw_options = nfw_get_option( 'nfw_options' );

	// Disable or enable the File Guard ?
	if ( empty($_POST['nfw_options']['fg_enable']) ) {
		$nfw_options['fg_enable'] = 0;
	} else {
		$nfw_options['fg_enable'] = $_POST['nfw_options']['fg_enable'];
	}

	if ( empty($_POST['nfw_options']['fg_mtime']) || ! preg_match('/^[1-9][0-9]?$/', $_POST['nfw_options']['fg_mtime']) ) {
		$nfw_options['fg_mtime'] = 10;
	} else {
		$nfw_options['fg_mtime'] = $_POST['nfw_options']['fg_mtime'];
	}

	if ( empty($_POST['nfw_options']['fg_exclude']) || strlen($_POST['nfw_options']['fg_exclude']) > 255 ) {
		$nfw_options['fg_exclude'] = '';
	} else {
		$exclude = '';
		$fg_exclude =  explode(',', $_POST['nfw_options']['fg_exclude'] );
		foreach ($fg_exclude as $path) {
			if ( $path ) {
				// No space characteres allowed:
				$path = str_replace( array(' ', '\\', '|'), '', $path);
				$exclude .= preg_quote( rtrim($path, ','), '`') . '|';
			}
		}
		$nfw_options['fg_exclude'] = rtrim($exclude, '|');
	}

	// Update :
	nfw_update_option( 'nfw_options', $nfw_options );

}
/* ================================================================== */
// EOF
