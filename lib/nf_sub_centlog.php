<?php
/*
 +=====================================================================+
 | NinjaFirewall (WP+ Edition)                                         |
 |                                                                     |
 | (c) NinTechNet - http://nintechnet.com/                             |
 +=====================================================================+ i18n+ / sa
*/

if (! defined( 'NFW_ENGINE_VERSION' ) ) { die( 'Forbidden' ); }

// Block immediately if user is not allowed :
nf_not_allowed( 'block', __LINE__ );

$nfw_options = nfw_get_option( 'nfw_options' );

if (! empty( $_POST['nfw_act']) ) {
	if ( empty($_POST['nfwnonce']) || ! wp_verify_nonce($_POST['nfwnonce'], 'cent_logs') ) {
		wp_nonce_ays('cent_logs');
	}
	if ( $_REQUEST['nfw_act'] == 'save_options') {
		nf_sub_centlog_save( $nfw_options );
		echo '<div class="updated notice is-dismissible"><p>' . __('Your changes have been saved.', 'nfwplus') . '</p></div>';
	}
	$nfw_options = nfw_get_option( 'nfw_options' );
}

echo '
<script>
function toogle_table(off) {
	if ( off == 1 ) {
		jQuery("#clogs_table").slideDown();
	} else if ( off == 2 ) {
		jQuery("#clogs_table").slideUp();
	}
	return;
}
function clear_pubkey() {
	document.getElementById("pubkey").innerHTML = "<font color=\"red\">'.
		 esc_js( __('Click the "Save Options" button to generate your new public key.', 'nfwplus') ) .
		'</font><p><span class=\"description\">' .
		 esc_js( __('You will need to upload that new key to the remote server(s).', 'nfwplus') ) .
		'</span></p>";
}

function ct_check_fields() {
	if ( document.getElementById("clogs_table").style.display == "none" ) { return; }
	// Secret key:
	var myseckey = document.ctform.elements["nfw_options[clogs_seckey]"];
	if (!  myseckey.value || !  myseckey.value.match( /^[\x20-\x7e]{30,100}$/ ) ) {
		alert("'. esc_js( __('Please enter a secret key, from 30 to 100 ASCII printable characters. It will be used to generate your public key.', 'nfwplus') ) . '");
		myseckey.focus();
		return false;
	}
	// Server IP:
	if (! document.ctform.elements["nfw_options[clogs_ip]"].value ) {
		alert("'. esc_js( __('Please enter this server IP address.', 'nfwplus') ) . '");
		document.ctform.elements["nfw_options[clogs_ip]"].focus();
		return false;
	}
	// URLs.
	if ( document.ctform.elements["nfw_options[clogs_urls]"].value.length < 10 ) {
		alert("'. esc_js( __('Please enter the remote websites URL.', 'nfwplus') ) . '");
		document.ctform.elements["nfw_options[clogs_urls]"].focus();
		return false;
	}
	return true;
}
</script>

<div class="wrap">
	<div style="width:33px;height:33px;background-image:url( ' . plugins_url() . '/nfwplus/images/ninjafirewall_32.png);background-repeat:no-repeat;background-position:0 0;margin:7px 5px 0 0;float:left;"></div>
	<h1>' . __('Centralized Logging', 'nfwplus') . '</h1>';

if ( empty($nfw_options['clogs_enable']) ) {
	$nfw_options['clogs_enable'] = 0;
} else {
	$nfw_options['clogs_enable'] = 1;
}

if ( empty( $nfw_options['clogs_ip'] ) || (! filter_var( $nfw_options['clogs_ip'], FILTER_VALIDATE_IP ) && $nfw_options['clogs_ip'] != '*' ) ) {
	$nfw_options['clogs_ip'] = $_SERVER['SERVER_ADDR'];
}

// Verify secret key syntax:
if (! empty( $nfw_options['clogs_seckey'] ) ) {
	$nfw_options['clogs_seckey'] = base64_decode( $nfw_options['clogs_seckey'] );
	if ( ! preg_match( '/^[\x20-\x7e]{30,100}$/', $nfw_options['clogs_seckey'] ) ) {
		$nfw_options['clogs_seckey'] = generate_clogs_seckey();
		$error_msg = __('Warning: Your previous secret key was either corrupted or missing. A new one, as well as a new public key, were created.', 'nfwplus');
	}
} else {
	$nfw_options['clogs_seckey'] = generate_clogs_seckey();
}

// List of URLs:
$urls = '';
if (! empty( $nfw_options['clogs_urls'] ) ) {
	$tmp = unserialize( $nfw_options['clogs_urls'] );
	if ( $tmp ) {
		foreach ($tmp as $url) {
			$urls .= htmlspecialchars( $url ) . "\n";
		}
	}
}
if ( empty( $urls ) && $nfw_options['clogs_enable'] == 1 ) {
	$error_msg = __('Please enter the remote websites URL.', 'nfwplus');
}

if (! empty( $error_msg ) ) {
	echo '<div class="error notice is-dismissible"><p>' . $error_msg . '</p></div>';
}

?>
<form method="post" name="ctform" onSubmit="return ct_check_fields()">
	<?php wp_nonce_field('cent_logs', 'nfwnonce', 0); ?>

	<table class="form-table">
		<tr style="background-color:#F9F9F9;border: solid 1px #DFDFDF;">
			<th scope="row"><?php _e('Enable Centralized Logging', 'nfwplus') ?></th>
			<td align="left">
			<label><input type="radio" id="fgenable" name="nfw_options[clogs_enable]" value="1"<?php checked($nfw_options['clogs_enable'], 1) ?> onclick="toogle_table(1);">&nbsp;<?php _e('Yes', 'nfwplus') ?></label>
			</td>
			<td align="left">
			<label><input type="radio" name="nfw_options[clogs_enable]" value="0"<?php checked($nfw_options['clogs_enable'], 0) ?> onclick="toogle_table(2);">&nbsp;<?php _e('No', 'nfwplus') ?></label>
			</td>
		</tr>
	</table>

	<br />

	<div id="clogs_table"<?php echo $nfw_options['clogs_enable'] == 1 ? '' : ' style="display:none"' ?>>
		<table class="form-table">
			<tr>
				<th scope="row"><?php _e('Secret key', 'nfwplus') ?></th>
				<td align="left">
					<input class="large-text" type="text" maxlength="100" name="nfw_options[clogs_seckey]" value="<?php echo htmlentities( $nfw_options['clogs_seckey'] ) ?>"  autocomplete="off" oninput="clear_pubkey();" />
					<p><span class="description"><?php _e('From 30 to 100 ASCII printable characters.', 'nfwplus') ?></span></p>
				</td>
			</tr>

			<tr>
				<th scope="row"><?php _e('This server\'s IP address', 'nfwplus'); echo ' ('. htmlspecialchars( $_SERVER['SERVER_ADDR'] ) . ')' ?></th>
				<td align="left">
					<input type="text" name="nfw_options[clogs_ip]" value="<?php echo htmlspecialchars( $nfw_options['clogs_ip'] ) ?>" placeholder="<?php _e('e.g.,', 'nfwplus') ?> 1.2.3.4"  oninput="clear_pubkey();" />
					<p><span class="description"><?php _e('Only this IP address (IPv4 or IPv6) will be allowed to connect to the remote websites. If you don\'t want to restrict the access by IP, enter the <code>*</code> character instead.', 'nfwplus') ?></span></p>
				</td>
			</tr>

			<tr>
				<th scope="row"><?php _e('Public key', 'nfwplus') ?></th>
				<td align="left" id="pubkey">
					<input type="text" class="large-text" value="<?php echo sha1( $nfw_options['clogs_seckey'] )  .':'. htmlspecialchars( $nfw_options['clogs_ip'] ) ?>" readonly />
					<p><span class="description"><?php printf( __('Add this key to the remote websites. <a href="%s">Consult our blog</a> for more info.', 'nfwplus'), 'https://blog.nintechnet.com/centralized-logging-with-ninjafirewall/' ) ?></span></p>
				</td>
			</tr>

			<tr>
				<th scope="row"><?php _e('Remote websites URL', 'nfwplus') ?></th>
				<td align="left">
					<textarea name="nfw_options[clogs_urls]" cols="40" rows="10" placeholder="http://example.org/index.php" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"><?php echo $urls ?></textarea>
					<br /><span class="description"><?php _e('Enter one URL per line, including the protocol (<code>http://</code> or <code>https://</code>). Only ASCII URLs are accepted.', 'nfwplus') ?></span>
				</td>
			</tr>

		</table>
	</div>

	<br />

	<input type="hidden" name="nfw_act" value="save_options" />
	<input class="button-primary" name="clsaveopt" value="<?php _e('Save Options', 'nfwplus') ?>" type="submit" />
</form>

</div>
<?php

/* ================================================================== */

function nf_sub_centlog_save( $nfw_options ) {

	if ( empty( $_POST['nfw_options']['clogs_enable'] ) ) {
		$nfw_options['clogs_enable'] = 0;

	} else {
		$nfw_options['clogs_enable'] = 1;
		if ( empty( $_POST['nfw_options']['clogs_seckey'] ) ||
			! preg_match( '/^[\x20-\x7e]{30,100}$/', $_POST['nfw_options']['clogs_seckey'] ) ) {
			$nfw_options['clogs_seckey'] = '';

		} else {
			// Prevent WP from adding slashes:
			$nfw_options['clogs_seckey'] = base64_encode( stripslashes( $_POST['nfw_options']['clogs_seckey'] ) );
		}

		if ( empty( $_POST['nfw_options']['clogs_ip'] ) ||
			(! filter_var( $_POST['nfw_options']['clogs_ip'], FILTER_VALIDATE_IP ) &&
			$_POST['nfw_options']['clogs_ip'] != '*' ) ) {
			$nfw_options['clogs_ip'] = $_SERVER['SERVER_ADDR'];
		} else {
			$nfw_options['clogs_ip'] = $_POST['nfw_options']['clogs_ip'];
		}

		if (! empty( $_POST['nfw_options']['clogs_urls'] ) ) {
			$res = explode( "\n", $_POST['nfw_options']['clogs_urls'] );
			$urls = array_values( array_filter( array_map( 'trim', $res ) ) );
			$res = array();
			foreach ( $urls as $url ) {
				if ( filter_var( $url, FILTER_VALIDATE_URL ) ) {
					$res[] = $url;
				}
			}
			if ( $res ) {
				sort( $res );
				$nfw_options['clogs_urls'] = serialize( $res );
			}
		}
	}

	nfw_update_option( 'nfw_options', $nfw_options);

}

/* ================================================================== */

function generate_clogs_seckey() {

	$key = '';
	for ( $i = 0; $i < 40; $i++ ) {
    $key .= chr( mt_rand( 33, 126 ) );
  }
  return $key;

}

/* ================================================================== */
// EOF
