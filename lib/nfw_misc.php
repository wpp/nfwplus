<?php
/*
 +=====================================================================+
 | NinjaFirewall (WP+ Edition)                                         |
 |                                                                     |
 | (c) NinTechNet - http://nintechnet.com/                             |
 +=====================================================================+ i18n+ / sa
*/

if (! defined( 'NFW_ENGINE_VERSION' ) ) { die( 'Forbidden' ); }

/* ================================================================== */

function nfw_garbage_collector() {

	// Clean/delete cache folder & temp files:

	$nfw_options = nfw_get_option( 'nfw_options' );
	$path = NFW_LOG_DIR . '/nfwlog/cache/';
	$now = time();

	// Make sure the cache folder exists, i.e, we have been
	// through the whole installation process:
	if (! is_dir( $path ) ) {
		return;
	}

	// Don't do anything if the cache folder
	// was cleaned up less than 5 minutes ago:
	$gc = $path . 'garbage_collector.php';
	if ( file_exists( $gc ) ) {
		$nfw_mtime = filemtime( $gc ) ;
		if ( $now - $nfw_mtime < 300 ) {
			return;
		}
		unlink( $gc );
	}
	touch( $gc );

	// File Guard temp files:
	$glob = glob( $path . "fg_*.php" );
	if ( is_array( $glob ) ) {
		foreach( $glob as $file ) {
			$nfw_ctime = filectime( $file );
			// Delete it, if it is too old:
			if ( $now - $nfw_options['fg_mtime'] * 3660 > $nfw_ctime ) {
				unlink( $file );
			}
		}
	}

	// Anti-Malware signatures: delete them if older than 1 hour:
	$nfw_malsigs = NFW_LOG_DIR . '/nfwlog/cache/malscan.txt';
	if ( file_exists( $nfw_malsigs ) ) {
		if ( time() - filemtime( $nfw_malsigs ) > 3600 ) {
			unlink( $nfw_malsigs );
		}
	}

	// Live Log:
	$nfw_livelogrun = $path . 'livelogrun.php';
	if ( file_exists( $nfw_livelogrun ) ) {
		$nfw_mtime = filemtime( $nfw_livelogrun );
		// If the file was not accessed for more than 100s, we assume
		// the admin has stopped using live log from WordPress
		// dashboard (refresh rate is max 45 seconds):
		if ( $now - $nfw_mtime > 100 ) {
			unlink( $nfw_livelogrun );
		}
	}
	// If the log was not modified for the past 10mn, we delete it as well:
	$nfw_livelog = $path . 'livelog.php';
	if ( file_exists( $nfw_livelog ) ) {
		$nfw_mtime = filemtime( $nfw_livelog ) ;
		if ( $now - $nfw_mtime > 600 ) {
			unlink( $nfw_livelog );
		}
	}

	// Flush the rate-limit cache:
	$glob = glob( $path . "rl.*.php" );
	if ( is_array( $glob ) ) {
		foreach( $glob as $file ) {
			$nfw_mtime = filemtime( $file );
			if ( $now - $nfw_mtime > $nfw_options['ac_rl_time'] ) {
				unlink( $file );
			}
		}
	}

	// If shared memory is disabled, make sure the memory segment
	// was deleted, otherwise the firewall would keep using it:
	if ( empty( $nfw_options['shmop'] ) ) {
		nfw_shm_delete(0);
	}
}

/* ================================================================== */

function nfw_get_blogtimezone() {

	// Try to get the user timezone from WP configuration...
	$tzstring = get_option( 'timezone_string' );
	// ...or from PHP...
	if (! $tzstring ) {
		$tzstring = ini_get( 'date.timezone' );
		// ...or use UTC :
		if (! $tzstring ) {
			$tzstring = 'UTC';
		}
	}
	// Set the timezone :
	date_default_timezone_set( $tzstring );

}
/* ================================================================== */

function nfw_select_ip() {

	// Check which IP we are supposed to use (set up by the user from
	// the Access Control > Source IP page (WP+ Edition only):

	// Note: Although this was already done by the firewall,
	// we check it here again in the case the firewall is not loaded.

	$nfw_options = nfw_get_option( 'nfw_options' );

	if ( empty($nfw_options['ac_ip']) || $nfw_options['ac_ip'] == 1 ) {
		// Ensure we have a proper and single IP (a user may use the .htninja file
		// to redirect HTTP_X_FORWARDED_FOR, which may contain more than one IP,
		// to REMOTE_ADDR).
		if (strpos($_SERVER['REMOTE_ADDR'], ',') !== false) {
			$nfw_match = array_map('trim', @explode(',', $_SERVER['REMOTE_ADDR']));
			foreach($nfw_match as $nfw_m) {
				if ( filter_var($nfw_m, FILTER_VALIDATE_IP) )  {
					// Fix it, so that WP and other plugins can use it:
					$_SERVER['REMOTE_ADDR'] = $nfw_m;
					break;
				}
			}
		}
	} elseif ( $nfw_options['ac_ip'] == 2 && ! empty($_SERVER['HTTP_X_FORWARDED_FOR']) ) {
		$nfw_match = array_map('trim', @explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']));
		foreach($nfw_match as $nfw_m) {
			if ( filter_var($nfw_m, FILTER_VALIDATE_IP) )  {
				define( 'NFW_REMOTE_ADDR', $nfw_m);
				break;
			}
		}
	} elseif ( $nfw_options['ac_ip'] == 3 && ! empty($nfw_options['ac_ip_2']) && ! empty($_SERVER[$nfw_options['ac_ip_2']]) ) {
		$nfw_match = array_map('trim', @explode(',', $_SERVER[$nfw_options['ac_ip_2']]));
		foreach($nfw_match as $nfw_m) {
			if ( filter_var($nfw_m, FILTER_VALIDATE_IP) )  {
				define( 'NFW_REMOTE_ADDR', $nfw_m);
				break;
			}
		}
	}
	// Fall back to REMOTE_ADDR if nothing found :
	if (! defined('NFW_REMOTE_ADDR') ) {
		define('NFW_REMOTE_ADDR', htmlspecialchars($_SERVER['REMOTE_ADDR']) );
	}
}
/* ================================================================== */

function nfw_is_goodguy( $role ) {

	// Check whether the user should be whitelisted :

	$nfw_options = nfw_get_option( 'nfw_options' );

	// If we don't know its role yet, we try to find it out :
	if ( empty($role) ) {
		$current_user = wp_get_current_user();
		$role = isset($current_user->roles[0]) ? $current_user->roles[0] : '';
	}

	if (! empty($nfw_options['ac_role'][0]) && $role == 'administrator' ) {
		return 1;
	} elseif (! empty($nfw_options['ac_role'][1]) && $role == 'editor' ) {
		return 2;
	} elseif (! empty($nfw_options['ac_role'][2]) && $role == 'author' ) {
		return 3;
	} elseif (! empty($nfw_options['ac_role'][3]) && $role == 'contributor' ) {
		return 4;
	} elseif (! empty($nfw_options['ac_role'][4]) && $role == 'subscriber' ) {
		return 5;
	}
	return 0;
}

/* ================================================================== */

function nfw_send_loginemail( $user_login, $whoami ) {

	$nfw_options = nfw_get_option( 'nfw_options' );

	if ( ( is_multisite() ) && ( $nfw_options['alert_sa_only'] == 2 ) ) {
		$recipient = get_option('admin_email');
	} else {
		$recipient = $nfw_options['alert_email'];
	}

	$subject = '[NinjaFirewall] ' . __('Alert: WordPress console login', 'nfwplus');
	if ( is_multisite() ) {
		$url = __('-Blog :', 'nfwplus') .' ' . network_home_url('/') . "\n\n";
	} else {
		$url = __('-Blog :', 'nfwplus') .' ' . home_url('/') . "\n\n";
	}
	if (! empty( $whoami ) ) {
		$whoami = " ($whoami)";
	}
	$message = __('Someone just logged in to your WordPress admin console:', 'nfwplus') . "\n\n".
				__('-User :', 'nfwplus') .' ' . $user_login . $whoami . "\n" .
				__('-IP   :', 'nfwplus') .' ' . NFW_REMOTE_ADDR . "\n" .
				__('-Date :', 'nfwplus') .' ' . ucfirst(date_i18n('F j, Y @ H:i:s')) . ' (UTC '. date('O') . ")\n" .
				$url .
				'NinjaFirewall (WP+ Edition) - http://ninjafirewall.com/' . "\n" .
				__('Help Desk', 'nfwplus') . ': https://nintechnet.com/helpdesk/' . "\n";
	wp_mail( $recipient, $subject, $message );

}
/* ================================================================== */

function is_nfw_enabled() {

	// Checks whether NF is enabled and/or active and/or debugging mode :

	$nfw_options = nfw_get_option( 'nfw_options' );

	// Check whether NF is running.

	// No communication from the firewall :
	if (! defined('NFW_STATUS') ) {
		define('NF_DISABLED', 10);
		return;
	}

	// NF was disabled by the admin :
	if ( isset($nfw_options['enabled']) && $nfw_options['enabled'] == '0' ) {
		define('NF_DISABLED', 9);
		return;
	}

	// There is another instance of NinjaFirewall firewall running,
	// maybe in the parent directory:
	if (NFW_STATUS == 20 || NFW_STATUS == 22 || NFW_STATUS == 23) {
		define('NF_DISABLED', 10);
		return;
	}

	// OK :
	if (NFW_STATUS == 21) {
		define('NF_DISABLED', 0);
		return;
	}

	// Error :
	define('NF_DISABLED', NFW_STATUS);
	return;

}

/* ================================================================== */

function nfw_admin_notice(){	// i18n

	// Display a big red warning and returned an error if:
	// -the firewall is not enabled.
	// -log dir does not exist or is not writable.

	// We don't display any fatal error message to users :
	if (nf_not_allowed( 0, __LINE__ ) ) { return; }

	if (! defined('NF_DISABLED') ) {
		is_nfw_enabled();
	}

	// Ensure we have our cache/log folder, or attempt to create it :
	if (! file_exists(NFW_LOG_DIR . '/nfwlog') ) {
		@mkdir( NFW_LOG_DIR . '/nfwlog', 0755);
		@touch( NFW_LOG_DIR . '/nfwlog/index.html' );
		@file_put_contents(NFW_LOG_DIR . '/nfwlog/.htaccess', "Order Deny,Allow\nDeny from all", LOCK_EX);
		if (! file_exists(NFW_LOG_DIR . '/nfwlog/cache') ) {
			@mkdir( NFW_LOG_DIR . '/nfwlog/cache', 0755);
			@touch( NFW_LOG_DIR . '/nfwlog/cache/index.html' );
			@file_put_contents(NFW_LOG_DIR . '/nfwlog/cache/.htaccess', "Order Deny,Allow\nDeny from all", LOCK_EX);
		}
	}
	if (! file_exists(NFW_LOG_DIR . '/nfwlog') ) {
		echo '<div class="error notice is-dismissible"><p><strong>' . __('NinjaFirewall error', 'nfwplus') . ' :</strong> ' .
			sprintf( __('%s directory cannot be created. Please review your installation and ensure that %s is writable.', 'nfwplus'), '<code>'. htmlspecialchars(NFW_LOG_DIR) .'/nfwlog/</code>',  '<code>/wp-content/</code>') . '</p></div>';
	}
	if (! is_writable(NFW_LOG_DIR . '/nfwlog') ) {
		echo '<div class="error notice is-dismissible"><p><strong>' . __('NinjaFirewall error', 'nfwplus') . ' :</strong> ' .
			sprintf( __('%s directory is read-only. Please review your installation and ensure that %s is writable.', 'nfwplus'), '<code>'. htmlspecialchars(NFW_LOG_DIR) .'/nfwlog/</code>', '<code>/nfwlog/</code>') . '</p></div>';
	}


	if (! NF_DISABLED) {
		// OK
		return;
	}

	// Don't display anything if we are looking at the main/options pages
	// (error message will be displayed already) or during the installation
	// process :
	if (isset($_GET['page']) && preg_match('/^(?:NinjaFirewall|nfsubopt)$/', $_GET['page']) ) {
		return;
	}

	$nfw_options = nfw_get_option('nfw_options');
	// If we cannot find options and if the firewall did not return
	// a #11 status code (corrupted DB/tables)...
	if ( empty($nfw_options['ret_code']) && NF_DISABLED != 11 ) {
		// ...we will assume that NinjaFirewall it is not installed yet :
		return;
	}

	if (! empty($GLOBALS['err_fw'][NF_DISABLED]) ) {
		$msg = $GLOBALS['err_fw'][NF_DISABLED];
	} else {
		$msg = __('unknown error', 'nfwplus') . ' #' . NF_DISABLED;
	}
	echo '<div class="error notice is-dismissible"><p><strong>' . __('NinjaFirewall fatal error:', 'nfwplus') . '</strong> ' . $msg .
		'. ' . __('Review your installation, your site is not protected.', 'nfwplus') . '</p></div>';
}

add_action('all_admin_notices', 'nfw_admin_notice');

/* ================================================================== */

function nfw_query( $query ) { // i18n

	$nfw_options = nfw_get_option( 'nfw_options' );
	// Return if not enabled, or if we are accessing the dashboard (e.g., /wp-admin/edit.php):
	if ( empty($nfw_options['enum_archives']) || empty($nfw_options['enabled']) || is_admin() ) {
		return;
	}
	if ( $query->is_main_query() && $query->is_author() ) {
		if ( $query->get('author_name') ) {
			$tmp = 'author_name=' . $query->get('author_name');
		} elseif ( $query->get('author') ) {
			$tmp = 'author=' . $query->get('author');
		} else {
			$tmp = 'author';
		}
		@session_destroy();
		$query->set('author_name', '0');
		nfw_log2('User enumeration scan (author archives)', $tmp, 2, 0);
		wp_redirect( home_url('/') );
		exit;
	}
}

if (! isset($_SESSION['nfw_goodguy']) ) {
	add_action('pre_get_posts','nfw_query');
}

/* ================================================================== */

// WP >= 4.7:
function nfwhook_rest_authentication_errors( $ret ) {

	if (! defined('NF_DISABLED') ) {
		is_nfw_enabled();
	}
	if ( NF_DISABLED ) { return $ret; }

	$nfw_options = nfw_get_option( 'nfw_options' );

	if (! empty( $nfw_options['no_restapi']) && ! isset($_SESSION['nfw_goodguy']) ) {
		nfw_log2( 'WordPress: Blocked access to the WP REST API', $_SERVER['REQUEST_URI'], 2, 0);
		return new WP_Error( 'nfw_rest_api_access_restricted', __('Forbidden access', 'nfwplus'), array('status' => $nfw_options['ret_code']) );
	}

	return $ret;
}
add_filter( 'rest_authentication_errors', 'nfwhook_rest_authentication_errors' );

/* ================================================================== */

function nfwhook_rest_request_before_callbacks( $res, $hnd, $req ) {

	if (! defined('NF_DISABLED') ) {
		is_nfw_enabled();
	}
	if ( NF_DISABLED ) { return $res; }

	$nfw_options = nfw_get_option( 'nfw_options' );

	if (! empty( $nfw_options['enum_restapi']) && ! isset($_SESSION['nfw_goodguy']) ) {

		if ( strpos( $req->get_route(), '/wp/v2/users' ) !== false && ! current_user_can('list_users') ) {
			nfw_log2('User enumeration scan (WP REST API)', $_SERVER['REQUEST_URI'], 2, 0);
			return new WP_Error('nfw_rest_api_access_restricted', __('Forbidden access', 'nfwplus'), array('status' => $nfw_options['ret_code']) );
		}
	}
	return $res;
}
add_filter('rest_request_before_callbacks', 'nfwhook_rest_request_before_callbacks', 999, 3);

/* ================================================================== */

function nfw_authenticate( $user ) { // i18n

	$nfw_options = nfw_get_option( 'nfw_options' );

	if ( empty( $nfw_options['enum_login']) || empty($nfw_options['enabled']) ) {
		return $user;
	}

	if ( is_wp_error( $user ) ) {
		if ( preg_match( '/^(?:in(?:correct_password|valid_username)|authentication_failed)$/', $user->get_error_code() ) ) {
			$user = new WP_Error( 'denied', sprintf( __( '<strong>ERROR</strong>: Invalid username or password.<br /><a href="%s">Lost your password</a>?', 'nfwplus' ), wp_lostpassword_url() ) );
			add_filter('shake_error_codes', 'nfw_err_shake');
		}
	}
	return $user;
}

add_filter( 'authenticate', 'nfw_authenticate', 90, 3 );

function nfw_err_shake( $shake_codes ) {
	// shake the login box :
	$shake_codes[] = 'denied';
	return $shake_codes;
}

/* ================================================================== */

function nfw_check_emailalert() {

	$nfw_options = nfw_get_option( 'nfw_options' );

	if ( is_multisite() && $nfw_options['alert_sa_only'] == 2 ) {
		$recipient = get_option('admin_email');
	} else {
		$recipient = $nfw_options['alert_email'];
	}

	global $current_user;
	$current_user = wp_get_current_user();

	// Check what it is :
	list( $a_1, $a_2, $a_3 ) = explode( ':', NFW_ALERT . ':' );

	// Shall we alert the admin ?
	if (! empty($nfw_options['a_' . $a_1 . $a_2]) ) {
		$alert_array = array(
			'1' => array (
				'0' => __('Plugin', 'nfwplus'), '1' => __('uploaded', 'nfwplus'),	'2' => __('installed', 'nfwplus'), '3' => __('activated', 'nfwplus'),
				'4' => __('updated', 'nfwplus'), '5' => __('deactivated', 'nfwplus'), '6' => __('deleted', 'nfwplus'), 'label' => __('Name', 'nfwplus')
			),
			'2' => array (
				'0' => __('Theme', 'nfwplus'), '1' => __('uploaded', 'nfwplus'), '2' => __('installed', 'nfwplus'), '3' => __('activated', 'nfwplus'),
				'4' => __('deleted', 'nfwplus'), 'label' => __('Name', 'nfwplus')
			),
			'3' => array (
				'0' => 'WordPress', '1' => __('upgraded', 'nfwplus'),	'label' => __('Version', 'nfwplus')
			)
		);

		if ( substr_count($a_3, ',') ) {
			$alert_array[$a_1][0] .= 's';
			$alert_array[$a_1]['label'] .= 's';
		}
		$subject = __('[NinjaFirewall] Alert: ', 'nfwplus') . $alert_array[$a_1][0] . ' ' . $alert_array[$a_1][$a_2];
		if ( is_multisite() ) {
			$url = __('-Blog : ', 'nfwplus') . network_home_url('/') . "\n\n";
		} else {
			$url = __('-Blog : ', 'nfwplus') . home_url('/') . "\n\n";
		}
		$message = __('NinjaFirewall has detected the following activity on your account:', 'nfwplus') . "\n\n".
			'-' . $alert_array[$a_1][0] . ' ' . $alert_array[$a_1][$a_2] . "\n" .
			'-' . $alert_array[$a_1]['label'] . ' : ' . $a_3 . "\n\n" .
			__('-User : ', 'nfwplus') . $current_user->user_login . ' (' . $current_user->roles[0] . ")\n" .
			__('-IP   : ', 'nfwplus') . NFW_REMOTE_ADDR . "\n" .
			__('-Date :', 'nfwplus') .' '. date_i18n('F j, Y @ H:i:s O') ."\n" .
			$url .
			'NinjaFirewall (WP+ Edition) - http://ninjafirewall.com/' . "\n" .
			'Help Desk: https://nintechnet.com/helpdesk/' . "\n";

		wp_mail( $recipient, $subject, $message );

		if (! empty($nfw_options['a_41']) ) {
			nfw_log2(
				$alert_array[$a_1][0] . ' ' . $alert_array[$a_1][$a_2] . ' by '. $current_user->user_login,
				$alert_array[$a_1]['label'] . ': ' . $a_3,
				6,
				0
			);
		}

	}
}

/* ================================================================== */

function nf_check_dbdata() {

	$nfw_options = nfw_get_option( 'nfw_options' );

	// Don't do anything if NinjaFirewall is disabled or DB monitoring option is off :
	if ( empty( $nfw_options['enabled'] ) || empty($nfw_options['a_51']) ) { return; }

	if ( is_multisite() ) {
		global $current_blog;
		$nfdbhash = NFW_LOG_DIR .'/nfwlog/cache/nfdbhash.'. $current_blog->site_id .'-'. $current_blog->blog_id .'.php';
	} else {
		global $blog_id;
		$nfdbhash = NFW_LOG_DIR .'/nfwlog/cache/nfdbhash.'. $blog_id .'.php';
	}

	$adm_users = nf_get_dbdata();
	if (! $adm_users) { return; }

	if (! file_exists($nfdbhash) ) {
		// We don't have any hash yet, let's create one and quit
		// (md5 is faster than sha1 or crc32 with long strings) :
		@file_put_contents( $nfdbhash, md5( serialize( $adm_users) ), LOCK_EX);
		return;
	}

	$old_hash = trim (file_get_contents($nfdbhash) );

	// Compare both hashes :
	if ( $old_hash == md5( serialize($adm_users)) ) {
		return;
	} else {
		$fstat = stat($nfdbhash);
		// We don't want to spam the admin, do we ?
		if ( ( time() - $fstat['mtime']) < 60 ) {
			return;
		}

		// Save the new hash :
		$tmp = @file_put_contents( $nfdbhash, md5( serialize( $adm_users) ), LOCK_EX);
		if ( $tmp === FALSE ) {
			return;
		}

		// Send an email to the admin :
		if ( ( is_multisite() ) && ( $nfw_options['alert_sa_only'] == 2 ) ) {
			$recipient = get_option('admin_email');
		} else {
			$recipient = $nfw_options['alert_email'];
		}

		$subject = __('[NinjaFirewall] Alert: Database changes detected', 'nfwplus');
		$message = __('NinjaFirewall has detected that one or more administrator accounts were modified in the database:', 'nfwplus') . "\n\n";
		if ( is_multisite() ) {
			$message .=__('Blog: ', 'nfwplus') . network_home_url('/') . "\n";
		} else {
			$message .=__('Blog: ', 'nfwplus') . home_url('/') . "\n";
		}
		$message.= __('User IP:', 'nfwplus') .' '. NFW_REMOTE_ADDR . "\n";
		$message.= __('Date : ', 'nfwplus') . date_i18n('F j, Y @ H:i:s') . ' (UTC '. date('O') . ")\n\n";
		$message.= sprintf(__('Total administrators : %s', 'nfwplus'), count($adm_users) ) . "\n\n";
		foreach( $adm_users as $obj => $adm ) {
			$message.= 'Admin ID : ' . $adm->ID . "\n";
			$message.= '-user_login : ' . $adm->user_login . "\n";
			$message.= '-user_nicename : ' . $adm->user_nicename . "\n";
			$message.= '-user_email : ' . $adm->user_email . "\n";
			$message.= '-user_registered : ' . $adm->user_registered . "\n";
			$message.= '-display_name : ' . $adm->display_name . "\n\n";
		}
		$message.= "\n" . __('If you cannot see any modifications in the above fields, it is likely that the administrator password was changed.', 'nfwplus'). "\n\n";
		$message.= 	'NinjaFirewall (WP+ Edition) - http://ninjafirewall.com/' . "\n" .
						'Help Desk: https://nintechnet.com/helpdesk/' . "\n";
		wp_mail( $recipient, $subject, $message );

		// Log event if required :
		if (! empty($nfw_options['a_41']) ) {
			nfw_log2('Database changes detected', 'administrator account', 4, 0);
		}
	}

}

/* ================================================================== */

function nf_get_dbdata() {

	return get_users(
		array( 'role' => 'administrator',
			'fields' => array(
				'ID', 'user_login', 'user_pass', 'user_nicename',
				'user_email', 'user_registered', 'display_name'
			)
		)
	);

}

/* ================================================================== */

function nfwhook_update_user_meta( $user_id, $meta_key, $meta_value, $prev_value ) {

	nfwhook_user_meta( $meta_key, $meta_value, $prev_value );

}
add_filter('update_user_meta', 'nfwhook_update_user_meta', 1, 4);

/* ================================================================== */

function nfwhook_add_user_meta( $user_id, $meta_key, $meta_value ) {

	nfwhook_user_meta( $user_id, $meta_key, $meta_value );

}
add_filter('add_user_meta', 'nfwhook_add_user_meta', 1, 3);

/* ================================================================== */

function nfwhook_user_meta( $id, $key, $value ) {

	if (! defined('NF_DISABLED') ) {
		is_nfw_enabled();
	}
	if ( NF_DISABLED || defined('NFW_DISABLE_PRVESC2') ) { return; }

	global $wpdb;

	if ( is_array( $key ) ) {
		$key = serialize( $key );
	}
	if ( strpos( $key, "{$wpdb->base_prefix}capabilities") !== FALSE && ! current_user_can('edit_users') ) {
		if ( is_array( $value ) ) {
			$value = serialize( $value );
		}
		if ( strpos( $value, "administrator") === FALSE ) { return; }
		$subject = __('Blocked privilege escalation attempt', 'nfwplus');

		$user_info = get_userdata( $id );
		if (! empty( $user_info->user_login ) ) {
			nfw_log2( 'WordPress: ' . $subject, "Username: {$user_info->user_login}, ID: $id", 3, 0);
		} else {
			nfw_log2( 'WordPress: ' . $subject, "$key: $value", 3, 0);
		}

		@session_destroy();

		$nfw_options = nfw_get_option( 'nfw_options' );

		// Alert the admin if needed:
		if (! empty( $nfw_options['a_53'] ) ) {

			if ( ( is_multisite() ) && ( $nfw_options['alert_sa_only'] == 2 ) ) {
				$recipient = get_option('admin_email');
			} else {
				$recipient = $nfw_options['alert_email'];
			}
			$subject = '[NinjaFirewall] ' . $subject;
			$message = __('NinjaFirewall has blocked an attempt to gain administrative privileges:', 'nfwplus') . "\n\n";
			if ( is_multisite() ) {
				$message.= __('Blog:', 'nfwplus') .' '. network_home_url('/') . "\n";
			} else {
				$message.= __('Blog:', 'nfwplus') .' '. home_url('/') . "\n";
			}
			$message.= __('Username:', 'nfwplus') .' '. $user_info->user_login . " (ID: $id)\n";
			$message.= __('User IP:', 'nfwplus') .' '. NFW_REMOTE_ADDR . "\n";
			$message.= 'SCRIPT_FILENAME: ' . $_SERVER['SCRIPT_FILENAME'] . "\n";
			$message.= 'REQUEST_URI: ' . $_SERVER['REQUEST_URI'] . "\n";
			$message.= __('Date:', 'nfwplus') .' '. date_i18n('F j, Y @ H:i:s') . ' (UTC '. date('O') . ")\n\n";
			$message.= __('This notification can be turned off from NinjaFirewall "Event Notifications" page.', 'nfwplus') . "\n\n";
			$message.=	'NinjaFirewall (WP+ Edition) - http://ninjafirewall.com/' . "\n" .
							'Help Desk: https://nintechnet.com/helpdesk/' . "\n";
			wp_mail( $recipient, $subject, $message );

		}

		die("<script>if(document.body===null||document.body===undefined){document.write('NinjaFirewall: $subject.');}else{document.body.innerHTML='NinjaFirewall: $subject.';}</script><noscript>NinjaFirewall: $subject.</noscript>");
	}
}
/* ================================================================== */

function nfw_login_form_hook() {

	if (! empty( $_SESSION['nfw_bfd'] ) ) {
		echo '<p class="message">'. __('NinjaFirewall brute-force protection is enabled and you are temporarily whitelisted.', 'nfwplus' ) . '</p><br />';
	}
}
add_filter( 'login_message', 'nfw_login_form_hook');

/* ================================================================== */

function nfw_session_debug() {

	// Make sure NinjaFirewall is running :
	if (! defined('NF_DISABLED') ) {
		is_nfw_enabled();
	}
	if ( NF_DISABLED ) { return; }

	$show_session_icon = 0;
	$current_user = wp_get_current_user();
	// Check users first:
	if ( defined( 'NFW_SESSION_DEBUG_USER' ) ) {
		$users = explode( ',', NFW_SESSION_DEBUG_USER );
		foreach ( $users as $user ) {
			if ( trim( $user ) == $current_user->user_login ) {
				$show_session_icon = 1;
				break;
			}
		}
	// Check capabilities:
	} elseif ( defined( 'NFW_SESSION_DEBUG_CAPS' ) ) {
		$caps = explode( ',', NFW_SESSION_DEBUG_CAPS );
		foreach ( $caps as $cap ) {
			if (! empty( $current_user->caps[ trim( $cap ) ] ) ) {
				$show_session_icon = 1;
				break;
			}
		}
	}

	if ( empty( $show_session_icon ) ) { return; }

	// Check if the user whitelisted?
	if ( empty( $_SESSION['nfw_goodguy'] ) ) {
		// No:
		$font = 'ff0000';
	} else {
		// Yes:
		$font = '00ff00';
	}

	global $wp_admin_bar;
	$wp_admin_bar->add_menu( array(
		'id'    => 'nfw_session_dbg',
		'title' => "<font color='#{$font}'>NF</font>",
	) );

}

// Check if the session debug option is enabled:
if ( defined( 'NFW_SESSION_DEBUG_USER' ) || defined( 'NFW_SESSION_DEBUG_CAPS' ) ) {
	add_action( 'admin_bar_menu', 'nfw_session_debug', 500 );
}

/* ================================================================== */
// EOF
