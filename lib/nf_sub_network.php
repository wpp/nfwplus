<?php
/*
 +=====================================================================+
 | NinjaFirewall (WP+ Edition)                                         |
 |                                                                     |
 | (c) NinTechNet - http://nintechnet.com/                             |
 +=====================================================================+
 | REVISION: 2015-11-21 19:17:34                                       |
 +=====================================================================+ i18n+ / sa
*/

if (! defined( 'NFW_ENGINE_VERSION' ) ) { die( 'Forbidden' ); }

if (! current_user_can( 'manage_network' ) ) {
	die( '<br /><br /><br /><div class="error notice is-dismissible"><p>' .
			sprintf( __('You are not allowed to perform this task (%s).', 'nfwplus'), __LINE__) .
			'</p></div>' );
}

$nfw_options = nfw_get_option( 'nfw_options' );

echo '
<div class="wrap">
	<div style="width:33px;height:33px;background-image:url( ' . plugins_url() . '/nfwplus/images/ninjafirewall_32.png);background-repeat:no-repeat;background-position:0 0;margin:7px 5px 0 0;float:left;"></div>
	<h1>' . __('Network', 'nfwplus') . '</h1>';
if (! is_multisite() ) {
	echo '<div class="updated notice is-dismissible"><p>' . __('You do not have a multisite network.', 'nfwplus') . '</p></div></div>';
	return;
}

// Saved ?
if ( isset( $_POST['nfw_options']) ) {
	if ( empty($_POST['nfwnonce']) || ! wp_verify_nonce($_POST['nfwnonce'], 'network_save') ) {
		wp_nonce_ays('network_save');
	}
	if ( $_POST['nfw_options']['nt_show_status'] == 2 ) {
		$nfw_options['nt_show_status'] = 2;
	} else {
		$nfw_options['nt_show_status'] = 1;
	}
	// Update options :
	nfw_update_option( 'nfw_options', $nfw_options );
	echo '<div class="updated notice is-dismissible"><p>' . __('Your changes have been saved.', 'nfwplus') . '</p></div>';
	$nfw_options = nfw_get_option( 'nfw_options' );
}

if ( empty($nfw_options['nt_show_status']) ) {
	$nfw_options['nt_show_status'] = 1;
}
?><br />
	<form method="post" name="nfwnetwork">
	<?php wp_nonce_field('network_save', 'nfwnonce', 0); ?>
	<h3><?php _e('NinjaFirewall Status', 'nfwplus') ?></h3>
		<table class="form-table">
			<tr>
				<th scope="row"><?php _e('Display NinjaFirewall status icon in the admin bar of all sites in the network', 'nfwplus') ?></th>
				<td align="left" width="200"><label><input type="radio" name="nfw_options[nt_show_status]" value="1"<?php echo $nfw_options['nt_show_status'] != 2 ? ' checked' : '' ?>>&nbsp;<?php _e('Yes (default)', 'nfwplus') ?></label></td>
				<td align="left"><label><input type="radio" name="nfw_options[nt_show_status]" value="2"<?php echo $nfw_options['nt_show_status'] == 2 ? ' checked' : '' ?>>&nbsp;<?php _e('No', 'nfwplus') ?></label></td>
			</tr>
		</table>

		<br />
		<br />
		<input class="button-primary" type="submit" name="Save" value="<?php _e('Save Network options', 'nfwplus') ?>" />
	</form>
</div>
<?php
/* ================================================================== */
// EOF
