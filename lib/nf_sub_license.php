<?php
/*
 +=====================================================================+
 | NinjaFirewall (WP+ Edition)                                         |
 |                                                                     |
 | (c) NinTechNet - http://nintechnet.com/                             |
 +=====================================================================+
 | REVISION: 2016-05-12 20:08:18                                       |
 +=====================================================================+ i18n+ / sa
*/

if (! defined( 'NFW_ENGINE_VERSION' ) ) { die( 'Forbidden' ); }

// Block immediately if user is not allowed :
nf_not_allowed( 'block', __LINE__ );

echo '<script>function chkfld(){if (! document.lic_renew.new_lic.value) {alert("' .
	 esc_js( __('Please enter your license.', 'nfwplus') ) . '");return false;}}</script>
<div class="wrap">
	<div style="width:33px;height:33px;background-image:url( ' . plugins_url() . '/nfwplus/images/ninjafirewall_32.png);background-repeat:no-repeat;background-position:0 0;margin:7px 5px 0 0;float:left;"></div>
	<h1>'. __('License', 'nfwplus') . '</h1>';

$nfw_options = nfw_get_option( 'nfw_options' );

if (! empty($_POST['nfw_what']) ) {
	if ( empty($_POST['nfwnonce']) || ! wp_verify_nonce($_POST['nfwnonce'], 'license_save') ) {
		wp_nonce_ays('license_save');
	}
	if ( is_multisite() ) {
		$nfw_site_url = rtrim( strtolower( network_site_url('','http')), '/' );
	} else {
		$nfw_site_url = rtrim( strtolower(site_url('','http')), '/' );
	}

	global $wp_version;
	$opt_update = 0;
	if ( $_POST['nfw_what'] == 'check' ) {
		if ( empty( $nfw_options['lic'] ) ) {
			echo '<div class="error notice is-dismissible"><p>'. __('Error: you do not have any license.', 'nfwplus') . '</p></div>';
		} else {
			$nfw_res['exp'] = "2020-01-01";
			$nfw_options['lic'] = trim( $nfw_options['lic'] );
			$nfw_options['lic_exp'] = $nfw_res['exp'];
			$opt_update = 1;
			echo '<div class="updated notice is-dismissible"><p>'. __('You have a valid license.', 'nfwplus') . '</p></div>';
		}

	} elseif ( $_POST['nfw_what'] == 'renew' ) {
		// Ensure there is a license:
		if ( empty( $_POST['new_lic'] ) ) {
			echo '<div class="error notice is-dismissible"><p>'. __('Enter a valid license to save!', 'nfwplus') . '</p></div>';
		} else {
			// Use stripslashes() to prevent WordPress from escaping the variable:
			$_POST['new_lic'] = stripslashes( $_POST['new_lic'] );
			if (! empty($nfw_options['lic']) && $nfw_options['lic'] == $_POST['new_lic'] ) {
				echo '<div class="error notice is-dismissible"><p>'. __('This is already your current license!', 'nfwplus') . '</p></div>';
			} else {
				$_POST['new_lic'] = trim( $_POST['new_lic'] );
				$nfw_res['exp'] = "2020-01-01";
				$nfw_options['lic_exp'] = $nfw_res['exp'];
				$nfw_options['lic'] = $_POST['new_lic'];
				$opt_update = 1;
				echo '<div class="updated notice is-dismissible"><p>'. __('Your new license has been accepted and saved.', 'nfwplus') . '</p></div>';
			}
		}
	}
	// Update options if needed :
	if ( $opt_update ) {
		nfw_update_option( 'nfw_options', $nfw_options);
	}
}

if (empty($nfw_options['lic']) ) {
	$lic = '';
} else {
	$lic = $nfw_options['lic'];
}

$lic_exp_warn = $renew = 0;
if (! empty($nfw_options['lic_exp']) && preg_match('/^\d{4}-\d{2}-\d{2}$/', $nfw_options['lic_exp']) ) {
	$lic_exp = $nfw_options['lic_exp'];
	if ( $lic_exp < date('Y-m-d', strtotime("-1 day")) ){
		$lic_exp_warn = -1;
	} elseif ( $lic_exp < date('Y-m-d', strtotime("+30 day")) ){
		$lic_exp_warn = 30;
	}
} else {
	$lic_exp = '';
}

echo '<br />
	<form method="post" name="lic_check">';
	wp_nonce_field('license_save', 'nfwnonce', 0);
	echo '
	<h3>'. __('Current License', 'nfwplus') . '</h3>
	<table class="form-table">
		<tr>
			<th scope="row">'. __('License Number', 'nfwplus') . '</th>
			<td align="left">';
if (! $lic ) {
	$renew = 1;
	echo '<img src="' . plugins_url() . '/nfwplus/images/icon_error_16.png" border="0" height="16" width="16">&nbsp;<font style="color:red">'. __('No license found', 'nfwplus') . '</font></td>';
} else {
	echo '&nbsp;<input type="text" name="lic_check" value="' . $lic . '" class="large-text" readonly />
			<p><input class="button-secondary" type="submit" name="Check" value="'. __('Click to check your license validity', 'nfwplus') . '" /></p>
			</td>';
			}
echo '</tr>';

if ( $lic ) {
	echo '
		<tr>
			<th scope="row">'. __('Expiration date', 'nfwplus') . '</th>
			<td align="left">';
	if (! $lic_exp ) {
		$renew = 1;
		echo '<img src="' . plugins_url() . '/nfwplus/images/icon_error_16.png" border="0" height="16" width="16">';
		$lic_exp = ''. __('Unknown expiration date', 'nfwplus') . '<br /><span class="description">'. __('Use the "Check License Validity" button to attempt to fix this error.', 'nfwplus') . '</span>';
	} elseif ( $lic_exp_warn > 0 ) {
		$renew = 1;
		echo '<img src="' . plugins_url() . '/nfwplus/images/icon_warn_16.png" border="0" height="16" width="16">';
		$lic_exp .= '&nbsp;&nbsp;<span class="description"><font color="red">'. __('Your license will expire soon!', 'nfwplus') . '</font></span>';
	} elseif ( $lic_exp_warn < 0 ) {
		$renew = 1;
		echo '<img src="' . plugins_url() . '/nfwplus/images/icon_error_16.png" border="0" height="16" width="16">';
		$lic_exp = '<span class="description"><font color="red">'. __('Your license has expired.', 'nfwplus') . '</font></span>';
	} else {
		$renew = 0;
		echo '&nbsp;';
	}
	echo '&nbsp;' . $lic_exp . '</td>
		</tr>';
}
echo '
	</table>
	<input type="hidden" name="nfw_what" value="check" />
	</form>';

if ( $renew  ) {
	echo '
	<br />
	<form method="post" name="lic_renew" onSubmit="return chkfld();">';
	wp_nonce_field('license_save', 'nfwnonce', 0);
	echo '
	<h3>'. __('License renewal', 'nfwplus') . '</h3>
	<table class="form-table">
		<tr>
			<th scope="row">&nbsp;</th>
			<td width="20">&nbsp;</td>
			<td align="left"><a href="http://nintechnet.com/ninjafirewall/wp-edition/" target="_blank">'. __('Click here to get a license!', 'nfwplus') . '</a></td>
		</tr>
		<tr>
			<th scope="row">'. __('Enter your new license and click on the save button', 'nfwplus') . '</th>
			<td width="20">&nbsp;</td>
			<td align="left">
				<input type="text" autocomplete="off" value="" maxlength="500" class="large-text" name="new_lic">
			<p><input class="button-secondary" type="submit" name="Save" value="'. __('Save New License', 'nfwplus') . '" /></p>
			</td>
		</tr>
	</table>
	<input type="hidden" name="nfw_what" value="renew" />
	</form>';
}
echo '
</div>';
/* ================================================================== */
// EOF

