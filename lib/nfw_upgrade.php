<?php
/*
 +=====================================================================+
 | NinjaFirewall (WP+ Edition)                                         |
 |                                                                     |
 | (c) NinTechNet - http://nintechnet.com/                             |
 +=====================================================================+ i18n- / sa
*/

if (! defined( 'NFW_ENGINE_VERSION' ) ) { die( 'Forbidden' ); }

// Return immediately if user is not allowed :
if ( nf_not_allowed(0, __LINE__) ) { return; }

// We need to get the options and rules again:
$nfw_options = nfw_get_option( 'nfw_options' );
$nfw_rules = nfw_get_option( 'nfw_rules' );

if (! empty($nfw_options['engine_version']) && version_compare($nfw_options['engine_version'], NFW_ENGINE_VERSION, '<') ) {
	// v1.0.2 update -------------------------------------------------
	// File Guard options :
	if ( empty( $nfw_options['fg_mtime']) ) {
		$nfw_options['fg_enable'] = 0;
		$nfw_options['fg_mtime'] = 10;
	}

	// v1.0.4 update -------------------------------------------------
	// Base64 encode the blocked message :
	if ( version_compare( $nfw_options['engine_version'], '1.0.4', '<' ) ) {
		$nfw_options['blocked_msg'] = base64_encode($nfw_options['blocked_msg']);
	}
	// v1.0.5 update -------------------------------------------------
	// Error from v1.0.4 to delete :
	if ( isset($nfw_options['$auth_msg']) ) {
		unset($nfw_options['$auth_msg']);
	}
	// v1.0.7 update -------------------------------------------------
	if ( version_compare( $nfw_options['engine_version'], '1.0.7', '<' ) ) {
		// Create 'wp-content/nfwlog/' directories and files :
		if ( is_writable(NFW_LOG_DIR) ) {
			if (! file_exists(NFW_LOG_DIR . '/nfwlog') ) {
				mkdir( NFW_LOG_DIR . '/nfwlog', 0755);
			}
			if (! file_exists(NFW_LOG_DIR . '/nfwlog/cache') ) {
				mkdir( NFW_LOG_DIR . '/nfwlog/cache', 0755);
			}
			touch( NFW_LOG_DIR . '/nfwlog/index.html' );
			touch( NFW_LOG_DIR . '/nfwlog/cache/index.html' );
			@file_put_contents(NFW_LOG_DIR . '/nfwlog/.htaccess', "Order Deny,Allow\nDeny from all", LOCK_EX);
			@file_put_contents(NFW_LOG_DIR . '/nfwlog/cache/.htaccess', "Order Deny,Allow\nDeny from all", LOCK_EX);

			// Restore brute-force protection configuration from the DB:
			$nfwbfd_log = NFW_LOG_DIR . '/nfwlog/cache/bf_conf.php';
			if ((! empty($nfw_options['bf_request'])) && (! empty($nfw_options['bf_bantime'])) &&
				(! empty($nfw_options['bf_attempt'])) && (! empty($nfw_options['bf_maxtime'])) &&
				(! empty($nfw_options['auth_name'])) && (! empty($nfw_options['auth_pass'])) &&
				(! empty($nfw_options['bf_rand'])) ) {
				if ( empty($nfw_options['bf_enable'])) {
					$nfw_options['bf_enable'] = 1;
				}
				if ( empty($nfw_options['auth_msg']) ) {
					$nfw_options['auth_msg'] = 'Access restricted';
				}
				// xmlrpc option (added to v1.0.5) :
				if (! isset($nfw_options['bf_xmlrpc']) ) {
					$nfw_options['bf_xmlrpc'] = 0;
				}
				// AUTH log (added to v1.0.6) :
				if (! isset($nfw_options['bf_authlog']) ) {
					$nfw_options['bf_authlog'] = 0;
				}
				$data = '<?php $bf_enable=' . $nfw_options['bf_enable'] .
					';$bf_request=\'' . $nfw_options['bf_request'] . '\'' .
					';$bf_bantime=' . $nfw_options['bf_bantime'] .
					';$bf_attempt=' . $nfw_options['bf_attempt'] .
					';$bf_maxtime=' . $nfw_options['bf_maxtime'] .
					';$bf_xmlrpc=' . $nfw_options['bf_xmlrpc'] .
					';$auth_name=\'' . $nfw_options['auth_name'] . '\'' .
					';$auth_pass=\'' . $nfw_options['auth_pass'] . '\';' .
					'$auth_msg=\'' . $nfw_options['auth_msg'] . '\'' .
					';$bf_rand=\'' . $nfw_options['bf_rand'] . '\';'.
					'$bf_authlog='. $nfw_options['bf_authlog'] . '; ?>';
				$fh = fopen( $nfwbfd_log, 'w' );
				fwrite( $fh, $data );
				fclose( $fh );
			}
		}
		// We don't need to backup the brute-force protection data to the DB anymore
		// because we're now using the log/cache dir inside the wp-content directory :
		unset($nfw_options['bf_enable']);
		unset($nfw_options['bf_request']);
		unset($nfw_options['bf_bantime']);
		unset($nfw_options['bf_attempt']);
		unset($nfw_options['bf_maxtime']);
		unset($nfw_options['bf_xmlrpc']);
		unset($nfw_options['auth_name']);
		unset($nfw_options['auth_pass']);
		unset($nfw_options['auth_msg']);
		unset($nfw_options['bf_rand']);
		unset($nfw_options['bf_authlog']);
		// Web filter :
		if ( defined('NFW_LOG_DIR') ) {
			$nfw_options['wp_content'] = NFW_LOG_DIR;
		} else {
			$nfw_options['wp_content'] = WP_CONTENT_DIR;
		}
	}
	// v1.0.8 update -------------------------------------------------
	// Allowed URL new Access Control:
	if ( version_compare( $nfw_options['engine_version'], '1.0.8', '<' ) ) {
		if (! isset($nfw_options['ac_wl_url']) ) {
			$nfw_options['ac_wl_url'] = 0;
		}
		if (! isset($nfw_options['ac_wl_url_log']) ) {
			$nfw_options['ac_wl_url_log'] = 0;
		}
	}
	// v1.1.1 update -------------------------------------------------
	if ( version_compare( $nfw_options['engine_version'], '1.1.1', '<' ) ) {
		if ( function_exists('header_register_callback') && function_exists('headers_list') && function_exists('header_remove') ) {
			// We enable X-XSS-Protection:
			$nfw_options['response_headers'] = '00010000';
		}
	}
	// v1.1.3 update -------------------------------------------------
	if ( version_compare( $nfw_options['engine_version'], '1.1.3', '<' ) ) {
		$nfw_options['a_41'] = 1;
		$nfw_options['sched_scan'] = 0;
		$nfw_options['report_scan'] = 0;
	}
	// v1.1.4 update -------------------------------------------------
	if ( version_compare( $nfw_options['engine_version'], '1.1.4', '<' ) ) {
		$nfw_options['a_51'] = 1;
	}
	// v1.1.5 update -------------------------------------------------
	if ( version_compare( $nfw_options['engine_version'], '1.1.5', '<' ) ) {
		$nfw_options['fg_exclude'] = '';
	}
	// v1.1.6 update -------------------------------------------------
	if ( version_compare( $nfw_options['engine_version'], '1.1.6', '<' ) ) {
		// Remove all old nfdbhash* files :
		$path = NFW_LOG_DIR . '/nfwlog/cache/';
		$glob = glob($path . "nfdbhash*php");
		if ( is_array($glob)) {
			foreach($glob as $file) {
				unlink($file);
			}
		}
	}
	// v1.7 update ---------------------------------------------------
	// Create daily report cronjob :
	if ( version_compare( $nfw_options['engine_version'], '1.7', '<' ) ) {
		$nfw_options['a_52'] = 1;
		if ( ! wp_next_scheduled('nfdailyreport') ) {
			nfw_get_blogtimezone();
			wp_schedule_event( strtotime( date('Y-m-d 00:00:05', strtotime("+1 day")) ), 'daily', 'nfdailyreport');
		}
		$nfw_options['no_xmlrpc_multi'] = 0;
	}
	// v3.1.2 update (file guard) ------------------------------------
	if ( version_compare( $nfw_options['engine_version'], '3.1.2', '<' ) ) {
		// Convert the current value for regex use:
		if (! empty( $nfw_options['fg_exclude'] ) ) {
			$nfw_options['fg_exclude'] = preg_quote( $nfw_options['fg_exclude'], '`');
		}
	}
	// v3.2 update (anti-malware) ------------------------------------
	if ( version_compare( $nfw_options['engine_version'], '3.2', '<' ) ) {
		$nfw_options['malware_dir'] = ABSPATH;
		$nfw_options['malware_symlink'] = 1;
		$nfw_options['malware_timestamp'] = 7;
		$nfw_options['malware_size'] = 2048;
	}
	// v3.3 update ---------------------------------------------------
	if ( version_compare( $nfw_options['engine_version'], '3.3', '<' ) ) {
		if ( function_exists('header_register_callback') && function_exists('headers_list') && function_exists('header_remove') ) {
			if (! empty( $nfw_options['response_headers'] ) && strlen( $nfw_options['response_headers'] ) == 6 ) {
				$nfw_options['response_headers'] .= '00';
			}
		}
	}
	// v3.4 update ---------------------------------------------------
	if ( version_compare( $nfw_options['engine_version'], '3.4', '<' ) ) {
		$nfw_options['a_53'] = 1;
	}
	// v3.5.1 update -------------------------------------------------
	if ( version_compare( $nfw_options['engine_version'], '3.5.1', '<' ) ) {
		// Create garbage collector's cron job:
		if ( wp_next_scheduled( 'nfwgccron' ) ) {
			wp_clear_scheduled_hook( 'nfwgccron' );
		}
		wp_schedule_event( time() + 60, 'hourly', 'nfwgccron' );
	}
	// ---------------------------------------------------------------

	// Adjust current version :
	$nfw_options['engine_version'] = NFW_ENGINE_VERSION;
	$is_update = 1;

	// Fetch the latest set of rules from wordpress.org:
	define('NFUPDATESDO', 2);
	@nf_sub_updates();

	if (! defined('NFW_NEWRULES_VERSION') ) {
		// If we weren't able to download the rules, set it to an older value:
		define('NFW_NEWRULES_VERSION', '20160101.1');
	}

	// Always update rules:
	if ( $nfw_rules_new = @unserialize(NFW_RULES) ) {
		// Rules successfully downloaded :
		foreach ( $nfw_rules_new as $new_key => $new_value ) {
			foreach ( $new_value as $key => $value ) {
				// if that rule exists already, we keep its 'ena' flag value
				// as it may have been changed by the user with the rules editor :
				// v3.x:
				if ( ( isset( $nfw_rules[$new_key]['ena'] ) ) && ( $key == 'ena' ) ) {
					$nfw_rules_new[$new_key]['ena'] = $nfw_rules[$new_key]['ena'];
				}
				// v1.x:
				if ( ( isset( $nfw_rules[$new_key]['on'] ) ) && ( $key == 'ena' ) ) {
					$nfw_rules_new[$new_key]['ena'] = $nfw_rules[$new_key]['on'];
				}
			}
		}
		// v1.x:
		if ( isset( $nfw_rules[NFW_DOC_ROOT]['what'] ) ) {
			$nfw_rules_new[NFW_DOC_ROOT]['cha'][1]['wha']= str_replace( '/', '/[./]*', $nfw_rules[NFW_DOC_ROOT]['what'] );
			$nfw_rules_new[NFW_DOC_ROOT]['ena']	= $nfw_rules[NFW_DOC_ROOT]['on'];
		// v3.x:
		} else {
			$nfw_rules_new[NFW_DOC_ROOT]['cha'][1]['wha']= $nfw_rules[NFW_DOC_ROOT]['cha'][1]['wha'];
			$nfw_rules_new[NFW_DOC_ROOT]['ena']	= $nfw_rules[NFW_DOC_ROOT]['ena'];
		}

		// The WP+ Edition does not need rule #531 :
		if ( isset($nfw_rules_new[531])) {
			unset($nfw_rules_new[531]);
		}

		// v1.0.7:20140925 update ----------------------------------------
		// We delete rules #151 and #152
		if ( version_compare( $nfw_options['rules_version'], '20140925', '<' ) ) {
			if ( isset($nfw_rules_new[151]) ) {
				unset($nfw_rules_new[151]);
			}
			if ( isset($nfw_rules_new[152]) ) {
				unset($nfw_rules_new[152]);
			}
		}
		// ---------------------------------------------------------------

		// update rules... :
		nfw_update_option( 'nfw_rules', $nfw_rules_new);
		// ...and the latest rules version number:
		$nfw_options['rules_version'] = NFW_NEWRULES_VERSION;

	} else {

		// We failed to download the rules, return the hardcoded version:
		if ( ( is_multisite() ) && ( $nfw_options['alert_sa_only'] == 2 ) ) {
			$recipient = get_option('admin_email');
		} else {
			$recipient = $nfw_options['alert_email'];
		}

		$subject = '[NinjaFirewall] ' . __('ERROR: Failed to update rules', 'nfwplus');
		if ( is_multisite() ) {
			$url = __('-Blog :', 'nfwplus') .' '. network_home_url('/') . "\n\n";
		} else {
			$url = __('-Blog :', 'nfwplus') .' '. home_url('/') . "\n\n";
		}
		$message = __('NinjaFirewall failed to update its rules. This is a critical error, your current rules may be corrupted or disabled. In order to solve the problem, please follow these instructions:', 'nfwplus') . "\n\n";
		$message.= __('1. Log in to your WordPress admin dashboard.', 'nfwplus') . "\n";
		$message.= __('2. Go to "NinjaFirewall > Updates".', 'nfwplus') . "\n";
		$message.= __('3. Click on "Check for updates now!".', 'nfwplus') .
						"\n\n".
						__('-Date :', 'nfwplus') .' '. ucfirst(date_i18n('F j, Y @ H:i:s')) . ' (UTC '. date('O') . ")\n" .
						$url .
					'NinjaFirewall (WP+ Edition) - http://ninjafirewall.com/' . "\n" .
					__('Help Desk', 'nfwplus') . ': https://nintechnet.com/helpdesk/' . "\n";
		wp_mail( $recipient, $subject, $message );
	}
}
// EOF
