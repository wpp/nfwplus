<?php
/*
 +=====================================================================+
 | NinjaFirewall (WP+ Edition)                                         |
 |                                                                     |
 | (c) NinTechNet - http://nintechnet.com/                             |
 +=====================================================================+ i18n+ / sa
*/

if (! defined( 'NFW_ENGINE_VERSION' ) ) { die( 'Forbidden' ); }

// Block immediately if user is not allowed :
nf_not_allowed( 'block', __LINE__ );

$yes = __('Yes', 'nfwplus');
$no =  __('No', 'nfwplus');
$default =  ' '. __('(default)', 'nfwplus');
$full_waf_msg = '<br /><img src="' . plugins_url() . '/nfwplus/images/icon_warn_16.png" border="0" height="16" width="16">&nbsp;<span class="description">' . sprintf( __('This feature is only available when NinjaFirewall is running in %s mode.', 'nfwplus'), '<a href="https://blog.nintechnet.com/full_waf-vs-wordpress_waf/">Full WAF</a>') . '</span>';
if ( defined('NFW_WPWAF') ) {
	$option_disabled = 1;
} else {
	$option_disabled = 0;
}

$nfw_options = nfw_get_option( 'nfw_options' );
$nfw_rules = nfw_get_option( 'nfw_rules' );

echo '
<script>
function restore() {
   if (confirm("' . esc_js( __('All fields will be restored to their default values. Go ahead?', 'nfwplus') ) . '")){
      return true;
   }else{
		return false;
   }
}

function san_onoff(what) {
	if (what == 0) {
		document.fwrules.sanid.disabled = true;
		document.fwrules.subs.disabled = true;
		document.fwrules.sizeid.disabled = true;
	} else {
		document.fwrules.sanid.disabled = false;
		document.fwrules.subs.disabled = false;
		document.fwrules.sizeid.disabled = false;
	}
}
function csp_onoff(what, csp) {
	if (what == 0) {
		document.getElementById(csp).readOnly = true;
	} else {
		document.getElementById(csp).readOnly = false;
		document.getElementById(csp).focus();
	}
}
function ssl_warn() {';
	// Obviously, if we are already in HTTPS mode, we don't send any warning:
	if ($_SERVER['SERVER_PORT'] == 443 ) {
		echo 'return true;';
	} else {
		echo '
		if (confirm("' . esc_js( __('WARNING: ensure that you can access your admin console over HTTPS before enabling this option, otherwise you will lock yourself out of your site. Go ahead?', 'nfwplus') ) . '")){
			return true;
		}
		return false;';
	}
echo '
}
function sanitise_fn(cbox) {
	if(cbox.checked) {
		if (confirm("' . esc_js( __('Any character that is not a letter [a-zA-Z], a digit [0-9], a dot [.], a hyphen [-] or an underscore [_] will be removed from the filename and replaced with the substitution character. Continue?', 'nfwplus') ) . '")){
			return true;
		}
		return false;
	}
}
function is_number(id) {
	var e = document.getElementById(id);
	if (! e.value ) { return }
	if (! /^[0-9]+$/.test(e.value) ) {
		alert("' . esc_js( __('Please enter numbers only.', 'nfwplus') ) . '");
		e.value = e.value.substring(0, e.value.length-1);
	}
}

</script>

<div class="wrap">
	<div style="width:33px;height:33px;background-image:url( ' . plugins_url() . '/nfwplus/images/ninjafirewall_32.png);background-repeat:no-repeat;background-position:0 0;margin:7px 5px 0 0;float:left;"></div>
	<h1>' . __('Firewall Policies', 'nfwplus') . '</h1>';

// Saved options ?
if ( isset( $_POST['nfw_options']) ) {
	if ( empty($_POST['nfwnonce']) || ! wp_verify_nonce($_POST['nfwnonce'], 'policies_save') ) {
		wp_nonce_ays('policies_save');
	}
	if (! empty($_POST['Save']) ) {
		nf_sub_policies_save();
		echo '<div class="updated notice is-dismissible"><p>' . __('Your changes have been saved.', 'nfwplus') . '</p></div>';
	} elseif (! empty($_POST['Default']) ) {
		nf_sub_policies_default();
		echo '<div class="updated notice is-dismissible"><p>' . __('Default values were restored.', 'nfwplus') . '</p></div>';
	} else {
		echo '<div class="error notice is-dismissible"><p>' . __('No action taken.', 'nfwplus') . '</p></div>';
	}
	$nfw_options = nfw_get_option( 'nfw_options' );
	$nfw_rules = nfw_get_option( 'nfw_rules' );
}

echo '<form method="post" name="fwrules">';
wp_nonce_field('policies_save', 'nfwnonce', 0);

if ( ( isset( $nfw_options['scan_protocol']) ) &&
	( preg_match( '/^[123]$/', $nfw_options['scan_protocol']) ) ) {
	$scan_protocol = $nfw_options['scan_protocol'];
} else {
	$scan_protocol = 3;
}

?>
	<h3>HTTP / HTTPS</h3>
	<table class="form-table">
		<tr>
			<th scope="row"><?php _e('Enable NinjaFirewall for', 'nfwplus') ?></th>
			<td width="20">&nbsp;</td>
			<td align="left">
			<p><label><input type="radio" name="nfw_options[scan_protocol]" value="3"<?php checked($scan_protocol, 3 ) ?>>&nbsp;<?php _e('HTTP and HTTPS traffic (default)', 'nfwplus') ?></label></p>
			<p><label><input type="radio" name="nfw_options[scan_protocol]" value="1"<?php checked($scan_protocol, 1 ) ?>>&nbsp;<?php _e('HTTP traffic only', 'nfwplus') ?></label></p>
			<p><label><input type="radio" name="nfw_options[scan_protocol]" value="2"<?php checked($scan_protocol, 2 ) ?>>&nbsp;<?php _e('HTTPS traffic only', 'nfwplus') ?></label></p>
			</td>
		</tr>
	</table>

<?php
if ( empty( $nfw_options['sanitise_fn']) ) {
	$sanitise_fn = 0;
} else {
	$sanitise_fn = 1;
}
if ( isset($nfw_options['uploads']) && preg_match( '/^[12]$/', $nfw_options['uploads']) ) {
	$uploads = $nfw_options['uploads'];
} else {
	$uploads = 0;
}
if ( empty( $nfw_options['upload_maxsize']) ) {
	$upload_maxsize = 1024;
} else {
	$upload_maxsize = $nfw_options['upload_maxsize'] / 1024;
}
if ( empty( $nfw_options['substitute'] ) || strlen( $nfw_options['substitute'] ) > 1 ) {
	$substitute = 'X';
} else {
	$substitute = htmlspecialchars( $nfw_options['substitute'] );
}
?>
	<br />
	<br />

	<h3><?php _e('Uploads', 'nfwplus') ?></h3>
	<table class="form-table">
		<tr>
			<th scope="row"><?php _e('File Uploads', 'nfwplus') ?></th>
			<td width="20">&nbsp;</td>
			<td align="left">
				<p><label><input type="radio" name="nfw_options[uploads]"<?php checked( $uploads, 0 ) ?> value="0" id="uf1" onClick="san_onoff(0);">&nbsp;<?php _e('Disallow uploads (default)', 'nfwplus') ?></label></p>
				<p><label><input type="radio" name="nfw_options[uploads]"<?php checked( $uploads, 1 ) ?> value="1" id="uf1" onClick="san_onoff(1);">&nbsp;<?php _e('Allow uploads', 'nfwplus') ?></label></p>
				<p><label><input type="radio" name="nfw_options[uploads]"<?php checked( $uploads, 2 ) ?> value="2" id="uf2" onClick="san_onoff(1);">&nbsp;<?php _e('Allow, but block scripts, ELF and system files', 'nfwplus') ?></label></p>
				<br />
				<p><label><input id="sanid" type="checkbox" onclick='return sanitise_fn(this);' name="nfw_options[sanitise_fn]"<?php checked( $sanitise_fn, 1 ); disabled( $uploads, 0 ) ?>>&nbsp;<?php _e('Sanitise filenames', 'nfwplus') ?>
				(<?php _e('substitution character:', 'nfwplus' ) ?></label> <input id="subs" maxlength="1" size="1" value="<?php echo $substitute ?>" name="nfw_options[substitute]" type="text" <?php disabled( $uploads, 0 ) ?>/> )
				</p>
				<p><?php _e('Maximum allowed file size', 'nfwplus' ) ?>&nbsp;<input id="sizeid" type="text" name="nfw_options[upload_maxsize]"<?php disabled( $uploads, 0 ) ?> size="5" value="<?php echo $upload_maxsize ?>" onkeyup="is_number('sizeid')">&nbsp;KB</p>

			</td>
		</tr>
	</table>

<?php
if ( empty( $nfw_options['get_scan']) ) {
	$get_scan = 0;
} else {
	$get_scan = 1;
}
if ( empty( $nfw_options['get_sanitise']) ) {
	$get_sanitise = 0;
} else {
	$get_sanitise = 1;
}
?>
	<br />
	<br />
	<h3><?php _e('HTTP GET variable', 'nfwplus') ?></h3>
	<table class="form-table">
		<tr>
			<th scope="row"><?php _e('Scan <code>GET</code> variable', 'nfwplus') ?></th>
			<td width="20">&nbsp;</td>
			<td align="left" width="120">
				<label><input type="radio" name="nfw_options[get_scan]" value="1"<?php checked( $get_scan, 1 ) ?>>&nbsp;<?php _e('Yes (default)', 'nfwplus') ?></label>
			</td>
			<td align="left">
				<label><input type="radio" name="nfw_options[get_scan]" value="0"<?php checked( $get_scan, 0 ) ?>>&nbsp;<?php _e('No', 'nfwplus') ?></label>
			</td>
		</tr>
		<tr>
			<th scope="row"><?php _e('Sanitise <code>GET</code> variable', 'nfwplus') ?></th>
			<td width="20">&nbsp;</td>
			<td align="left" width="120">
				<label><input type="radio" name="nfw_options[get_sanitise]" value="1"<?php checked( $get_sanitise, 1 ) ?>>&nbsp;<?php _e('Yes', 'nfwplus') ?></label>
			</td>
			<td align="left">
				<label><input type="radio" name="nfw_options[get_sanitise]" value="0"<?php checked( $get_sanitise, 0 ) ?>>&nbsp;<?php _e('No (default)', 'nfwplus') ?></label>
			</td>
		</tr>
	</table>

	<br />
	<br />

<?php
if ( empty( $nfw_options['post_scan']) ) {
	$post_scan = 0;
} else {
	$post_scan = 1;
}
if ( empty( $nfw_options['post_sanitise']) ) {
	$post_sanitise = 0;
} else {
	$post_sanitise = 1;
}
if ( empty( $nfw_options['post_b64']) ) {
	$post_b64 = 0;
} else {
	$post_b64 = 1;
}
?>
	<h3><?php _e('HTTP POST variable', 'nfwplus') ?></h3>
	<table class="form-table">
		<tr valign="top">
			<th scope="row"><?php _e('Scan <code>POST</code> variable', 'nfwplus') ?></th>
			<td width="20">&nbsp;</td>
			<td align="left" width="120">
				<label><input type="radio" name="nfw_options[post_scan]" value="1"<?php checked( $post_scan, 1 ) ?>>&nbsp;<?php _e('Yes (default)', 'nfwplus') ?></label>
			</td>
			<td align="left">
				<label><input type="radio" name="nfw_options[post_scan]" value="0"<?php checked( $post_scan, 0 ) ?>>&nbsp;<?php _e('No', 'nfwplus') ?></label>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row"><?php _e('Sanitise <code>POST</code> variable', 'nfwplus') ?></th>
			<td width="20">&nbsp;</td>
			<td align="left" width="120" style="vertical-align:top;">
				<label><input type="radio" name="nfw_options[post_sanitise]" value="1"<?php checked( $post_sanitise, 1 ) ?>>&nbsp;<?php _e('Yes', 'nfwplus') ?></label>
			</td>
			<td align="left">
				<label><input type="radio" name="nfw_options[post_sanitise]" value="0"<?php checked( $post_sanitise, 0 ) ?>>&nbsp;<?php _e('No (default)', 'nfwplus') ?></label><br /><span class="description">&nbsp;<?php _e('Do not enable this option unless you know what you are doing!', 'nfwplus') ?></span>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row"><?php _e('Decode Base64-encoded <code>POST</code> variable', 'nfwplus') ?></th>
			<td width="20">&nbsp;</td>
			<td align="left" width="120">
				<label><input type="radio" name="nfw_options[post_b64]" value="1"<?php checked( $post_b64, 1 ) ?>>&nbsp;<?php _e('Yes (default)', 'nfwplus') ?></label>
			</td>
			<td align="left">
				<label><input type="radio" name="nfw_options[post_b64]" value="0"<?php checked( $post_b64, 0 ) ?>>&nbsp;<?php _e('No', 'nfwplus') ?></label>
			</td>
		</tr>
	</table>
	<br />
	<br />
<?php
if ( empty( $nfw_options['request_sanitise']) ) {
	$request_sanitise = 0;
} else {
	$request_sanitise = 1;
}
?>
	<h3><?php _e('HTTP REQUEST variable', 'nfwplus') ?></h3>
	<table class="form-table">
		<tr>
			<th scope="row"><?php _e('Sanitise <code>REQUEST</code> variable', 'nfwplus') ?></th>
			<td width="20">&nbsp;</td>
			<td align="left" width="120" style="vertical-align:top;">
				<label><input type="radio" name="nfw_options[request_sanitise]" value="1"<?php checked( $request_sanitise, 1 ) ?>>&nbsp;<?php _e('Yes', 'nfwplus') ?></label>
			</td>
			<td align="left">
				<label><input type="radio" name="nfw_options[request_sanitise]" value="0"<?php checked( $request_sanitise, 0 ) ?>>&nbsp;<?php _e('No (default)', 'nfwplus') ?></label><br /><span class="description">&nbsp;<?php _e('Do not enable this option unless you know what you are doing!', 'nfwplus') ?></span>
			</td>
		</tr>
	</table>
	<br />
	<br />
<?php
if ( empty( $nfw_options['cookies_scan']) ) {
	$cookies_scan = 0;
} else {
	$cookies_scan = 1;
}
if ( empty( $nfw_options['cookies_sanitise']) ) {
	$cookies_sanitise = 0;
} else {
	$cookies_sanitise = 1;
}
?>
	<h3><?php _e('Cookies', 'nfwplus') ?></h3>
	<table class="form-table">
		<tr>
			<th scope="row"><?php _e('Scan cookies', 'nfwplus') ?></th>
			<td width="20">&nbsp;</td>
			<td align="left" width="120">
				<label><input type="radio" name="nfw_options[cookies_scan]" value="1"<?php checked( $cookies_scan, 1 ) ?>>&nbsp;<?php _e('Yes (default)', 'nfwplus') ?></label>
			</td>
			<td align="left">
				<label><input type="radio" name="nfw_options[cookies_scan]" value="0"<?php checked( $cookies_scan, 0 ) ?>>&nbsp;<?php _e('No', 'nfwplus') ?></label>
			</td>
		</tr>
		<tr>
			<th scope="row"><?php _e('Sanitise cookies', 'nfwplus') ?></th>
			<td width="20">&nbsp;</td>
			<td align="left" width="120">
				<label><input type="radio" name="nfw_options[cookies_sanitise]" value="1"<?php checked( $cookies_sanitise, 1 ) ?>>&nbsp;<?php _e('Yes', 'nfwplus') ?></label>
			</td>
			<td align="left">
				<label><input type="radio" name="nfw_options[cookies_sanitise]" value="0"<?php checked( $cookies_sanitise, 0 ) ?>>&nbsp;<?php _e('No (default)', 'nfwplus') ?></label>
			</td>
		</tr>
	</table>
	<br />
	<br />
<?php
if ( empty( $nfw_options['ua_scan']) ) {
	$ua_scan = 0;
} else {
	$ua_scan = 1;
}
if ( empty( $nfw_options['ua_sanitise']) ) {
	$ua_sanitise = 0;
} else {
	$ua_sanitise = 1;
}
?>
	<h3><?php _e('HTTP_USER_AGENT server variable', 'nfwplus') ?></h3>
	<table class="form-table">
		<tr>
			<th scope="row"><?php _e('Scan <code>HTTP_USER_AGENT</code>', 'nfwplus') ?></th>
			<td width="20">&nbsp;</td>
			<td align="left" width="120">
				<label><input type="radio" name="nfw_options[ua_scan]" value="1"<?php checked( $ua_scan, 1 ) ?>>&nbsp;<?php _e('Yes (default)', 'nfwplus') ?></label>
			</td>
			<td align="left">
				<label><input type="radio" name="nfw_options[ua_scan]" value="0"<?php checked( $ua_scan, 0 ) ?>>&nbsp;<?php _e('No', 'nfwplus') ?></label>
			</td>
		</tr>
		<tr>
			<th scope="row"><?php _e('Sanitise <code>HTTP_USER_AGENT</code>', 'nfwplus') ?></th>
			<td width="20">&nbsp;</td>
			<td align="left" width="120">
				<label><input type="radio" name="nfw_options[ua_sanitise]" value="1"<?php checked( $ua_sanitise, 1 ) ?>>&nbsp;<?php _e('Yes (default)', 'nfwplus') ?></label>
			</td>
			<td align="left">
				<label><input type="radio" name="nfw_options[ua_sanitise]" value="0"<?php checked( $ua_sanitise, 0 ) ?>>&nbsp;<?php _e('No', 'nfwplus') ?></label>
			</td>
		</tr>
	</table>
	<br />
	<br />
<?php
if ( empty( $nfw_options['referer_scan']) ) {
	$referer_scan = 0;
} else {
	$referer_scan = 1;
}
if ( empty( $nfw_options['referer_sanitise']) ) {
	$referer_sanitise = 0;
} else {
	$referer_sanitise = 1;
}
if ( empty( $nfw_options['referer_post']) ) {
	$referer_post = 0;
} else {
	$referer_post = 1;
}
?>
	<h3><?php _e('HTTP_REFERER server variable', 'nfwplus') ?></h3>
	<table class="form-table">
		<tr>
			<th scope="row"><?php _e('Scan <code>HTTP_REFERER</code>', 'nfwplus') ?></th>
			<td width="20">&nbsp;</td>
			<td align="left" width="120">
				<label><input type="radio" name="nfw_options[referer_scan]" value="1"<?php checked( $referer_scan, 1 ) ?>>&nbsp;<?php _e('Yes', 'nfwplus') ?></label>
			</td>
			<td align="left">
				<label><input type="radio" name="nfw_options[referer_scan]" value="0"<?php checked( $referer_scan, 0 ) ?>>&nbsp;<?php _e('No (default)', 'nfwplus') ?></label>
			</td>
		</tr>
		<tr>
			<th scope="row"><?php _e('Sanitise <code>HTTP_REFERER</code>', 'nfwplus') ?></th>
			<td width="20">&nbsp;</td>
			<td align="left" width="120">
				<label><input type="radio" name="nfw_options[referer_sanitise]" value="1"<?php checked( $referer_sanitise, 1 ) ?>>&nbsp;<?php _e('Yes (default)', 'nfwplus') ?></label>
			</td>
			<td align="left">
				<label><input type="radio" name="nfw_options[referer_sanitise]" value="0"<?php checked( $referer_sanitise, 0 ) ?>>&nbsp;<?php _e('No', 'nfwplus') ?></label>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row"><?php _e('Block <code>POST</code> requests that do not have an <code>HTTP_REFERER</code> header', 'nfwplus') ?></th>
			<td width="20">&nbsp;</td>
			<td align="left" width="120" style="vertical-align:top;">
				<label><input type="radio" name="nfw_options[referer_post]" value="1"<?php checked( $referer_post, 1 ) ?>>&nbsp;<?php _e('Yes', 'nfwplus') ?></label>
			</td>
			<td align="left" style="vertical-align:top;">
				<label><input type="radio" name="nfw_options[referer_post]" value="0"<?php checked( $referer_post, 0 ) ?>>&nbsp;<?php _e('No (default)', 'nfwplus') ?></label><br /><span class="description">&nbsp;Keep this option disabled if you are using scripts like Paypal IPN, WordPress WP-Cron (unless you added their IPs to your <a href="?page=nfsubaccess#ipaccess">IP</a> or <a href="?page=nfsubaccess#urlaccess">URL</a> Access Control whitelist).</span>
			</td>
		</tr>
	</table>

	<br /><br />
	<?php

	// Some compatibility checks:
	// 1. header_register_callback(): requires PHP >=5.4
	// 2. headers_list() and header_remove(): some hosts may disable them.
	$err_msg = $err = '';
	$err_img = '<p><span class="description"><img src="' . plugins_url() . '/nfwplus/images/icon_warn_16.png" border="0" height="16" width="16">&nbsp;';
	$msg = __('This option is disabled because the %s PHP function is not available on your server.', 'nfwplus');
	if (! function_exists('header_register_callback') ) {
		$err_msg = $err_img . sprintf($msg, '<code>header_register_callback()</code>') . '</span></p>';
		$err = 1;
	} elseif (! function_exists('headers_list') ) {
		$err_msg = $err_img . sprintf($msg, '<code>headers_list()</code>') . '</span></p>';
		$err = 1;
	} elseif (! function_exists('header_remove') ) {
		$err_msg = $err_img . sprintf($msg, '<code>header_remove()</code>') . '</span></p>';
		$err = 1;
	}
	if ( empty($nfw_options['response_headers']) || strlen($nfw_options['response_headers']) != 8 || $err_msg ) {
		$nfw_options['response_headers'] = '00000000';
	}
	?>
	<h3><?php _e('HTTP response headers', 'nfwplus') ?></h3>
	<table class="form-table">
		<tr>
			<th scope="row"><?php printf( __('Set %s to protect against MIME type confusion attacks', 'nfwplus'), '<code><a href="http://nintechnet.com/ninjafirewall/wp-edition/doc/#responseheaders" target="_blank">X-Content-Type-Options</a></code>') ?></th>
			<td width="20">&nbsp;</td>
			<td align="left" width="120">
				<label><input type="radio" name="nfw_options[x_content_type_options]" value="1"<?php checked( $nfw_options['response_headers'][1], 1 ); disabled($err, 1); ?>><?php echo $yes; ?></label>
			</td>
			<td align="left">
				<label><input type="radio" name="nfw_options[x_content_type_options]" value="0"<?php checked( $nfw_options['response_headers'][1], 0 ); disabled($err, 1); ?>><?php echo $no . $default; ?></label><?php echo $err_msg ?>
			</td>
		</tr>
		<tr>
			<th scope="row"><?php printf( __('Set %s to protect against clickjacking attempts', 'nfwplus'), '<code><a href="http://nintechnet.com/ninjafirewall/wp-edition/doc/#responseheaders" target="_blank">X-Frame-Options</a></code>') ?></th>
			<td width="20">&nbsp;</td>
			<td align="left" width="120" style="vertical-align:top;">
				<p><label><input type="radio" name="nfw_options[x_frame_options]" value="1"<?php checked( $nfw_options['response_headers'][2], 1 ); disabled($err, 1); ?>><code>SAMEORIGIN</code></label></p>
				<p><label><input type="radio" name="nfw_options[x_frame_options]" value="2"<?php checked( $nfw_options['response_headers'][2], 2 ); disabled($err, 1); ?>><code>DENY</code></label></p>
			</td>
			<td align="left" style="vertical-align:top;"><p><label><input type="radio" name="nfw_options[x_frame_options]" value="0"<?php checked( $nfw_options['response_headers'][2], 0 ); disabled($err, 1); ?>><?php echo $no . $default; ?></label><?php echo $err_msg ?></p></td>
		</tr>
		<tr>
			<th scope="row"><?php printf( __("Enforce %s (IE, Chrome and Safari browsers)", 'nfwplus'), '<code><a href="http://nintechnet.com/ninjafirewall/wp-edition/doc/#responseheaders" target="_blank">X-XSS-Protection</a></code>') ?></th>
			<td width="20"></td>
			<td align="left" width="120">
				<label><input type="radio" name="nfw_options[x_xss_protection]" value="1"<?php checked( $nfw_options['response_headers'][3], 1 ); disabled($err, 1); ?>><?php echo $yes . $default ?></label>
			</td>
			<td align="left">
				<label><input type="radio" name="nfw_options[x_xss_protection]" value="0"<?php checked( $nfw_options['response_headers'][3], 0 ); disabled($err, 1); ?>><?php echo $no; ?></label><?php echo $err_msg ?>
			</td>
		</tr>
		<tr>
			<th scope="row"><?php printf( __('Force %s flag on all cookies to mitigate XSS attacks', 'nfwplus'), '<code><a href="http://nintechnet.com/ninjafirewall/wp-edition/doc/#responseheaders" target="_blank">HttpOnly</a></code>') ?></th>
			<td width="20">&nbsp;</td>
			<td align="left" width="120" style="vertical-align:top;">
				<label><input type="radio" name="nfw_options[cookies_httponly]" value="1"<?php checked( $nfw_options['response_headers'][0], 1 ); disabled($err, 1); ?> >&nbsp;<?php echo $yes ?></label>
			</td>
			<td align="left" style="vertical-align:top;">
				<label><input type="radio" name="nfw_options[cookies_httponly]" value="0"<?php checked( $nfw_options['response_headers'][0], 0 ); disabled($err, 1); ?>>&nbsp;<?php echo $no . $default; ?></label><br /><span class="description"><?php _e('If your PHP scripts use cookies that need to be accessed from JavaScript, you should disable this option.', 'nfwplus') ?></span><?php echo $err_msg ?>
			</td>
		</tr>
		<?php
		// We don't send HSTS headers over HTTP (only display this message if there
		// is no other warning to display, $err==0 ):
		if ($_SERVER['SERVER_PORT'] != 443 && ! $err && (! isset( $_SERVER['HTTP_X_FORWARDED_PROTO']) || $_SERVER['HTTP_X_FORWARDED_PROTO'] != 'https') ) {
			$hsts_err = 1;
			$hsts_msg = '<br /><img src="' . plugins_url() . '/nfwplus/images/icon_warn_16.png" border="0" height="16" width="16">&nbsp;<span class="description">' . __('HSTS headers can only be set when you are accessing your site over HTTPS.', 'nfwplus') . '</span>';
		} else {
			$hsts_msg = '';
			$hsts_err = 0;
		}
		?>
		<tr>
			<th scope="row"><?php printf( __('Set %s (HSTS) to enforce secure connections to the server', 'nfwplus'), '<code><a href="http://nintechnet.com/ninjafirewall/wp-edition/doc/#responseheaders" target="_blank">Strict-Transport-Security</a></code>') ?></th>
			<td width="20">&nbsp;</td>
			<td align="left" width="120" style="vertical-align:top;">
				<p><label><input type="radio" name="nfw_options[strict_transport]" value="1"<?php checked( $nfw_options['response_headers'][4], 1 ); disabled($hsts_err, 1); ?>><?php _e('1 month', 'nfwplus') ?></label></p>
				<p><label><input type="radio" name="nfw_options[strict_transport]" value="2"<?php checked( $nfw_options['response_headers'][4], 2 ); disabled($hsts_err, 1); ?>><?php _e('6 months', 'nfwplus') ?></label></p>
				<p><label><input type="radio" name="nfw_options[strict_transport]" value="3"<?php checked( $nfw_options['response_headers'][4], 3 ); disabled($hsts_err, 1); ?>><?php _e('1 year', 'nfwplus') ?></label></p>
				<br />
				<label><input type="checkbox" name="nfw_options[strict_transport_sub]" value="1"<?php checked( $nfw_options['response_headers'][5], 1 ); disabled($hsts_err, 1); ?>><?php _e('Apply to subdomains', 'nfwplus') ?></label>
			</td>
			<td align="left" style="vertical-align:top;">
				<p><label><input type="radio" name="nfw_options[strict_transport]" value="0"<?php checked( $nfw_options['response_headers'][4], 0 ); disabled($hsts_err, 1); ?>><?php echo $no . $default; ?></label></p>
				<p><label><input type="radio" name="nfw_options[strict_transport]" value="4"<?php checked( $nfw_options['response_headers'][4], 4 ); disabled($hsts_err, 1); ?>><?php _e('Set <code>max-age</code> to 0', 'nfwplus'); ?></label><?php echo $err_msg ?></p>
				<?php echo $hsts_msg; ?>
			</td>
		</tr>

		<?php
			if (! isset( $nfw_options['csp_frontend_data'] ) ) {
				$nfw_options['csp_frontend_data'] = '';
			}
			if (! isset( $nfw_options['csp_backend_data'] ) ) {
				$nfw_options['csp_backend_data'] = nf_sub_policies_csp();
			}
			if (! isset( $nfw_options['response_headers'][6] ) ) {
				$nfw_options['response_headers'][6] = 0;
			}
			if (! isset( $nfw_options['response_headers'][7] ) ) {
				$nfw_options['response_headers'][7] = 0;
			}
		?>
		<tr>
			<th scope="row"><?php printf( __('Set %s for the website frontend', 'nfwplus'), '<code><a href="http://nintechnet.com/ninjafirewall/wp-edition/doc/#responseheaders" target="_blank">Content-Security-Policy</a></code>') ?></th>
			<td width="20">&nbsp;</td>
			<td align="left" width="120" style="vertical-align:top;">
				<p><label><input type="radio" onclick="csp_onoff(1, 'csp_frontend')" name="nfw_options[csp_frontend]" value="1"<?php checked( $nfw_options['response_headers'][6], 1 ); disabled($err, 1); ?>><?php _e('Yes', 'nfwplus') ?></label></p>
				<p><label><input type="radio" onclick="csp_onoff(0, 'csp_frontend')" name="nfw_options[csp_frontend]" value="0"<?php checked( $nfw_options['response_headers'][6], 0 ); disabled($err, 1); ?>><?php _e('No (default)', 'nfwplus') ?></label></p>
			</td>
			<td align="left" style="vertical-align:top;">
				<textarea autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" name="nfw_options[csp_frontend_data]" id="csp_frontend" class="large-text code" rows="4"<?php __checked_selected_helper($err, 1, true, 'readonly'); __checked_selected_helper($nfw_options['response_headers'][6], 0, true, 'readonly') ?>><?php echo htmlspecialchars( $nfw_options['csp_frontend_data'] ) ?></textarea>
				<span class="description"><?php _e('This CSP header will apply to the website frontend only.', 'nfwplus') ?></span>
				<?php echo $err_msg ?>
			</td>
		</tr>

		<tr>
			<th scope="row"><?php printf( __('Set %s for the WordPress admin dashboard', 'nfwplus'), '<code><a href="http://nintechnet.com/ninjafirewall/wp-edition/doc/#responseheaders" target="_blank">Content-Security-Policy</a></code>') ?></th>
			<td width="20">&nbsp;</td>
			<td align="left" width="120" style="vertical-align:top;">
				<p><label><input type="radio" onclick="csp_onoff(1, 'csp_backend')" name="nfw_options[csp_backend]" value="1"<?php checked( $nfw_options['response_headers'][7], 1 ); disabled($err, 1); ?>><?php _e('Yes', 'nfwplus') ?></label></p>
				<p><label><input type="radio" onclick="csp_onoff(0, 'csp_backend')" name="nfw_options[csp_backend]" value="0"<?php checked( $nfw_options['response_headers'][7], 0 ); disabled($err, 1); ?>><?php _e('No (default)', 'nfwplus') ?></label></p>
			</td>
			<td align="left" style="vertical-align:top;">
				<textarea autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" name="nfw_options[csp_backend_data]" id="csp_backend" class="large-text code" rows="4"<?php __checked_selected_helper($err, 1, true, 'readonly'); __checked_selected_helper($nfw_options['response_headers'][7], 0, true, 'readonly') ?>><?php echo htmlspecialchars( $nfw_options['csp_backend_data'] ) ?></textarea>
				<span class="description"><?php _e('This CSP header will apply to the WordPress admin dashboard only.', 'nfwplus') ?></span>
				<?php echo $err_msg ?>
			</td>
		</tr>

	</table>

	<br /><br />

<?php
if ( empty( $nfw_rules[NFW_WRAPPERS]['ena']) ) {
	$php_wrappers = 0;
} else {
	$php_wrappers = 1;
}
if ( empty( $nfw_rules[NFW_OBJECTS]['ena']) ) {
	$php_objects = 0;
} else {
	$php_objects = 1;
}
if ( empty( $nfw_options['php_errors']) ) {
	$php_errors = 0;
} else {
	$php_errors = 1;
}
if ( empty( $nfw_options['php_self']) ) {
	$php_self = 0;
} else {
	$php_self = 1;
}
if ( empty( $nfw_options['php_path_t']) ) {
	$php_path_t = 0;
} else {
	$php_path_t = 1;
}
if ( empty( $nfw_options['php_path_i']) ) {
	$php_path_i = 0;
} else {
	$php_path_i = 1;
}
?>
	<h3>PHP</h3>
	<table class="form-table">
		<tr>
			<th scope="row"><?php _e('Block PHP built-in wrappers', 'nfwplus') ?></th>
			<td width="20">&nbsp;</td>
			<td align="left" width="120">
				<label><input type="radio" name="nfw_rules[php_wrappers]" value="1"<?php checked( $php_wrappers, 1 ) ?>>&nbsp;<?php _e('Yes (default)', 'nfwplus') ?></label>
			</td>
			<td align="left">
				<label><input type="radio" name="nfw_rules[php_wrappers]" value="0"<?php checked( $php_wrappers, 0 ) ?>>&nbsp;<?php _e('No', 'nfwplus') ?></label>
			</td>
		</tr>
		<tr>
			<th scope="row"><?php _e('Block serialized PHP objects', 'nfwplus') ?></th>
			<td width="20">&nbsp;</td>
			<td align="left" width="120">
				<label><input type="radio" name="nfw_rules[php_objects]" value="1"<?php checked( $php_objects, 1 ) ?>>&nbsp;<?php _e('Yes', 'nfwplus') ?></label>
			</td>
			<td align="left">
				<label><input type="radio" name="nfw_rules[php_objects]" value="0"<?php checked( $php_objects, 0 ) ?>>&nbsp;<?php _e('No (default)', 'nfwplus') ?></label>
			</td>
		</tr>
		<tr>
			<th scope="row"><?php _e('Hide PHP notice and error messages', 'nfwplus') ?></th>
			<td width="20">&nbsp;</td>
			<td align="left" width="120">
				<label><input type="radio" name="nfw_options[php_errors]" value="1"<?php checked( $php_errors, 1 ) ?>>&nbsp;<?php _e('Yes (default)', 'nfwplus') ?></label>
			</td>
			<td align="left">
				<label><input type="radio" name="nfw_options[php_errors]" value="0"<?php checked( $php_errors, 0 ) ?>>&nbsp;<?php _e('No', 'nfwplus') ?></label>
			</td>
		</tr>
		<tr>
			<th scope="row"><?php _e('Sanitise <code>PHP_SELF</code>', 'nfwplus') ?></th>
			<td width="20">&nbsp;</td>
			<td align="left" width="120">
				<label><input type="radio" name="nfw_options[php_self]" value="1"<?php checked( $php_self, 1 ) ?>>&nbsp;<?php _e('Yes (default)', 'nfwplus') ?></label>
			</td>
			<td align="left">
				<label><input type="radio" name="nfw_options[php_self]" value="0"<?php checked( $php_self, 0 ) ?>>&nbsp;<?php _e('No', 'nfwplus') ?></label>
			</td>
		</tr>
		<tr>
			<th scope="row"><?php _e('Sanitise <code>PATH_TRANSLATED</code>', 'nfwplus') ?></th>
			<td width="20">&nbsp;</td>
			<td align="left" width="120">
				<label><input type="radio" name="nfw_options[php_path_t]" value="1"<?php checked( $php_path_t, 1 ) ?>>&nbsp;<?php _e('Yes (default)', 'nfwplus') ?></label>
			</td>
			<td align="left">
				<label><input type="radio" name="nfw_options[php_path_t]" value="0"<?php checked( $php_path_t, 0 ) ?>>&nbsp;<?php _e('No', 'nfwplus') ?></label>
			</td>
		</tr>
		<tr>
			<th scope="row"><?php _e('Sanitise <code>PATH_INFO</code>', 'nfwplus') ?></th>
			<td width="20">&nbsp;</td>
			<td align="left" width="120">
				<label><input type="radio" name="nfw_options[php_path_i]" value="1"<?php checked( $php_path_i, 1 ) ?>>&nbsp;<?php _e('Yes (default)', 'nfwplus') ?></label>
			</td>
			<td align="left">
				<label><input type="radio" name="nfw_options[php_path_i]" value="0"<?php checked( $php_path_i, 0 ) ?>>&nbsp;<?php _e('No', 'nfwplus') ?></label>
			</td>
		</tr>
	</table>

	<br />
	<br />

<?php

// If the document root is < 5 characters, grey out that option:
if ( strlen( $_SERVER['DOCUMENT_ROOT'] ) < 5 ) {
	$nfw_rules[NFW_DOC_ROOT]['ena'] = 0;
	$greyed = 'style="color:#bbbbbb"';
	$disabled = 'disabled ';
	$disabled_msg = '<br /><span class="description">&nbsp;This option is not compatible with your actual configuration.</span>';
} else {
	$greyed = '';
	$disabled = '';
	$disabled_msg = '';
}

if ( empty( $nfw_rules[NFW_DOC_ROOT]['ena']) ) {
	$block_doc_root = 0;
} else {
	$block_doc_root = 1;
}
if ( empty( $nfw_rules[NFW_NULL_BYTE]['ena']) ) {
	$block_null_byte = 0;
} else {
	$block_null_byte = 1;
}
if ( empty( $nfw_rules[NFW_ASCII_CTRL]['ena']) ) {
	$block_ctrl_chars = 0;
} else {
	$block_ctrl_chars = 1;
}
if ( empty( $nfw_rules[NFW_LOOPBACK]['ena']) ) {
	$no_localhost_ip = 0;
} else {
	$no_localhost_ip = 1;
}
if ( empty( $nfw_options['no_host_ip']) ) {
	$no_host_ip = 0;
} else {
	$no_host_ip = 1;
}
?>
	<h3><?php _e('Various', 'nfwplus') ?></h3>
	<table class="form-table">
		<tr valign="top">
			<th scope="row"><?php _e('Block the <code>DOCUMENT_ROOT</code> server variable in HTTP request', 'nfwplus') ?></th>
			<td width="20">&nbsp;</td>
			<td align="left" width="120">
				<label <?php echo $greyed ?>><input type="radio" name="nfw_rules[block_doc_root]" value="1"<?php checked( $block_doc_root, 1 ) ?>>&nbsp;<?php _e('Yes (default)', 'nfwplus') ?></label>
			</td>
			<td align="left">
				<label <?php echo $greyed ?>><input <?php echo $disabled ?>type="radio" name="nfw_rules[block_doc_root]" value="0"<?php checked( $block_doc_root, 0 ) ?>>&nbsp;<?php _e('No', 'nfwplus') ?></label><?php echo $disabled_msg ?>
			</td>
		</tr>
		<tr>
			<th scope="row"><?php _e('Block ASCII character 0x00 (NULL byte)', 'nfwplus') ?></th>
			<td width="20">&nbsp;</td>
			<td align="left" width="120">
				<label><input type="radio" name="nfw_rules[block_null_byte]" value="1"<?php checked( $block_null_byte, 1 ) ?>>&nbsp;<?php _e('Yes (default)', 'nfwplus') ?></label>
			</td>
			<td align="left">
				<label><input type="radio" name="nfw_rules[block_null_byte]" value="0"<?php checked( $block_null_byte, 0 ) ?>>&nbsp;<?php _e('No', 'nfwplus') ?></label>
			</td>
		</tr>
		<tr>
			<th scope="row"><?php _e('Block ASCII control characters 1 to 8 and 14 to 31', 'nfwplus') ?></th>
			<td width="20">&nbsp;</td>
			<td align="left">
				<label><input type="radio" name="nfw_rules[block_ctrl_chars]" value="1"<?php checked( $block_ctrl_chars, 1 ) ?>>&nbsp;<?php _e('Yes', 'nfwplus') ?></label>
			</td>
			<td align="left">
				<label><input type="radio" name="nfw_rules[block_ctrl_chars]" value="0"<?php checked( $block_ctrl_chars, 0 ) ?>>&nbsp;<?php _e('No (default)', 'nfwplus') ?></label>
			</td>
		</tr>
		<tr>
			<th scope="row"><?php _e('Block localhost IP in <code>GET/POST</code> request', 'nfwplus') ?></th>
			<td width="20">&nbsp;</td>
			<td align="left" width="120">
				<label><input type="radio" name="nfw_rules[no_localhost_ip]" value="1"<?php checked( $no_localhost_ip, 1 ) ?>>&nbsp;<?php _e('Yes (default)', 'nfwplus') ?></label>
			</td>
			<td align="left">
				<label><input type="radio" name="nfw_rules[no_localhost_ip]" value="0"<?php checked( $no_localhost_ip, 0 ) ?>>&nbsp;<?php _e('No', 'nfwplus') ?></label>
			</td>
		</tr>
		<tr>
			<th scope="row"><?php _e('Block HTTP requests with an IP in the <code>HTTP_HOST</code> header', 'nfwplus') ?></th>
			<td width="20">&nbsp;</td>
			<td align="left" width="120">
				<label><input type="radio" name="nfw_options[no_host_ip]" value="1"<?php checked( $no_host_ip, 1 ) ?>>&nbsp;<?php _e('Yes', 'nfwplus') ?></label>
			</td>
			<td align="left">
				<label><input type="radio" name="nfw_options[no_host_ip]" value="0"<?php checked( $no_host_ip, 0 ) ?>>&nbsp;<?php _e('No (default)', 'nfwplus') ?></label>
			</td>
		</tr>
	</table>

	<br />
	<br />

<?php

if ( @strpos( $nfw_options['wp_dir'], 'wp-admin' ) !== FALSE ) {
	$wp_admin = 1;
} else {
	$wp_admin = 0;
}
if ( @strpos( $nfw_options['wp_dir'], 'wp-includes' ) !== FALSE ) {
	$wp_inc = 1;
} else {
	$wp_inc = 0;
}
if ( @strpos( $nfw_options['wp_dir'], 'uploads' ) !== FALSE ) {
	$wp_upl = 1;
} else {
	$wp_upl = 0;
}
if ( @strpos( $nfw_options['wp_dir'], 'cache' ) !== FALSE ) {
	$wp_cache = 1;
} else {
	$wp_cache = 0;
}
if ( empty( $nfw_options['enum_archives']) ) {
	$enum_archives = 0;
} else {
	$enum_archives = 1;
}
if ( empty( $nfw_options['enum_login']) ) {
	$enum_login = 0;
} else {
	$enum_login = 1;
}
if ( empty( $nfw_options['enum_restapi']) ) {
	$enum_restapi = 0;
} else {
	$enum_restapi = 1;
}
if ( empty( $nfw_options['no_restapi']) ) {
	$no_restapi = 0;
} else {
	$no_restapi = 1;
}
if ( empty( $nfw_options['no_xmlrpc']) ) {
	$no_xmlrpc = 0;
} else {
	$no_xmlrpc = 1;
}
if ( empty( $nfw_options['no_xmlrpc_multi']) ) {
	$no_xmlrpc_multi = 0;
} else {
	$no_xmlrpc_multi = 1;
}
if ( empty( $nfw_options['no_xmlrpc_pingback']) ) {
	$no_xmlrpc_pingback = 0;
} else {
	$no_xmlrpc_pingback = 1;
}
if ( empty( $nfw_options['no_post_themes']) ) {
	$no_post_themes = 0;
} else {
	$no_post_themes = 1;
}

if ( empty( $nfw_options['force_ssl']) ) {
	$force_ssl = 0;
} else {
	$force_ssl = 1;
}
if ( empty( $nfw_options['disallow_edit']) ) {
	$disallow_edit = 0;
} else {
	$disallow_edit = 1;
}
if ( empty( $nfw_options['disallow_mods']) ) {
	$disallow_mods = 0;
} else {
	$disallow_mods = 1;
}

// We must disable these 2 options if their constants are already defined
// (e.g., in the wp-config.php). Note that even if they are defined as 'false',
// we must disable them because NinjaFirewall would define them first, and
// then WordPress would try as well. We would end up with a PHP error:
$force_ssl_already_enabled = $disallow_edit_already_enabled = $disallow_mods_already_enabled = 0;
if ( defined('DISALLOW_FILE_EDIT') && ! $disallow_edit ) {
	$disallow_edit_already_enabled = 1;
}
if ( defined('DISALLOW_FILE_MODS') && ! $disallow_mods ) {
	$disallow_mods_already_enabled = 1;
}
// Because FORCE_SSL_ADMIN is always defined by WP (see wp-includes/default_constants.php),
// we disable this option only if it is set to 'true':
if ( defined('FORCE_SSL_ADMIN') && FORCE_SSL_ADMIN == true && ! $force_ssl ) {
	$force_ssl_already_enabled = 1;
}
?>
	<a name="wpdir"></a>
	<h3>WordPress</h3>
	<table class="form-table">
		<tr>
			<th scope="row"><?php
				_e('Block direct access to any PHP file located in one of these directories', 'nfwplus');
				if ( defined('NFW_WPWAF') ) {
					echo '<br /><font style="font-weight:400">' . $full_waf_msg . '</font>';
				}
			?></th>
			<td width="20">&nbsp;</td>
			<td align="left">
				<table class="form-table">
					<tr style="border: solid 1px #DFDFDF;">
						<td align="center" width="10"><input type="checkbox" name="nfw_options[wp_admin]" id="wp_01"<?php checked( $wp_admin, 1 ); disabled( $option_disabled, 1) ?>></td>
						<td>
						<label for="wp_01">
						<p><code>/wp-admin/css/*</code></p>
						<p><code>/wp-admin/images/*</code></p>
						<p><code>/wp-admin/includes/*</code></p>
						<p><code>/wp-admin/js/*</code></p>
						</label>
						</td>
					</tr>
					<tr style="border: solid 1px #DFDFDF;">
						<td align="center" width="10"><input type="checkbox" name="nfw_options[wp_inc]" id="wp_02"<?php checked( $wp_inc, 1 ); disabled( $option_disabled, 1) ?>></td>
						<td>
						<label for="wp_02">
						<p><code>/wp-includes/*.php</code></p>
						<p><code>/wp-includes/css/*</code></p>
						<p><code>/wp-includes/images/*</code></p>
						<p><code>/wp-includes/js/*</code></p>
						<p><code>/wp-includes/theme-compat/*</code></p>
						</label>
						<br />
						<span class="description"><?php _e('NinjaFirewall will not block access to the TinyMCE WYSIWYG editor even if this option is enabled.', 'nfwplus') ?></span>
						</td>
					</tr>
					<tr style="border: solid 1px #DFDFDF;">
						<td align="center" width="10"><input type="checkbox" name="nfw_options[wp_upl]" id="wp_03"<?php checked( $wp_upl, 1 ); disabled( $option_disabled, 1) ?>></td>
						<td><label for="wp_03">
							<p><code>/<?php echo basename(WP_CONTENT_DIR); ?>/uploads/*</code></p>
							<p><code>/<?php echo basename(WP_CONTENT_DIR); ?>/blogs.dir/*</code></p>
						</label></td>
					</tr>
					<tr style="border: solid 1px #DFDFDF;">
						<td align="center" style="vertical-align:top" width="10"><input type="checkbox" name="nfw_options[wp_cache]" id="wp_04"<?php checked( $wp_cache, 1 ); disabled( $option_disabled, 1) ?>></td>
						<td style="vertical-align:top"><label for="wp_04"><code>*/cache/*</code></label>
						<br />
						<br />
						<span class="description"><?php _e('Unless you have PHP scripts in a "/cache/" folder that need to be accessed by your visitors, we recommend to enable this option.', 'nfwplus') ?></span>
						</td>
					</tr>
				</table>
				<br />&nbsp;
			</td>
		</tr>
	</table>

	<?php
	if ( is_dir( WP_PLUGIN_DIR . '/jetpack' ) ) {
		$is_JetPack = '<p><img src="' . plugins_url() . '/nfwplus/images/icon_warn_16.png" border="0" height="16" width="16">&nbsp;<span class="description">' . __('If you are using the Jetpack plugin, blocking <code>system.multicall</code> may prevent it from working correctly.', 'nfwplus') . '</span></p>';
	} else {
		$is_JetPack = '';
	}
	?>

	<table class="form-table">
		<tr>
			<th scope="row"><?php _e('Protect against username enumeration', 'nfwplus') ?></th>
			<td width="20">&nbsp;</td>
			<td align="left">
				<p><label><input type="checkbox" name="nfw_options[enum_archives]" value="1"<?php checked( $enum_archives, 1 ) ?>>&nbsp;<?php _e('Through the author archives', 'nfwplus') ?></label></p>
				<p><label><input type="checkbox" name="nfw_options[enum_login]" value="1"<?php checked( $enum_login, 1 ) ?>>&nbsp;<?php _e('Through the login page', 'nfwplus') ?></label></p>
				<p><label><input type="checkbox" name="nfw_options[enum_restapi]" value="1"<?php checked( $enum_restapi, 1 ) ?>>&nbsp;<?php _e('Through the WordPress REST API', 'nfwplus') ?></label></p>
			</td>
		</tr>

		<?php
		global $wp_version;
		if ( version_compare( $wp_version, '4.7', '<' ) ) {
			$restapi_error = '1';
			$restapi_msg = '<p><img src="' . plugins_url() . '/nfwplus/images/icon_warn_16.png" border="0" height="16" width="16">&nbsp;<span class="description">' . __('This feature is only available when running WordPress 4.7 or above.', 'nfwplus') . '</span></p>';
		} else {
			$restapi_msg = '';
			$restapi_error = 0;
		}
		?>
		<tr>
			<th scope="row"><?php _e('WordPress REST API', 'nfwplus') ?>*</th>
			<td width="20">&nbsp;</td>
			<td align="left">
				<p><label><input type="checkbox" name="nfw_options[no_restapi]" value="1"<?php checked( $no_restapi, 1 );disabled( $restapi_error, 1) ?>>&nbsp;<?php _e('Block any access to the API', 'nfwplus') ?></label></p>
				<?php echo $restapi_msg; ?>
			</td>
		</tr>

		<tr>
			<th scope="row"><?php _e('WordPress XML-RPC API', 'nfwplus') ?>*</th>
			<td width="20">&nbsp;</td>
			<td align="left">
				<p><label><input type="checkbox" name="nfw_options[no_xmlrpc]" value="1"<?php checked( $no_xmlrpc, 1 ) ?>>&nbsp;<?php _e('Block any access to the API', 'nfwplus') ?></label></p>
				<p><label><input type="checkbox" name="nfw_options[no_xmlrpc_multi]" value="1"<?php checked( $no_xmlrpc_multi, 1 ) ?>>&nbsp;<?php _e('Block <code>system.multicall</code> method', 'nfwplus') ?></label></p>
				<?php echo $is_JetPack; ?>
				<p><label><input type="checkbox" name="nfw_options[no_xmlrpc_pingback]" value="1"<?php checked( $no_xmlrpc_pingback, 1 ) ?>>&nbsp;<?php _e('Block Pingbacks', 'nfwplus') ?></label></p>
			</td>
		</tr>
	</table>

	<span class="description">*<?php _e('Disabling access to the REST or XML-RPC API may break some functionality on your blog, its themes or plugins.', 'nfwplus') ?></span>

	<table class="form-table">
		<tr valign="top">
			<th scope="row" style="vertical-align:top"><?php _e('Block <code>POST</code> requests in the themes folder', 'nfwplus') ?> <code>/<?php echo basename(WP_CONTENT_DIR); ?>/themes</code></th>
			<td width="20">&nbsp;</td>
			<td align="left" width="120" style="vertical-align:top">
				<label><input type="radio" name="nfw_options[no_post_themes]" value="1"<?php checked( $no_post_themes, 1 ); disabled( $option_disabled, 1) ?>>&nbsp;<?php _e('Yes', 'nfwplus') ?></label>
			</td>
			<td align="left" style="vertical-align:top">
				<label><input type="radio" name="nfw_options[no_post_themes]" value="0"<?php checked( $no_post_themes, 0 ); disabled( $option_disabled, 1) ?>>&nbsp;<?php _e('No (default)', 'nfwplus') ?></label>
				<?php
				if ( defined('NFW_WPWAF') ) {
					echo '<br />'. $full_waf_msg;
				}
			?>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row"><a name="builtinconstants"></a><?php _e('Force SSL for admin and logins', 'nfwplus') ?> <code><a href="http://codex.wordpress.org/Editing_wp-config.php#Require_SSL_for_Admin_and_Logins" target="_blank">FORCE_SSL_ADMIN</a></code></th>
			<td width="20">&nbsp;</td>
			<td align="left" width="120">
				<label><input type="radio" name="nfw_options[force_ssl]" value="1"<?php checked( $force_ssl, 1 ) ?> onclick="return ssl_warn();" <?php disabled( $force_ssl_already_enabled, 1 ) ?>>&nbsp;<?php _e('Yes', 'nfwplus') ?></label>
			</td>
			<td align="left">
				<label><input type="radio" id="ssl_0" name="nfw_options[force_ssl]" value="0"<?php checked( $force_ssl, 0 ) ?> <?php disabled( $force_ssl_already_enabled, 1 ) ?>>&nbsp;<?php _e('No (default)', 'nfwplus') ?></label>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row"><?php _e('Disable the plugin and theme editor', 'nfwplus') ?> <code><a href="http://codex.wordpress.org/Editing_wp-config.php#Disable_the_Plugin_and_Theme_Editor" target="_blank">DISALLOW_FILE_EDIT</a></code></th>
			<td width="20">&nbsp;</td>
			<td align="left" width="120">
				<label><input type="radio" name="nfw_options[disallow_edit]" value="1"<?php checked( $disallow_edit, 1 ) ?> <?php disabled( $disallow_edit_already_enabled, 1 ) ?>>&nbsp;<?php _e('Yes', 'nfwplus') ?></label>
			</td>
			<td align="left">
				<label><input type="radio" name="nfw_options[disallow_edit]" value="0"<?php checked( $disallow_edit, 0 ) ?> <?php disabled( $disallow_edit_already_enabled, 1 ) ?>>&nbsp;<?php _e('No (default)', 'nfwplus') ?></label>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row"><?php _e('Disable plugin and theme update/installation', 'nfwplus') ?> <code><a href="http://codex.wordpress.org/Editing_wp-config.php#Disable_Plugin_and_Theme_Update_and_Installation" target="_blank">DISALLOW_FILE_MODS</a></code></th>
			<td width="20">&nbsp;</td>
			<td align="left" width="120">
				<label><input type="radio" name="nfw_options[disallow_mods]" value="1"<?php checked( $disallow_mods, 1 ) ?> <?php disabled( $disallow_mods_already_enabled, 1 ) ?>>&nbsp;<?php _e('Yes', 'nfwplus') ?></label>
			</td>
			<td align="left">
				<label><input type="radio" name="nfw_options[disallow_mods]" value="0"<?php checked( $disallow_mods, 0 ) ?> <?php disabled( $disallow_mods_already_enabled, 1 ) ?>>&nbsp;<?php _e('No (default)', 'nfwplus') ?></label>
			</td>
		</tr>

	</table>

	<br />
	<br />
	<br />
	<input class="button-primary" type="submit" name="Save" value="<?php _e('Save Firewall Policies', 'nfwplus') ?>" />
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<input class="button-secondary" type="submit" name="Default" value="<?php _e('Restore Default Values', 'nfwplus') ?>" onclick="return restore();" />
	</form>
</div>

<?php

/* ================================================================== */

function nf_sub_policies_save() {

	// Save policies :

	// Block immediately if user is not allowed :
	nf_not_allowed( 'block', __LINE__ );

	$nfw_options = nfw_get_option( 'nfw_options' );
	$nfw_rules = nfw_get_option( 'nfw_rules' );

	// Options

	// HTTP/S traffic to scan :
	if ( (isset( $_POST['nfw_options']['scan_protocol'])) &&
		( preg_match( '/^[123]$/', $_POST['nfw_options']['scan_protocol'])) ) {
			$nfw_options['scan_protocol'] = $_POST['nfw_options']['scan_protocol'];
	} else {
		// Default : HTTP + HTTPS
		$nfw_options['scan_protocol'] = 3;
	}

	// Allow uploads ?
	if ( isset( $_POST['nfw_options']['uploads']) && preg_match( '/^[12]$/', $_POST['nfw_options']['uploads']) ) {
			$nfw_options['uploads'] = $_POST['nfw_options']['uploads'];
	} else {
		$nfw_options['uploads'] = 0;
	}
	// Sanitise filenames (if uploads are allowed) ?
	if ( isset( $_POST['nfw_options']['sanitise_fn']) ) {
		$nfw_options['sanitise_fn'] = 1;
	} else {
		$nfw_options['sanitise_fn'] = 0;
	}
	// Substitution character:
	if ( empty( $_POST['nfw_options']['substitute'] ) || strlen( $_POST['nfw_options']['substitute'] ) > 1 ) {
		$nfw_options['substitute'] = 'X';
	} else {
		$nfw_options['substitute'] = $_POST['nfw_options']['substitute'];
	}
	// Max file size :
	if ( isset($_POST['nfw_options']['upload_maxsize']) && preg_match( '/^\d+$/', $_POST['nfw_options']['upload_maxsize']) ) {
		$nfw_options['upload_maxsize'] = $_POST['nfw_options']['upload_maxsize'] * 1024;
	} else {
		// Default : 1,048,576 bytes (1 MB)
		$nfw_options['upload_maxsize'] = 1048576;
	}


	// Scan GET requests ?
	if ( empty( $_POST['nfw_options']['get_scan']) ) {
		$nfw_options['get_scan'] = 0;
	} else {
		// Default: yes
		$nfw_options['get_scan'] = 1;
	}
	// Sanitise GET requests ?
	if ( empty( $_POST['nfw_options']['get_sanitise']) ) {
		// Default: no
		$nfw_options['get_sanitise'] = 0;
	} else {
		$nfw_options['get_sanitise'] = 1;
	}


	// Scan POST requests ?
	if ( empty( $_POST['nfw_options']['post_scan']) ) {
		$nfw_options['post_scan'] = 0;
	} else {
		// Default: yes
		$nfw_options['post_scan'] = 1;
	}
	// Sanitise POST requests ?
	if ( empty( $_POST['nfw_options']['post_sanitise']) ) {
		// Default: no
		$nfw_options['post_sanitise'] = 0;
	} else {
		$nfw_options['post_sanitise'] = 1;
	}
	// Decode base64 values in POST requests ?
	if ( empty( $_POST['nfw_options']['post_b64']) ) {
		$nfw_options['post_b64'] = 0;
	} else {
		// Default: yes
		$nfw_options['post_b64'] = 1;
	}


	// HTTP response headers:
	if ( function_exists('header_register_callback') && function_exists('headers_list') && function_exists('header_remove') ) {
		$nfw_options['response_headers'] = '00000000';
		$nfw_options['csp_frontend_data'] = '';
		$nfw_options['csp_backend_data'] = '';
		// X-Content-Type-Options
		if ( empty( $_POST['nfw_options']['x_content_type_options']) ) {
			$nfw_options['response_headers'][1] = 0;
		} else {
			$nfw_options['response_headers'][1] = 1;
		}
		// X-Frame-Options
		if ( empty( $_POST['nfw_options']['x_frame_options']) ) {
			$nfw_options['response_headers'][2] = 0;
		} elseif ( $_POST['nfw_options']['x_frame_options'] == 1) {
			$nfw_options['response_headers'][2] = 1;
		} else {
			$nfw_options['response_headers'][2] = 2;
		}
		// X-XSS-Protection
		if ( empty( $_POST['nfw_options']['x_xss_protection']) ) {
			$nfw_options['response_headers'][3] = 0;
		} else {
			$nfw_options['response_headers'][3] = 1;
		}
		// HttpOnly cookies ?
		if ( empty( $_POST['nfw_options']['cookies_httponly']) ) {
			$nfw_options['response_headers'][0] = 0;
		} else {
			$nfw_options['response_headers'][0] = 1;
		}
		// Strict-Transport-Security ?
		if (! isset( $_POST['nfw_options']['strict_transport_sub']) ) {
			$nfw_options['response_headers'][5] = 0;
		} else {
			$nfw_options['response_headers'][5] = 1;
		}
		if ( empty( $_POST['nfw_options']['strict_transport']) ) {
			$nfw_options['response_headers'][4] = 0;
			$nfw_options['response_headers'][5] = 0;
		} elseif ( $_POST['nfw_options']['strict_transport'] == 1) {
			$nfw_options['response_headers'][4] = 1;
		} elseif ( $_POST['nfw_options']['strict_transport'] == 2) {
			$nfw_options['response_headers'][4] = 2;
		} elseif ( $_POST['nfw_options']['strict_transport'] == 3) {
			$nfw_options['response_headers'][4] = 3;
		} else {
			$nfw_options['response_headers'][4] = 4;
		}

		// Content-Security-Policy (frontend/backend) ?
		$nfw_options['csp_frontend_data'] = stripslashes( str_replace( array( '<', '>', "\x0a", "\x0d", '%', '$', '&') , '', $_POST['nfw_options']['csp_frontend_data'] ) );
		if ( empty( $_POST['nfw_options']['csp_frontend']) || empty( $nfw_options['csp_frontend_data'] ) ) {
			$nfw_options['response_headers'][6] = 0;
		} else {
			$nfw_options['response_headers'][6] = 1;
		}

		$nfw_options['csp_backend_data'] = stripslashes( str_replace( array( '<', '>', "\x0a", "\x0d", '%', '$', '&') , '', $_POST['nfw_options']['csp_backend_data'] ) );
		if ( empty( $_POST['nfw_options']['csp_backend']) || empty( $nfw_options['csp_backend_data'] ) ) {
			$nfw_options['response_headers'][7] = 0;
		} else {
			$nfw_options['response_headers'][7] = 1;
		}
	}

	// Sanitise REQUEST requests ?
	if ( empty( $_POST['nfw_options']['request_sanitise']) ) {
		// Default: yes
		$nfw_options['request_sanitise'] = 0;
	} else {
		$nfw_options['request_sanitise'] = 1;
	}


	// Scan COOKIES requests ?
	if ( empty( $_POST['nfw_options']['cookies_scan']) ) {
		// Default:no
		$nfw_options['cookies_scan'] = 0;
	} else {
		$nfw_options['cookies_scan'] = 1;
	}
	// Sanitise COOKIES requests ?
	if ( empty( $_POST['nfw_options']['cookies_sanitise']) ) {
		$nfw_options['cookies_sanitise'] = 0;
	} else {
		// Default: yes
		$nfw_options['cookies_sanitise'] = 1;
	}


	// Scan HTTP_USER_AGENT requests ?
	if ( empty( $_POST['nfw_options']['ua_scan']) ) {
		$nfw_options['ua_scan'] = 0;
	} else {
		// Default: yes
		$nfw_options['ua_scan'] = 1;
	}
	// Sanitise HTTP_USER_AGENT requests ?
	if ( empty( $_POST['nfw_options']['ua_sanitise']) ) {
		$nfw_options['ua_sanitise'] = 0;
	} else {
		// Default: yes
		$nfw_options['ua_sanitise'] = 1;
	}


	// Scan HTTP_REFERER requests ?
	if ( empty( $_POST['nfw_options']['referer_scan']) ) {
		// Default: no
		$nfw_options['referer_scan'] = 0;
	} else {
		$nfw_options['referer_scan'] = 1;
	}
	// Sanitise HTTP_REFERER requests ?
	if ( empty( $_POST['nfw_options']['referer_sanitise']) ) {
		$nfw_options['referer_sanitise'] = 0;
	} else {
		// Default: yes
		$nfw_options['referer_sanitise'] = 1;
	}
	// Block POST requests without HTTP_REFERER ?
	if ( empty( $_POST['nfw_options']['referer_post']) ) {
		// Default: NO
		$nfw_options['referer_post'] = 0;
	} else {
		$nfw_options['referer_post'] = 1;
	}

	// Block HTTP requests with an IP in the Host header ?
	if ( empty( $_POST['nfw_options']['no_host_ip']) ) {
		// Default: NO
		$nfw_options['no_host_ip'] = 0;
	} else {
		$nfw_options['no_host_ip'] = 1;
	}

	// Hide PHP notice & error messages :
	if ( empty( $_POST['nfw_options']['php_errors']) ) {
		$nfw_options['php_errors'] = 0;
	} else {
		// Default: yes
		$nfw_options['php_errors'] = 1;
	}

	// Sanitise PHP_SELF ?
	if ( empty( $_POST['nfw_options']['php_self']) ) {
		$nfw_options['php_self'] = 0;
	} else {
		// Default: yes
		$nfw_options['php_self'] = 1;
	}
	// Sanitise PATH_TRANSLATED ?
	if ( empty( $_POST['nfw_options']['php_path_t']) ) {
		$nfw_options['php_path_t'] = 0;
	} else {
		// Default: yes
		$nfw_options['php_path_t'] = 1;
	}
	// Sanitise PATH_INFO ?
	if ( empty( $_POST['nfw_options']['php_path_i']) ) {
		$nfw_options['php_path_i'] = 0;
	} else {
		// Default: yes
		$nfw_options['php_path_i'] = 1;
	}

	// WordPress directories PHP restrictions :
	$nfw_options['wp_dir'] = $tmp = '';
	if ( isset( $_POST['nfw_options']['wp_admin']) ) {
		$tmp .= '/wp-admin/(?:css|images|includes|js)/|';
	}
	if ( isset( $_POST['nfw_options']['wp_inc']) ) {
		$tmp .= '/wp-includes/(?:(?:css|images|js(?!/tinymce/wp-tinymce\.php)|theme-compat)/|[^/]+\.php)|';
	}
	if ( isset( $_POST['nfw_options']['wp_upl']) ) {
		$tmp .= '/' . basename(WP_CONTENT_DIR) .'/(?:uploads|blogs\.dir)/|';
	}
	if ( isset( $_POST['nfw_options']['wp_cache']) ) {
		$tmp .= '/cache/|';
	}
	if ( $tmp ) {
		$nfw_options['wp_dir'] = rtrim( $tmp, '|' );
	}

	// Protect against username enumeration attempts ?
	if (! isset( $_POST['nfw_options']['enum_archives']) ) {
		// Default : no
		$nfw_options['enum_archives'] = 0;
	} else {
		$nfw_options['enum_archives'] = 1;
	}
	if (! isset( $_POST['nfw_options']['enum_login']) ) {
		// Default : no
		$nfw_options['enum_login'] = 0;
	} else {
		$nfw_options['enum_login'] = 1;
	}
	if (! isset( $_POST['nfw_options']['enum_restapi']) ) {
		$nfw_options['enum_restapi'] = 0;
	} else {
		$nfw_options['enum_restapi'] = 1;
	}
	if (! isset( $_POST['nfw_options']['no_restapi']) ) {
		$nfw_options['no_restapi'] = 0;
	} else {
		$nfw_options['no_restapi'] = 1;
	}

	// Block WordPress XML-RPC API ?
	if ( empty( $_POST['nfw_options']['no_xmlrpc']) ) {
		// Default : no
		$nfw_options['no_xmlrpc'] = 0;
	} else {
		$nfw_options['no_xmlrpc'] = 1;
		// Clear no_xmlrpc_multi and no_xmlrpc_pingback as
		// they will be blocked anyway !
		$_POST['nfw_options']['no_xmlrpc_multi'] = 0;
		$_POST['nfw_options']['no_xmlrpc_pingback'] = 0;
	}
	if ( empty( $_POST['nfw_options']['no_xmlrpc_multi']) ) {
		// Default : no
		$nfw_options['no_xmlrpc_multi'] = 0;
	} else {
		$nfw_options['no_xmlrpc_multi'] = 1;
	}
	if ( empty( $_POST['nfw_options']['no_xmlrpc_pingback']) ) {
		// Default : no
		$nfw_options['no_xmlrpc_pingback'] = 0;
	} else {
		$nfw_options['no_xmlrpc_pingback'] = 1;
	}

	// Block POST requests in the themes folder ?
	if ( empty( $_POST['nfw_options']['no_post_themes']) ) {
		// Default : no
		$nfw_options['no_post_themes'] = 0;
	} else {
		$nfw_options['no_post_themes'] = '/'. basename(WP_CONTENT_DIR) .'/themes/';
	}

	// Force SSL for admin and logins ?
	if ( empty( $_POST['nfw_options']['force_ssl']) ) {
		// Default : no
		$nfw_options['force_ssl'] = 0;
	} else {
		$nfw_options['force_ssl'] = 1;
	}

	// Disable the plugin and theme editor
	if ( empty( $_POST['nfw_options']['disallow_edit']) ) {
		// Default : no
		$nfw_options['disallow_edit'] = 0;
	} else {
		$nfw_options['disallow_edit'] = 1;
	}

	// Disable plugin and theme update/installation
	if ( empty( $_POST['nfw_options']['disallow_mods']) ) {
		// Default : no
		$nfw_options['disallow_mods'] = 0;
	} else {
		$nfw_options['disallow_mods'] = 1;
	}

	// Rules

	// Block NULL byte 0x00 (#ID 2) :
	if ( empty( $_POST['nfw_rules']['block_null_byte']) ) {
		$nfw_rules[NFW_NULL_BYTE]['ena'] = 0;
	} else {
		// Default: yes
		$nfw_rules[NFW_NULL_BYTE]['ena'] = 1;
	}

	// Block ASCII control characters 1 to 8 and 14 to 31 (#ID 500) :
	if ( empty( $_POST['nfw_rules']['block_ctrl_chars']) ) {
		// Default: no
		$nfw_rules[NFW_ASCII_CTRL]['ena'] = 0;
	} else {
		$nfw_rules[NFW_ASCII_CTRL]['ena'] = 1;
	}


	// Block the DOCUMENT_ROOT server variable in GET/POST requests (#ID 510) :
	if ( empty( $_POST['nfw_rules']['block_doc_root']) ) {
		$nfw_rules[NFW_DOC_ROOT]['ena'] = 0;
	} else {
		// Default: yes

		// We need to ensure that the document root is at least
		// 5 characters, otherwise this option could block a lot
		// of legitimate requests:
		if ( strlen( $_SERVER['DOCUMENT_ROOT'] ) > 5 ) {
			$nfw_rules[NFW_DOC_ROOT]['cha'][1]['wha'] = str_replace( '/', '/[./]*', $_SERVER['DOCUMENT_ROOT'] );
			$nfw_rules[NFW_DOC_ROOT]['ena']	= 1;
		} elseif ( strlen( getenv( 'DOCUMENT_ROOT' ) ) > 5 ) {
			$nfw_rules[NFW_DOC_ROOT]['cha'][1]['wha'] = str_replace( '/', '/[./]*', getenv( 'DOCUMENT_ROOT' ) );
			$nfw_rules[NFW_DOC_ROOT]['ena']	= 1;
		// we must disable that option:
		} else {
			$nfw_rules[NFW_DOC_ROOT]['ena']	= 0;
		}
	}

	// Block PHP built-in wrappers (#ID 520) :
	if ( empty( $_POST['nfw_rules']['php_wrappers']) ) {
		$nfw_rules[NFW_WRAPPERS]['ena'] = 0;
	} else {
		// Default: yes
		$nfw_rules[NFW_WRAPPERS]['ena'] = 1;
	}
	// Block serialized PHP objects
	if ( empty( $_POST['nfw_rules']['php_objects']) ) {
		// Default: no
		$nfw_rules[NFW_OBJECTS]['ena'] = 0;
	} else {
		$nfw_rules[NFW_OBJECTS]['ena'] = 1;
	}
	// Block localhost IP in GET/POST requests (#ID 540) :
	if ( empty( $_POST['nfw_rules']['no_localhost_ip']) ) {
		$nfw_rules[NFW_LOOPBACK]['ena'] = 0;
	} else {
		// Default: yes
		$nfw_rules[NFW_LOOPBACK]['ena'] = 1;
	}

	// Save option + rules :
	nfw_update_option( 'nfw_options', $nfw_options );
	nfw_update_option( 'nfw_rules', $nfw_rules );

}

/* ================================================================== */

function nf_sub_policies_csp() {
	return "script-src 'self' 'unsafe-inline' 'unsafe-eval' *.videopress.com *.google.com *.wp.com; style-src 'self' 'unsafe-inline' *.googleapis.com *.google.com *.jquery.com; connect-src 'self'; media-src 'self' *.youtube.com *.w.org; child-src 'self' *.videopress.com *.google.com; object-src 'self'; form-action 'self'; img-src 'self' *.gravatar.com *.wp.com *.w.org *.cldup.com woocommerce.com data:;";
}

/* ================================================================== */

function nf_sub_policies_default() {

	// Restore default firewall policies :

	// Block immediately if user is not allowed :
	nf_not_allowed( 'block', __LINE__ );

	$nfw_options = nfw_get_option( 'nfw_options' );
	$nfw_rules = nfw_get_option( 'nfw_rules' );

	$nfw_options['scan_protocol']		= 3;
	$nfw_options['uploads']				= 0;
	$nfw_options['sanitise_fn']		= 0;
	$nfw_options['substitute'] 		= 'X';
	$nfw_options['upload_maxsize'] 	= 1048576;
	$nfw_options['get_scan']			= 1;
	$nfw_options['get_sanitise']		= 0;
	$nfw_options['post_scan']			= 1;
	$nfw_options['post_sanitise']		= 0;
	$nfw_options['request_sanitise'] = 0;
	if ( function_exists('header_register_callback') && function_exists('headers_list') && function_exists('header_remove') ) {
		$nfw_options['response_headers'] = '00010000';
		$nfw_options['csp_backend_data'] = nf_sub_policies_csp();
		$nfw_options['csp_frontend_data'] = '';
	}
	$nfw_options['cookies_scan']		= 1;
	$nfw_options['cookies_sanitise']	= 0;
	$nfw_options['ua_scan']				= 1;
	$nfw_options['ua_sanitise']		= 1;
	$nfw_options['referer_scan']		= 0;
	$nfw_options['referer_sanitise']	= 1;
	$nfw_options['referer_post']		= 0;
	$nfw_options['no_host_ip']			= 0;
	$nfw_options['php_errors']			= 1;
	$nfw_options['php_self']			= 1;
	$nfw_options['php_path_t']			= 1;
	$nfw_options['php_path_i']			= 1;
	$nfw_options['wp_dir'] 				= '/wp-admin/(?:css|images|includes|js)/|' .
		'/wp-includes/(?:(?:css|images|js(?!/tinymce/wp-tinymce\.php)|theme-compat)/|[^/]+\.php)|' .
		'/'. basename(WP_CONTENT_DIR) .'/(?:uploads|blogs\.dir)/';
	$nfw_options['enum_archives']		= 0;
	$nfw_options['enum_login']			= 0;
	$nfw_options['enum_restapi']		= 0;
	$nfw_options['no_restapi']			= 0;
	$nfw_options['no_xmlrpc']			= 0;
	$nfw_options['no_xmlrpc_multi']	= 0;
	$nfw_options['no_xmlrpc_pingback']= 0;
	$nfw_options['no_post_themes']	= 0;
	$nfw_options['force_ssl'] 			= 0;
	$nfw_options['disallow_edit'] 	= 0;
	$nfw_options['disallow_mods'] 	= 0;
	$nfw_options['post_b64']			= 1;
	$nfw_rules[NFW_LOOPBACK]['ena']	= 1;
	$nfw_rules[NFW_WRAPPERS]['ena']	= 1;
	$nfw_rules[NFW_OBJECTS]['ena']	= 0;

	if ( strlen( $_SERVER['DOCUMENT_ROOT'] ) > 5 ) {
		$nfw_rules[NFW_DOC_ROOT]['cha'][1]['wha'] = str_replace( '/', '/[./]*', $_SERVER['DOCUMENT_ROOT'] );
		$nfw_rules[NFW_DOC_ROOT]['ena'] = 1;
	} elseif ( strlen( getenv( 'DOCUMENT_ROOT' ) ) > 5 ) {
		$nfw_rules[NFW_DOC_ROOT]['cha'][1]['wha'] = str_replace( '/', '/[./]*', getenv( 'DOCUMENT_ROOT' ) );
		$nfw_rules[NFW_DOC_ROOT]['ena'] = 1;
	} else {
		$nfw_rules[NFW_DOC_ROOT]['ena']  = 0;
	}

	$nfw_rules[NFW_NULL_BYTE]['ena']  = 1;
	$nfw_rules[NFW_ASCII_CTRL]['ena'] = 0;

	nfw_update_option( 'nfw_options', $nfw_options);
	nfw_update_option( 'nfw_rules', $nfw_rules);

}

/* ================================================================== */
// EOF
