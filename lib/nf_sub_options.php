<?php
/*
 +=====================================================================+
 | NinjaFirewall (WP+ Edition)                                         |
 |                                                                     |
 | (c) NinTechNet - http://nintechnet.com/                             |
 +=====================================================================+ i18n+ / sa
*/

if (! defined( 'NFW_ENGINE_VERSION' ) ) { die( 'Forbidden' ); }

// Block immediately if user is not allowed :
nf_not_allowed( 'block', __LINE__ );

$nfw_options = nfw_get_option( 'nfw_options' );

// try to find MySQL charset :
@$nfw_mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
$charset = @$nfw_mysqli->character_set_name();
@$nfw_mysqli->close();

echo '
<script>
function preview_msg() {
	var t1 = document.option_form.elements[\'nfw_options[blocked_msg]\'].value.replace(\'%%REM_ADDRESS%%\',\'' . htmlspecialchars( NFW_REMOTE_ADDR ) . '\');
	var t2 = t1.replace(\'%%NUM_INCIDENT%%\',\'1234567\');
	var t3 = t2.replace(\'%%NINJA_LOGO%%\',\'<img src="' . plugins_url() . '/nfwplus/images/ninjafirewall_75.png" width="75" height="75" title="NinjaFirewall">\');
	var ns;
	if ( t3.match(/<style/i) ) {
		ns = "'. esc_js( __('CSS style sheets', 'nfwplus') ) .'";
	}
	if ( t3.match(/<script/i) ) {
		ns = "'. esc_js( __('Javascript code', 'nfwplus') ) .'";
	}
	if ( ns ) {
		alert("'. sprintf( esc_js( __('Your message seems to contain %s. For security reasons, it cannot be previewed from the admin dashboard.', 'nfwplus') ), '"+ ns +"'). '");
		return false;
	}
	document.getElementById(\'out_msg\').innerHTML = t3;
	jQuery("#td_msg").slideDown();
	document.getElementById(\'btn_msg\').value = \'' . esc_js( __('Refresh preview', 'nfwplus') ) . '\';
}
function default_msg() {
	document.option_form.elements[\'nfw_options[blocked_msg]\'].value = "' . preg_replace( '/[\r\n]/', '\n', NFW_DEFAULT_MSG) .'";
}
function check_charset(){';
if ( $charset == 'gbk' ) {
	echo 'if (confirm("' .
	 esc_js( __('Warning: your database is using GB2312 character set for simplified Chinese characters (GBK charset). NinjaFirewall may not be able to properly sanitise multi-byte characters because it will not have access to the database if this option is enabled. We recommend to turned it off.', 'nfwplus') ) .'\n'. esc_js( __('Are you sure you want to enable it?', 'nfwplus') ) . '") ) {
			return true;
		} else {
			return false;
		}
	}';
} else {
	echo 'return true;}';
}

echo '
</script>

<div class="wrap">
	<div style="width:33px;height:33px;background-image:url( ' . plugins_url() . '/nfwplus/images/ninjafirewall_32.png);background-repeat:no-repeat;background-position:0 0;margin:7px 5px 0 0;float:left;"></div>
	<h1>' . __('Firewall Options', 'nfwplus') . '</h1>';

// Saved options ?
if ( isset( $_POST['nfw_options']) ) {
	if ( empty($_POST['nfwnonce']) || ! wp_verify_nonce($_POST['nfwnonce'], 'options_save') ) {
		wp_nonce_ays('options_save');
	}
	$res = nf_sub_options_save();
	$nfw_options = nfw_get_option( 'nfw_options' );
	if ($res) {
		echo '<div class="error notice is-dismissible"><p>' . $res . '.</p></div>';
	} else {
		echo '<div class="updated notice is-dismissible"><p>' . __('Your changes have been saved.', 'nfwplus') . '</p></div>';
	}
}

?><br />
	<form method="post" name="option_form" enctype="multipart/form-data">
	<?php wp_nonce_field('options_save', 'nfwnonce', 0); ?>
	<table class="form-table">
		<tr>
			<th scope="row"><?php _e('Firewall protection', 'nfwplus') ?></th>
<?php
// Enabled :
if (! empty( $nfw_options['enabled']) ) {
	echo '
			<td width="20" align="left">&nbsp;</td>
			<td align="left">
				<select name="nfw_options[enabled]" style="width:200px">
					<option value="1" selected>' . __('Enabled', 'nfwplus') . '</option>
					<option value="0">' . __('Disabled', 'nfwplus') . '</option>
				</select>';
// Disabled :
} else {
	echo '
			<td width="20" align="left"><img src="' . plugins_url() . '/nfwplus/images/icon_error_16.png" border="0" height="16" width="16"></td>
			<td align="left">
				<select name="nfw_options[enabled]" style="width:200px">
					<option value="1">' . __('Enabled', 'nfwplus') . '</option>
					<option value="0" selected>' . __('Disabled', 'nfwplus') . '</option>
				</select>&nbsp;<span class="description">&nbsp;' . __('Warning: your site is not protected!', 'nfwplus') . '</span>';
}
echo '
			</td>
		</tr>
		<tr>
		';

// Shared memory ?
if ( empty( $nfw_options['shmop']) ) {
	$nfw_options['shmop'] = 0;
} else {
	$nfw_options['shmop'] = 1;
}

if ( nf_sub_options_test_shmop() ) {
	$res = 1;
	$msg = '';
} else {
	$nfw_options['shmop'] = 0;
	// Update options :
	nfw_update_option( 'nfw_options', $nfw_options);
	$res = 0;
	$msg = '<p><span class="description">' . __('Your server configuration does not seem to be compatible with that option.', 'nfwplus') . '</span></p>';
}

	?><th scope="row"><?php _e('Use shared memory', 'nfwplus') ?></th>
			<td width="20">&nbsp;</td>
			<td align="left">
			<label><input type="radio" name="nfw_options[shmop]" <?php disabled( $res, 0)?> value="1"<?php checked($nfw_options['shmop'], 1)?> onClick="return check_charset();"/><?php _e('Yes', 'nfwplus') ?></label>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<label><input type="radio" name="nfw_options[shmop]" value="0"<?php checked($nfw_options['shmop'], 0)?>/><?php _e('No (default)', 'nfwplus') ?></label><?php echo $msg ?>
			</td>
		</tr>
		<tr>
			<th scope="row"><?php _e('Debugging mode', 'nfwplus') ?></th>
	<?php


// Debugging enabled ?
if (! empty( $nfw_options['debug']) ) {
echo '<td width="20" align="center"><img src="' . plugins_url() . '/nfwplus/images/icon_error_16.png" border="0" height="16" width="16"></td>
			<td align="left">
				<select name="nfw_options[debug]" style="width:200px">
				<option value="1" selected>' . __('Enabled', 'nfwplus') . '</option>
					<option value="0">' . __('Disabled (default)', 'nfwplus') . '</option>
				</select>&nbsp;<span class="description">&nbsp;' . __('Warning: your site is not protected!', 'nfwplus') . '</span>
			</td>';

} else {
// Debugging disabled ?
echo '<td width="20">&nbsp;</td>
			<td align="left">
				<select name="nfw_options[debug]" style="width:200px">
				<option value="1">' . __('Enabled', 'nfwplus') . '</option>
					<option value="0" selected>' . __('Disabled (default)', 'nfwplus') . '</option>
				</select>
			</td>';
}

// Get (if any) the HTTP error code to return :
if (! @preg_match( '/^(?:40[0346]|50[03])$/', $nfw_options['ret_code']) ) {
	$nfw_options['ret_code'] = '403';
}
?>
		</tr>
		<tr>
			<th scope="row"><?php _e('HTTP error code to return', 'nfwplus') ?></th>
			<td width="20">&nbsp;</td>
			<td align="left">
			<select name="nfw_options[ret_code]" style="width:200px">
			<option value="400"<?php selected($nfw_options['ret_code'], 400) ?>><?php _e('400 Bad Request', 'nfwplus') ?></option>
			<option value="403"<?php selected($nfw_options['ret_code'], 403) ?>><?php _e('403 Forbidden (default)', 'nfwplus') ?></option>
			<option value="404"<?php selected($nfw_options['ret_code'], 404) ?>><?php _e('404 Not Found', 'nfwplus') ?></option>
			<option value="406"<?php selected($nfw_options['ret_code'], 406) ?>><?php _e('406 Not Acceptable', 'nfwplus') ?></option>
			<option value="500"<?php selected($nfw_options['ret_code'], 500) ?>><?php _e('500 Internal Server Error', 'nfwplus') ?></option>
			<option value="503"<?php selected($nfw_options['ret_code'], 503) ?>><?php _e('503 Service Unavailable', 'nfwplus') ?></option>
			</select>
			</td>
		</tr>
<?php
echo '
		<tr>
			<th scope="row">' . __('Blocked user message', 'nfwplus') . '</th>
			<td width="20">&nbsp;</td>
			<td align="left">
				<textarea name="nfw_options[blocked_msg]" class="small-text code" cols="60" rows="5" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">';

if (! empty( $nfw_options['blocked_msg']) ) {
	echo htmlentities(base64_decode($nfw_options['blocked_msg']));
} else {
	echo NFW_DEFAULT_MSG;
}
?></textarea>
				<p><input class="button-secondary" type="button" id="btn_msg" value="<?php _e('Preview message', 'nfwplus') ?>" onclick="javascript:preview_msg();" />&nbsp;&nbsp;<input class="button-secondary" type="button" id="btn_msg" value="<?php _e('Default message', 'nfwplus') ?>" onclick="javascript:default_msg();" /></p>
			</td>
		</tr>
	</table>

	<div id="td_msg" style="display:none">
		<table class="form-table" border="1">
			<tr><td id="out_msg" style="border:1px solid #DFDFDF;background-color:#ffffff;" width="100%"></td></tr>
		</table>
	</div>

	<table class="form-table">
		<tr>
			<th scope="row"><?php _e('Export configuration', 'nfwplus') ?></th>
			<td width="20">&nbsp;</td>
			<td align="left"><input class="button-secondary" type="submit" name="nf_export" value="<?php _e('Download', 'nfwplus') ?>" /><br /><span class="description"><?php _e( 'File Check configuration will not be exported/imported.', 'nfwplus') ?></span></td>
		</tr>
		<tr>
			<th scope="row"><?php _e('Import configuration', 'nfwplus') ?></th>
			<td width="20">&nbsp;</td>
			<td align="left"><input type="file" name="nf_imp" /><br /><span class="description"><?php
				printf( __( 'Imported configuration must match plugin version %s.', 'nfwplus'), NFW_ENGINE_VERSION);
				echo '<br />'. __('It will override all your current firewall options and rules.', 'nfwplus')
			?></span></td>
		</tr>
	</table>

	<br />
	<input class="button-primary" type="submit" name="Save" value="<?php _e('Save Firewall Options', 'nfwplus') ?>" />
	</form>
</div>
<?php

return;

/* ================================================================== */

function nf_sub_options_save() {

	// Save options :

	global $nfw_options;

	// Check if we are uploading/importing the configuration :
	if (! empty($_FILES['nf_imp']['size']) ) {
		// Keep the license and its expiration date :
		return nf_sub_options_import( $nfw_options['lic'],$nfw_options['lic_exp'] );
	}

	if ( empty( $_POST['nfw_options']['enabled']) ) {
		if (! empty($nfw_options['enabled']) ) {
			// Alert the admin :
			nf_sub_options_alert(1);
		}
		$nfw_options['enabled'] = 0;

		// If shmop is enabled, we close and delete it :
		if ( $nfw_options['shmop'] ) {
			nfw_shm_delete(0);
		}
		// Disable cron jobs:
		if ( wp_next_scheduled('nfscanevent') ) {
			wp_clear_scheduled_hook('nfscanevent');
		}
		if ( wp_next_scheduled('nfsecupdates') ) {
			wp_clear_scheduled_hook('nfsecupdates');
		}
		if ( wp_next_scheduled('nfdailyreport') ) {
			wp_clear_scheduled_hook('nfdailyreport');
		}
		// Disable brute-force protection :
		if ( file_exists( NFW_LOG_DIR . '/nfwlog/cache/bf_conf.php' ) ) {
			rename(NFW_LOG_DIR . '/nfwlog/cache/bf_conf.php', NFW_LOG_DIR . '/nfwlog/cache/bf_conf_off.php');
		}

	} else {
		$nfw_options['enabled'] = 1;

		// Re-enable cron jobs, if needed :
		if (! empty($nfw_options['sched_scan']) ) {
			if ($nfw_options['sched_scan'] == 1) {
				$schedtype = 'hourly';
			} elseif ($nfw_options['sched_scan'] == 2) {
				$schedtype = 'twicedaily';
			} else {
				$schedtype = 'daily';
			}
			if ( wp_next_scheduled('nfscanevent') ) {
				wp_clear_scheduled_hook('nfscanevent');
			}
			wp_schedule_event( time() + 3600, $schedtype, 'nfscanevent');
		}
		if (! empty($nfw_options['enable_updates']) ) {
			if ($nfw_options['sched_updates'] == 1) {
				$schedtype = 'hourly';
			} elseif ($nfw_options['sched_updates'] == 2) {
				$schedtype = 'twicedaily';
			} else {
				$schedtype = 'daily';
			}
			if ( wp_next_scheduled('nfsecupdates') ) {
				wp_clear_scheduled_hook('nfsecupdates');
			}
			wp_schedule_event( time() + 15, $schedtype, 'nfsecupdates');
		}
		// Re-enable daily report, if needed :
		if (! empty($nfw_options['a_52']) ) {
			if ( wp_next_scheduled('nfdailyreport') ) {
				wp_clear_scheduled_hook('nfdailyreport');
			}
			nfw_get_blogtimezone();
			wp_schedule_event( strtotime( date('Y-m-d 00:00:05', strtotime("+1 day")) ), 'daily', 'nfdailyreport');
		}
		// Reenable brute-force protection :
		if ( file_exists( NFW_LOG_DIR . '/nfwlog/cache/bf_conf_off.php' ) ) {
			rename(NFW_LOG_DIR . '/nfwlog/cache/bf_conf_off.php', NFW_LOG_DIR . '/nfwlog/cache/bf_conf.php');
		}
	}

	if ( (isset( $_POST['nfw_options']['ret_code'])) &&
		(preg_match( '/^(?:40[0346]|50[03])$/', $_POST['nfw_options']['ret_code'])) ) {
		$nfw_options['ret_code'] = $_POST['nfw_options']['ret_code'];
	} else {
		$nfw_options['ret_code'] = '403';
	}

	if ( empty( $_POST['nfw_options']['blocked_msg']) ) {
		$nfw_options['blocked_msg'] = base64_encode(NFW_DEFAULT_MSG);
	} else {
		$nfw_options['blocked_msg'] = base64_encode(stripslashes($_POST['nfw_options']['blocked_msg']));
	}

	if ( empty( $_POST['nfw_options']['debug']) ) {
		$nfw_options['debug'] = 0;
	} else {
		if ( empty($nfw_options['debug']) ) {
			// Alert the admin :
			nf_sub_options_alert(2);
		}
		$nfw_options['debug'] = 1;
	}

	if ( empty( $_POST['nfw_options']['shmop']) ) {
		$nfw_options['shmop'] = 0;
		// Flush our memory block :
		nfw_shm_delete(0);
	} else {
		$nfw_options['shmop'] = 1;
	}

	// Save them :
	nfw_update_option( 'nfw_options', $nfw_options);

	// Make sure the garbage collector cron job is scheduled:
	if (! wp_next_scheduled( 'nfwgccron' ) ) {
		wp_schedule_event( time() + 60, 'hourly', 'nfwgccron' );
	}

}
/* ================================================================== */

function nf_sub_options_import($lic, $lic_exp) {

	// Import NF configuration from file :

	$data = file_get_contents($_FILES['nf_imp']['tmp_name']);
	$err_msg = __('Uploaded file is either corrupted or its format is not supported (#%s)', 'nfwplus');
	if (! $data) {
		return sprintf($err_msg, 1);
	}
	@list ($options, $rules, $bf) = @explode("\n:-:\n", $data . "\n:-:\n");

	// Detect and remove potential Unicode BOM:
	if ( preg_match( '/^\xef\xbb\xbf/', $options ) ) {
		$options = preg_replace( '/^\xef\xbb\xbf/', '', $options );
	}

	if (! $options || ! $rules) {
		return sprintf($err_msg, 2);
	}
	$nfw_options = @unserialize($options);
	$nfw_rules = @unserialize($rules);
	if (! empty($bf) ) {
		$bf_conf = unserialize($bf);
	}

	if ( empty($nfw_options['engine_version']) ) {
		return sprintf($err_msg, 3);
	}
	if ( $nfw_options['engine_version'] != NFW_ENGINE_VERSION  ) {
		return __('The imported file is not compatible with that version of NinjaFirewall', 'nfwplus');
	}

	if ( empty($nfw_rules[1]) ) {
		return sprintf($err_msg, 5);
	}

	// Check whether it is from the WP+ or WP edn:
	if (! isset($nfw_options['shmop']) ) {
		// We need to adjust the WP options/rules to make
		// them compatible with the WP+ edn:

		// Get the current WP+ options:
		$wpplus_options = nfw_get_option( 'nfw_options' );

		foreach ( $wpplus_options as $key => $value ) {
			if (! isset( $nfw_options[$key]) ) {
				// Add the missing key/value to complement the imported
				// WP options (e.g. Access Control etc):
				$nfw_options[$key] = $value;
			}
		}
		// The WP+ Edition does not need rule #531:
		if ( isset( $nfw_rules[531]) ) {
			unset( $nfw_rules[531] );
		}
		// We don't need that key/value pair value either (WP+ uses the Access Control one):
		if ( isset( $nfw_options['wl_admin']) ) {
			unset( $nfw_options['wl_admin'] );
		}
	}

	// We need to delete any existing shared memory segment...
	nfw_shm_delete(0);
	// ...and disable the option if shmop_open() does not exist on that server :
	if (! function_exists('shmop_open')) {
		$nfw_options['shmop'] = 0;
	}

	// Fix paths and directories :
	$nfw_options['logo'] = plugins_url() . '/nfwplus/images/ninjafirewall_75.png';
	$nfw_options['wp_dir'] = '/wp-admin/(?:css|images|includes|js)/|' .
									 '/wp-includes/(?:(?:css|images|js(?!/tinymce/wp-tinymce\.php)|theme-compat)/|[^/]+\.php)|' .
									 '/'. basename(WP_CONTENT_DIR) .'/uploads/|/cache/';
	// $nfw_options['alert_email'] = get_option('admin_email');

	// Anti-Malware: if the path doest not exist on this server,
	// set it to ABSPATH:
	if (! is_dir( $nfw_options['malware_dir'] ) ) {
		$nfw_options['malware_dir'] = rtrim( ABSPATH, '/\\ ' );
	}

	// We don't import the File Check 'snapshot directory' path:
	$nfw_options['snapdir'] = '';
	// We delete any File Check cron jobs :
	if ( wp_next_scheduled('nfscanevent') ) {
		wp_clear_scheduled_hook('nfscanevent');
	}

	// Re-enable auto updates, if needed :
	if ( wp_next_scheduled('nfsecupdates') ) {
		// Clear old one :
		wp_clear_scheduled_hook('nfsecupdates');
	}
	if (! empty($nfw_options['enable_updates']) ) {
		if ($nfw_options['sched_updates'] == 1) {
			$schedtype = 'hourly';
		} elseif ($nfw_options['sched_updates'] == 2) {
			$schedtype = 'twicedaily';
		} else {
			$schedtype = 'daily';
		}
		wp_schedule_event( time() + 15, $schedtype, 'nfsecupdates');
	}
	// Re-enable daily report, if needed :
	if ( wp_next_scheduled('nfdailyreport') ) {
		// Clear old one :
		wp_clear_scheduled_hook('nfdailyreport');
	}
	if (! empty($nfw_options['a_52']) ) {
		nfw_get_blogtimezone();
		wp_schedule_event( strtotime( date('Y-m-d 00:00:05', strtotime("+1 day")) ), 'daily', 'nfdailyreport');
	}

	// Re-enable the garbage collector, if needed:
	if ( wp_next_scheduled('nfwgccron') ) {
		// Clear old one:
		wp_clear_scheduled_hook('nfwgccron');
	}
	wp_schedule_event( time() + 60, 'hourly', 'nfwgccron' );

	// Check compatibility before importing HSTS headers configration
	// or unset the option :
	if (! function_exists('header_register_callback') || ! function_exists('headers_list') || ! function_exists('header_remove') ) {
		if ( isset($nfw_options['response_headers']) ) {
			unset($nfw_options['response_headers']);
		}
	}

	// Generate new salt values for the anti-spam :
	$nfw_options['as_salt']	= wp_generate_password();
	$nfw_options['as_field'] = wp_generate_password( mt_rand(5, 12), FALSE);
	$nfw_options['as_field_2'] = wp_generate_password( mt_rand(5, 12), FALSE);

	// If brute force protection is enabled, we need to create a new config file :
	$nfwbfd_log = NFW_LOG_DIR . '/nfwlog/cache/bf_conf.php';
	if (! empty($bf_conf) ) {
		$fh = fopen($nfwbfd_log, 'w');
		fwrite($fh, $bf_conf);
		fclose($fh);
	} else {
	// ...or delete the current one, if any :
		if ( file_exists($nfwbfd_log) ) {
			unlink($nfwbfd_log);
		}
	}

	// We need to keep the actual license and its expiration date :
	$nfw_options['lic'] = $lic;
	$nfw_options['lic_exp'] = $lic_exp;

	// Save options :
	nfw_update_option( 'nfw_options', $nfw_options);

	// Add the correct DOCUMENT_ROOT :
	if ( strlen( $_SERVER['DOCUMENT_ROOT'] ) > 5 ) {
		$nfw_rules[NFW_DOC_ROOT]['cha'][1]['wha'] =  str_replace( '/', '/[./]*', $_SERVER['DOCUMENT_ROOT'] );
	} elseif ( strlen( getenv( 'DOCUMENT_ROOT' ) ) > 5 ) {
		$nfw_rules[NFW_DOC_ROOT]['cha'][1]['wha'] = str_replace( '/', '/[./]*', getenv( 'DOCUMENT_ROOT' ) );
	} else {
		$nfw_rules[NFW_DOC_ROOT]['ena']  = 0;
	}
	// Save rules :
	nfw_update_option( 'nfw_rules', $nfw_rules);

	// Alert the admin :
	nf_sub_options_alert(3);

	return;
}

/* ================================================================== */

function nf_sub_options_test_shmop() {

	if (! function_exists('shmop_open')) {
		// Error :
		return false;
	}
	// Attempt to allocate a 1-kbyte block :
	if ( $shm_id = @shmop_open( ftok( __FILE__, 'N' ), "c", 0600, 1024) ) {
		shmop_delete($shm_id);
		shmop_close($shm_id);
		// Success :
		return true;
	} else {
		// Error :
		return false;
	}
}

/* ================================================================== */

function nf_sub_options_alert( $what ) {

	$nfw_options = nfw_get_option( 'nfw_options' );

	if ( ( is_multisite() ) && ( $nfw_options['alert_sa_only'] == 2 ) ) {
		$recipient = get_option('admin_email');
	} else {
		$recipient = $nfw_options['alert_email'];
	}

	global $current_user;
	$current_user = wp_get_current_user();

	$subject = __('[NinjaFirewall] Alert: Firewall is disabled', 'nfwplus');
	if ( is_multisite() ) {
		$url = __('-Blog :', 'nfwplus') .' '. network_home_url('/') . "\n\n";
	} else {
		$url = __('-Blog :', 'nfwplus') .' '. home_url('/') . "\n\n";
	}
	// Disabled ?
	if ($what == 1) {
		$message = __('Someone disabled NinjaFirewall from your WordPress admin dashboard:', 'nfwplus') . "\n\n";
	// Debugging mode :
	} elseif ($what == 2) {
		$message = __('NinjaFirewall is disabled because someone enabled debugging mode from your WordPress admin dashboard:', 'nfwplus') . "\n\n";
	// Imported configuration ?
	} elseif ($what == 3) {
		$subject = __('[NinjaFirewall] Alert: Firewall override settings', 'nfwplus');
		$message = __('Someone imported a new configuration which overrode the firewall settings:', 'nfwplus') . "\n\n";
	} else {
		// Should never reach this line!
		return;
	}

	$message .= __('-User :', 'nfwplus') .' '. $current_user->user_login . ' (' . $current_user->roles[0] . ")\n" .
		__('-IP   :', 'nfwplus') .' '. NFW_REMOTE_ADDR . "\n" .
		__('-Date :', 'nfwplus') .' '. ucfirst( date_i18n('F j, Y @ H:i:s O') ) ."\n" .
		$url .
		'NinjaFirewall (WP+ Edition) - http://ninjafirewall.com/' . "\n" .
		__('Help Desk:', 'nfwplus') .' https://nintechnet.com/helpdesk/' . "\n";
	wp_mail( $recipient, $subject, $message );

}

/* ================================================================== */
// EOF
