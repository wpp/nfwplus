<?php
/*
 +=====================================================================+
 | NinjaFirewall (WP+ Edition)                                         |
 |                                                                     |
 | (c) NinTechNet - http://nintechnet.com/                             |
 +=====================================================================+ i18n+ / sa
*/

if (! defined( 'NFW_ENGINE_VERSION' ) ) { die( 'Forbidden' ); }

// Block immediately if user is not allowed :
nf_not_allowed( 'block', __LINE__ );

$nfw_options = nfw_get_option( 'nfw_options' );
$nfw_rules = nfw_get_option( 'nfw_rules' );

// Is NF enabled/working ?
if (! defined('NF_DISABLED') ) {
	is_nfw_enabled();
}

?>

<div class="wrap">
	<div style="width:33px;height:33px;background-image:url(<?php echo plugins_url() ?>/nfwplus/images/ninjafirewall_32.png);background-repeat:no-repeat;background-position:0 0;margin:7px 5px 0 0;float:left;"></div>
	<h1>NinjaFirewall (<font color=#21759B>WP+</font> Edition)</h1>
	<?php
	// first run ?
	if ( @NFW_STATUS == 21 && ! empty( $_REQUEST['nfw_firstrun']) ) {
		echo '<br><div class="updated notice is-dismissible"><p>' .
			__('Congratulations, NinjaFirewall is up and running!', 'nfwplus') .	'<br />' .
			__('If you need help, click on the contextual "Help" menu tab located in the upper right corner of each page.', 'nfwplus');
		if (! empty($_SESSION['email_install']) ) {
			echo '<p>' . __('A "Quick Start, FAQ & Troubleshooting Guide" email was sent to', 'nfwplus') .' <code>' .htmlspecialchars( $_SESSION['email_install'] ) .'</code>.</p>';
			unset($_SESSION['email_install']);
		}
		echo '</p></div>';
		unset($_SESSION['abspath']); 	unset($_SESSION['http_server']);
		unset($_SESSION['php_ini_type']); unset($_SESSION['abspath_writable']);
		unset($_SESSION['ini_write']); unset($_SESSION['htaccess_write']);
		unset( $_SESSION['waf_mode'] ); unset($_SESSION['nfw_lic']);
	}

	// License expiration warning :
	if ( empty($nfw_options['lic']) ) {
		echo '<div class="error notice is-dismissible"><p>' .
			__('You do not have a valid NinjaFirewall license', 'nfwplus') . ' (#1)! <a href="admin.php?page=nfsublicense">' .
			__('Click here to get one', 'nfwplus') . '</a>.</p></div>';
	} else {
		if (! empty($nfw_options['lic_exp']) && preg_match('/^\d{4}-\d{2}-\d{2}$/', $nfw_options['lic_exp']) ) {
			if ( $nfw_options['lic_exp'] < date('Y-m-d') ){
				echo '<div class="error notice is-dismissible"><p>' .
			__('Your NinaFirewall license has expired!', 'nfwplus') . ' <a href="admin.php?page=nfsublicense">' .
			__('Click here to renew it', 'nfwplus') . '</a>.</p></div>';
			} elseif ( $nfw_options['lic_exp'] < date('Y-m-d', strtotime("+30 day")) ){
				echo '<div class="error notice is-dismissible"><p>' .
			__('Your NinaFirewall license will expire soon!', 'nfwplus') . ' <a href="admin.php?page=nfsublicense">' .
			__('Click here to renew it', 'nfwplus') . '</a>.</p></div>';
			}
		} else {
			echo '<div class="error notice is-dismissible"><p>' .
			__('You do not have a valid NinjaFirewall license', 'nfwplus') . ' (#2)! <a href="admin.php?page=nfsublicense">' .
			__('Click here to get one', 'nfwplus') . '</a>.</p></div>';
		}
	}

	?>
	<br />
	<table class="form-table">

	<?php
	if (NF_DISABLED) {
		if (! empty($GLOBALS['err_fw'][NF_DISABLED]) ) {
			$msg = $GLOBALS['err_fw'][NF_DISABLED];
		} else {
			$msg = __('unknown error', 'nfwplus') . ' #' . NF_DISABLED;
		}
	?>
		<tr>
			<th scope="row"><?php _e('Firewall', 'nfwplus') ?></th>
			<td width="20" align="left"><img src="<?php echo plugins_url() . '/nfwplus/images/icon_error_16.png' ?>" border="0" height="16" width="16"></td>
			<td><?php echo $msg ?></td>
		</tr>

	<?php
	} else {
	?>

		<tr>
			<th scope="row"><?php _e('Firewall', 'nfwplus') ?></th>
			<td width="20" align="left">&nbsp;</td>
			<td><?php _e('Enabled', 'nfwplus') ?></td>
		</tr>

	<?php
	}

	if ( defined('NFW_WPWAF') ) {
		$mode = __('WordPress WAF', 'nfwplus');
	} else {
		$mode = __('Full WAF', 'nfwplus');
	}
	?>
		<tr>
			<th scope="row"><?php _e('Mode', 'nfwplus') ?></th>
			<td width="20" align="left">&nbsp;</td>
			<td><?php printf( __('NinjaFirewall is running in %s mode.', 'nfwplus'), '<a href="https://blog.nintechnet.com/full_waf-vs-wordpress_waf/">'. $mode .'</a>'); ?></td>
		</tr>
	<?php

	if (! empty( $nfw_options['debug']) ) {
	?>
		<tr>
			<th scope="row"><?php _e('Debugging mode', 'nfwplus') ?></th>
			<td width="20" align="left"><img src="<?php echo plugins_url() ?>/nfwplus/images/icon_error_16.png" border="0" height="16" width="16"></td>
			<td><?php _e('Enabled.', 'nfwplus') ?>&nbsp;<a href="?page=nfsubopt"><?php _e('Click here to turn Debugging Mode off', 'nfwplus') ?></a></td>
		</tr>
	<?php
	}
	?>
		<tr>
			<th scope="row"><?php _e('PHP SAPI', 'nfwplus') ?></th>
			<td width="20" align="left">&nbsp;</td>
			<td>
				<?php
				if ( defined('HHVM_VERSION') ) {
					echo 'HHVM';
				} else {
					echo strtoupper(PHP_SAPI);
				}
				echo ' ~ '. PHP_MAJOR_VERSION .'.'. PHP_MINOR_VERSION .'.'. PHP_RELEASE_VERSION;
				?>
			</td>
		</tr>
		<tr>
			<th scope="row"><?php _e('Version', 'nfwplus') ?></th>
			<td width="20" align="left">&nbsp;</td>
			<td><?php echo NFW_ENGINE_VERSION . ' ~ ' . __('Security rules:', 'nfwplus' ) . ' ' . preg_replace('/(\d{4})(\d\d)(\d\d)/', '$1-$2-$3', $nfw_options['rules_version']) ?></td>
		</tr>
	<?php

	// If security rules updates are disabled, warn the user:
	if ( empty( $nfw_options['enable_updates'] ) ) {
		?>
		<tr>
			<th scope="row"><?php _e('Updates', 'nfwplus') ?></th>
			<td width="20" align="left"><img src="<?php echo plugins_url() ?>/nfwplus/images/icon_warn_16.png" border="0" height="16" width="16"></td>
			<td><a href="?page=nfsubupdates"><?php _e( 'Security rules updates are disabled.', 'nfwplus' ) ?></a> <?php _e( 'If you want your blog to be protected against the latest threats, enable automatic security rules updates.', 'nfwplus' ) ?></td>
		</tr>
		<?php
	}

	// Are we using shared memory ?
	$icn = $msg = '';
	if ( empty($nfw_options['shmop']) ) {
		$icn = '&nbsp;';
		$msg = '<a href="?page=nfsubopt">' . __('Disabled', 'nfwplus') . '</a>';
	} else {
		// Try to access it :
		$nf_shm_key = ftok( dirname( dirname( __DIR__ ) ), 'N' );

		if ( $shm_id = @shmop_open($nf_shm_key, "a", 0, 0) ) {
			// So far, so good. Check if it is up to date:
			$nfw_data = serialize($nfw_options) . $nf_shm_key . serialize($nfw_rules);
			$shmop_size = shmop_size($shm_id);
			if ( md5( shmop_read($shm_id, 0, $shmop_size)) != md5( $nfw_data ) ) {
				$icn = '<img src="' . plugins_url() . '/nfwplus/images/icon_error_16.png" border="0" height="16" width="16">';
				$msg = sprintf(__('The shared memory block seems corrupted. Try to reload this page to fix it or, if this error persists, please <a href="%s">disable shared memory</a> to avoid any problem.', 'nfwplus'), '?page=nfsubopt');
			} else {
				$icn = '&nbsp;';
				$msg = __('Enabled', 'nfwplus') .' '. sprintf( __( '(RAM usage: %s bytes)', 'nfwplus'), number_format( $shmop_size ) );
			}
		} else {
			if ( $nfw_options['enabled'] ) {
				$icn = '<img src="' . plugins_url() . '/nfwplus/images/icon_error_16.png" border="0" height="16" width="16">';
				$msg = sprintf(__('Unable to access/read the shared memory block. Try to reload this page or, if this error persists, please <a href="%s">disable shared memory</a> to avoid any problem.', 'nfwplus'), '?page=nfsubopt');
			} else {
				$icn = '&nbsp;';
				$msg = __('Firewall is disabled', 'nfwplus');
			}
		}
	}
	?>
	<tr>
		<th scope="row"><?php _e('Shared memory', 'nfwplus') ?></th>
		<td width="20" align="left"><?php echo $icn ?></td>
		<td><?php echo $msg ?></td>
	</tr>
	<?php
	// Check if the admin is whitelisted, and warn if it is not :
	if ( empty($_SESSION['nfw_goodguy']) ) {
		?>
		<tr>
			<th scope="row"><?php _e('Admin user', 'nfwplus') ?></th>
			<td width="20" align="left"><img src="<?php echo plugins_url() . '/nfwplus/images/icon_warn_16.png' ?>" border="0" height="16" width="16"></td>
			<td><?php printf( __('You are not whitelisted. Ensure that the "Do not block the following users" option in the <a href="%s">Access Control menu</a> includes the Admin/Super Admin, otherwise you will likely get blocked by the firewall while working from the WordPress administration dashboard.', 'nfwplus'), '?page=nfsubaccess') ?></td>
		</tr>
	<?php
	}

	// Try to find out if there is any "lost" session between the firewall
	// and the plugin part of NinjaFirewall (could be a buggy plugin killing
	// the session etc), unless we just installed it :
	if (! empty($_SESSION['nfw_st']) && ! NF_DISABLED && empty($_REQUEST['nfw_firstrun']) ) {
		?>
		<tr>
			<th scope="row"><?php _e('User session') ?></th>
			<td width="20" align="left"><img src="<?php echo plugins_url() . '/nfwplus/images/icon_warn_16.png' ?>" border="0" height="16" width="16"></td>
			<td><?php _e('It seems the user session was not set by the firewall script or may have been destroyed by another plugin. You may get blocked by the firewall while working from the WordPress administration dashboard.', 'nfwplus') ?></td>
		</tr>
		<?php
		unset($_SESSION['nfw_st']);
	}
	if ( defined('NFW_SWL') && ! empty($_SESSION['nfw_goodguy']) && empty($_REQUEST['nfw_firstrun']) ) {
		?>
		<tr>
			<th scope="row"><?php _e('User session') ?></th>
			<td width="20" align="left"><img src="<?php echo plugins_url() . '/nfwplus/images/icon_warn_16.png' ?>" border="0" height="16" width="16"></td>
			<td><?php _e('It seems that the user session set by NinjaFirewall was not found by the firewall script. You may get blocked by the firewall while working from the WordPress administration dashboard.', 'nfwplus') ?></td>
		</tr>
		<?php
	}


	// Centralized logging: remote server
	if ( ! empty( $nfw_options['clogs_pubkey'] ) ) {
		$err_msg = $ok_msg = '';
		if (! preg_match( '/^[a-f0-9]{40}:([a-f0-9:.]{3,39}|\*)$/', $nfw_options['clogs_pubkey'], $match ) ) {
			$err_msg = sprintf( __('the public key is invalid. Please <a href="%s">check your configuration</a>.', 'nfwplus'), '?page=nfsublog#clogs');

		} else {
			if ( $match[1] == '*' ) {
				$ok_msg = __( "No IP address restriction.", 'nfwplus');

			} elseif ( filter_var( $match[1], FILTER_VALIDATE_IP ) ) {
				$ok_msg = sprintf( __("IP address %s is allowed to access NinjaFirewall's log on this server.", 'nfwplus'), htmlspecialchars( $match[1]) );

			} else {
				$err_msg = sprintf( __('the whitelisted IP is not valid. Please <a href="%s">check your configuration</a>.', 'nfwplus'), '?page=nfsublog#clogs');
			}
		}
		?>
		<tr>
			<th scope="row"><?php _e('Centralized Logging', 'nfwplus') ?></th>
		<?php
		if ( $err_msg ) {
			?>
				<td width="20" align="left"><img src="<?php echo plugins_url() . '/nfwplus/images/icon_error_16.png' ?>" border="0" height="16" width="16"></td>
				<td><?php printf( __('Error: %s', 'nfwplus'), $err_msg) ?></td>
			</tr>
			<?php
			$err_msg = '';
		} else {
			?>
				<td width="20" align="left">&nbsp;</td>
				<td><a href="?page=nfsublog#clogs"><?php _e('Enabled', 'nfwplus'); echo "</a>. $ok_msg"; ?></td>
			</tr>
		<?php
		}
	}



	// If the Source IP was changed by the user, ensure it exists
	// or warm about it :
	$warn_msg = '';
	if ( @$nfw_options['ac_ip'] == 3  && ! empty($nfw_options['ac_ip_2']) && empty($_SERVER[$nfw_options['ac_ip_2']]) ) {
		$warn_msg = $nfw_options['ac_ip_2'];
	} elseif ( @$nfw_options['ac_ip'] == 2 && empty($_SERVER['HTTP_X_FORWARDED_FOR']) ) {
		$warn_msg = 'HTTP_X_FORWARDED_FOR';
	}
	if ($warn_msg) {
		?>
		<tr valign="top">
			<th scope="row"><?php _e('Source IP', 'nfwplus') ?></th>
			<td width="20" align="left"><img src="<?php echo plugins_url() ?>/nfwplus/images/icon_error_16.png" border="0" height="16" width="16"></td>
			<td><?php printf( __('<a href="%s">Access Control Source IP</a> is setup to use %s, however your server does not support that variable. All IP-based directives will fail.', 'nfwplus'), '?page=nfsubaccess#sourceip', '<code>'. htmlentities($warn_msg) .'</code>') ?></td>
		</tr>
		<?php
	} else {
		if (! empty($_SESSION['nfw_goodguy']) ) {
			$current_user = wp_get_current_user();
		?>
		<tr>
			<th scope="row"><?php _e('Admin user', 'nfwplus') ?></th>
			<td width="20" align="left">&nbsp;</td>
			<td><code><?php echo htmlspecialchars($current_user->user_login) ?></code>: <?php _e('You are whitelisted by the firewall.', 'nfwplus') ?></td>
		</tr>
	<?php
		}
	}

	// Display restrictions (not available to WPMU):
	if ( defined('NFW_ALLOWED_ADMIN') && ! is_multisite() ) {
	?>
		<tr>
			<th scope="row"><?php _e('Restrictions', 'nfwplus') ?></th>
			<td width="20" align="left">&nbsp;</td>
			<td><?php _e('Access to NinjaFirewall is restricted to:', 'nfwplus') ?> <code><?php echo htmlspecialchars(NFW_ALLOWED_ADMIN) ?></code></td>
		</tr>
	<?php
	}

	// Check IP and warn if localhost or private IP :
	if (! filter_var(NFW_REMOTE_ADDR, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) ) {
		?>
		<tr valign="top">
			<th scope="row"><?php _e('Source IP', 'nfwplus') ?></th>
			<td width="20" align="left"><img src="<?php echo plugins_url() ?>/nfwplus/images/icon_warn_16.png" border="0" height="16" width="16"></td>
			<td><?php printf( __('You have a private IP: %s', 'nfwplus') .'<br />'.
				__('If your site is behind a reverse proxy or a load balancer, ensure that the <a href="%s">Source IP</a> is setup accordingly.', 'nfwplus'), htmlentities(NFW_REMOTE_ADDR), '?page=nfsubaccess#sourceip') ?></td>
		</tr>
		<?php
	}

	// Look for CDN's (Incapsula/Cloudflare) and warn the user about using
	// the correct IPs, unless it was already copied to REMOTE_ADDR :
	if (! empty($_SERVER["HTTP_CF_CONNECTING_IP"]) ) {
		// CloudFlare :
		if ( NFW_REMOTE_ADDR != $_SERVER["HTTP_CF_CONNECTING_IP"] ) {
		?>
		<tr>
			<th scope="row"><?php _e('CDN detection', 'nfwplus') ?></th>
			<td width="20" align="left"><img src="<?php echo plugins_url() ?>/nfwplus/images/icon_warn_16.png" border="0" height="16" width="16"></td>
			<td><?php printf( __('%s detected: you seem to be using Cloudflare CDN services. Ensure that the <a href="%s">Source IP</a> is setup accordingly.', 'nfwplus'), '<code>HTTP_CF_CONNECTING_IP</code>', '?page=nfsubaccess#sourceip') ?></td>
		</tr>
		<?php
		}
	}
	if (! empty($_SERVER["HTTP_INCAP_CLIENT_IP"]) ) {
		// Incapsula :
		if ( NFW_REMOTE_ADDR != $_SERVER["HTTP_INCAP_CLIENT_IP"] ) {
		?>
		<tr>
			<th scope="row"><?php _e('CDN detection', 'nfwplus') ?></th>
			<td width="20" align="left"><img src="<?php echo plugins_url() ?>/nfwplus/images/icon_warn_16.png" border="0" height="16" width="16"></td>
			<td><?php printf( __('%s detected: you seem to be using Incapsula CDN services. Ensure that the <a href="%s">Source IP</a> is setup accordingly.', 'nfwplus'), '<code>HTTP_INCAP_CLIENT_IP</code>', '?page=nfsubaccess#sourceip') ?></td>
		</tr>
		<?php
		}
	}

	// Check whether loggin is enable or not :
	if (empty($nfw_options['logging']) ) {
		?>
		<tr>
			<th scope="row"><?php _e('Logging', 'nfwplus') ?></th>
			<td width="20" align="left"><img src="<?php echo plugins_url() ?>/nfwplus/images/icon_warn_16.png" border="0" height="16" width="16"></td>
			<td><?php _e('Logging is disabled.', 'nfwplus') ?> <a href="?page=nfsublog"><?php _e('Click here to re-enable it.', 'nfwplus') ?></a></td>
		</tr>
		<?php
	}

	// Ensure /log/ dir is writable :
	if (! is_writable( NFW_LOG_DIR . '/nfwlog') ) {
		?>
			<tr>
			<th scope="row"><?php _e('Log dir', 'nfwplus') ?></th>
			<td width="20" align="left"><img src="<?php echo plugins_url() ?>/nfwplus/images/icon_error_16.png" border="0" height="16" width="16"></td>
			<td><?php printf( __('%s directory is not writable! Please chmod it to 0777 or equivalent.', 'nfwplus'), '<code>'. htmlspecialchars(NFW_LOG_DIR) .'/nfwlog/</code>') ?></td>
		</tr>
	<?php
	}

	// Ensure /log/cache dir is writable :
	if (! is_writable( NFW_LOG_DIR . '/nfwlog/cache') ) {
		?>
			<tr>
			<th scope="row"><?php _e('Log dir', 'nfwplus') ?></th>
			<td width="20" align="left"><img src="<?php echo plugins_url() ?>/nfwplus/images/icon_error_16.png" border="0" height="16" width="16"></td>
			<td><?php printf( __('%s directory is not writable! Please chmod it to 0777 or equivalent.', 'nfwplus'), '<code>'. htmlspecialchars(NFW_LOG_DIR) .'/nfwlog/cache</code>') ?></td>
		</tr>
	<?php
	}

	// check for NinjaFirewall optional config file :
	$doc_root = rtrim($_SERVER['DOCUMENT_ROOT'], '/');
	if ( @file_exists( $file = dirname($doc_root) . '/.htninja') ||
		@file_exists( $file = $doc_root . '/.htninja') ) {
		echo '<tr>
		<th scope="row">' . __('Optional configuration file', 'nfwplus') . '</th>
		<td width="20">&nbsp;</td>
		<td><code>' . htmlentities($file) . '</code></td>
		</tr>';
	}

	echo '</table>';
	?>
</div>
<?php
/* ================================================================== */
// EOF
