<?php
/*
Plugin Name: NinjaFirewall (WP+)
Plugin URI: https://nintechnet.com/
Description: A true Web Application Firewall to protect and secure WordPress.
Version: 3.5.2
Author: The Ninja Technologies Network
Author URI: https://nintechnet.com/
Network: true
Text Domain: nfwplus
Domain Path: /languages
*/

define( 'NFW_ENGINE_VERSION', '3.5.2' );

/*
 +=====================================================================+
 | NinjaFirewall (WP+ Edition)                                         |
 |                                                                     |
 | (c) NinTechNet - https://nintechnet.com/                            |
 +=====================================================================+
*/

if (! defined( 'ABSPATH' ) ) { die( 'Forbidden' ); }

if (! headers_sent() ) {
	if (version_compare(PHP_VERSION, '5.4', '<') ) {
		if (! session_id() ) {
			session_start();
			$_SESSION['nfw_st'] = 1;
		}
	} else {
		if (session_status() !== PHP_SESSION_ACTIVE) {
			session_start();
			$_SESSION['nfw_st'] = 2;
		}
	}
}

/* ================================================================== */
if (! defined('NFW_NOI18N') ) {
	load_plugin_textdomain('nfwplus', FALSE, dirname(plugin_basename(__FILE__)).'/languages/');
}
$null = __('A true Web Application Firewall to protect and secure WordPress.', 'nfwplus');
/* ================================================================== */

// Some constants & variables first :
define('NFW_NULL_BYTE', 2);
define('NFW_ASCII_CTRL', 500);
define('NFW_DOC_ROOT', 510);
define('NFW_WRAPPERS', 520);
define('NFW_OBJECTS', 525);
define('NFW_LOOPBACK', 540);
define('NFW_BOT_LIST', 'acunetix|AhrefsBot|backdoor|bandit|' .
	'blackwidow|BOT for JCE|core-project|dts agent|emailmagnet|' .
	'exploit|extract|flood|grabber|harvest|httrack|havij|hunter|indy library|' .
	'LoadTimeBot|mfibot|Microsoft URL Control|Miami Style|morfeus|' .
	'nessus|NetLyzer|pmafind|scanner|semrushbot|siphon|spbot|sqlmap|survey|teleport|updown_tester|xovibot'
);
define( 'NFW_DEFAULT_MSG', '<br /><br /><br /><br /><center>' .
		sprintf( __('Sorry %s, your request cannot be processed.', 'nfwplus'), '<b>%%REM_ADDRESS%%</b>') .
		'<br />' . __('For security reasons, it was blocked and logged.', 'nfwplus') .
		'<br /><br />%%NINJA_LOGO%%<br /><br />' .
			__('If you believe this was an error please contact the<br />webmaster and enclose the following incident ID:', 'nfwplus') .
		'<br /><br />[ <b>#%%NUM_INCIDENT%%</b> ]</center>'
);
$err_fw = array(
	1	=> __('Cannot find WordPress configuration file', 'nfwplus'),
	2	=>	__('Cannot read WordPress configuration file', 'nfwplus'),
	3	=>	__('Cannot retrieve WordPress database credentials', 'nfwplus'),
	4	=>	__('Cannot connect to WordPress database', 'nfwplus'),
	5	=>	__('Cannot retrieve user options from database (#2)', 'nfwplus'),
	6	=>	__('Cannot retrieve user options from database (#3)', 'nfwplus'),
	7	=>	__('Cannot retrieve user rules from database (#2)', 'nfwplus'),
	8	=>	__('Cannot retrieve user rules from database (#3)', 'nfwplus'),
	9	=>	__('The firewall has been disabled from the <a href="admin.php?page=nfsubopt">administration console</a>', 'nfwplus'),
	10	=> __('Unable to communicate with the firewall. Please check your settings', 'nfwplus'),
	11	=>	__('Cannot retrieve user options from database (#1)', 'nfwplus'),
	12	=>	__('Cannot retrieve user rules from database (#1)', 'nfwplus'),
	13 => sprintf( __("The firewall cannot access its log and cache folders. If you changed the name of WordPress %s or %s folders, you must define NinjaFirewall's built-in %s constant (see %s for more info)", 'nfwplus'), '<code>/wp-content/</code>', '<code>/plugins/</code>', '<code>NFW_LOG_DIR</code>', "<a href='https://nintechnet.com/ninjafirewall/wp-edition/help/?htninja' target='_blank'>Path to NinjaFirewall's log and cache directory</a>"),
);

if (! defined('NFW_LOG_DIR') ) {
	define('NFW_LOG_DIR', WP_CONTENT_DIR);
}
if (! empty($_SERVER['DOCUMENT_ROOT']) && $_SERVER['DOCUMENT_ROOT'] != '/' ) {
	$_SERVER['DOCUMENT_ROOT'] = rtrim( $_SERVER['DOCUMENT_ROOT'] , '/' );
}
/* ================================================================== */	// i18n+

require( plugin_dir_path(__FILE__) . 'lib/nfw_misc.php' );

if (! defined( 'NFW_REMOTE_ADDR') ) {
	nfw_select_ip();
}

add_action( 'nfwgccron', 'nfw_garbage_collector' );

/* ================================================================== */	// i18n+

function nfw_activate() {

	// Install/activate NinjaFirewall :

	// Block immediately if user is not allowed :
	nf_not_allowed( 'block', __LINE__ );

	// We need at least WP 3.3 :
	global $wp_version;
	if ( version_compare( $wp_version, '3.3', '<' ) ) {
		exit( sprintf( __('NinjaFirewall requires WordPress 3.3 or greater but your current version is %s.', 'nfwplus'), $wp_version) );
	}

	// We need at least PHP 5.3 :
	if ( version_compare( PHP_VERSION, '5.3.0', '<' ) ) {
		exit( sprintf( __('NinjaFirewall requires PHP 5.3 or greater but your current version is %s.', 'nfwplus'), PHP_VERSION) );
	}

	// We need the mysqli extension loaded :
	if (! function_exists('mysqli_connect') ) {
		exit( sprintf( __('NinjaFirewall requires the PHP %s extension.', 'nfwplus'), '<code>mysqli</code>') );
	}

	// Yes, there are still some people who have SAFE_MODE enabled with
	// PHP 5.3 ! We must check that right away otherwise the user may lock
	// himself/herself out of the site as soon as NinjaFirewall will be
	// activated :
	if ( ini_get( 'safe_mode' ) ) {
		exit( __('You have SAFE_MODE enabled. Please disable it, it is deprecated as of PHP 5.3.0 (see http://php.net/safe-mode).', 'nfwplus'));
	}

	// Multisite installation requires superadmin privileges :
	if ( ( is_multisite() ) && (! current_user_can( 'manage_network' ) ) ) {
		exit( __('You are not allowed to activate NinjaFirewall.', 'nfwplus') );
	}

	// We don't do Windows :
	if ( PATH_SEPARATOR == ';' ) {
		exit( __('NinjaFirewall is not compatible with Microsoft Windows.', 'nfwplus') );
	}

	// If already installed/setup, just enable the firewall... :
	if ( $nfw_options = nfw_get_option( 'nfw_options' ) ) {
		$nfw_options['enabled'] = 1;
		nfw_update_option( 'nfw_options', $nfw_options);

		// Re-enable scheduled scan, if needed :
		if (! empty($nfw_options['sched_scan']) ) {
			if ($nfw_options['sched_scan'] == 1) {
				$schedtype = 'hourly';
			} elseif ($nfw_options['sched_scan'] == 2) {
				$schedtype = 'twicedaily';
			} else {
				$schedtype = 'daily';
			}
			if ( wp_next_scheduled('nfscanevent') ) {
				wp_clear_scheduled_hook('nfscanevent');
			}
			wp_schedule_event( time() + 3600, $schedtype, 'nfscanevent');
		}
		// Re-enable auto updates, if needed :
		if (! empty($nfw_options['enable_updates']) ) {
			if ($nfw_options['sched_updates'] == 1) {
				$schedtype = 'hourly';
			} elseif ($nfw_options['sched_updates'] == 2) {
				$schedtype = 'twicedaily';
			} else {
				$schedtype = 'daily';
			}
			if ( wp_next_scheduled('nfsecupdates') ) {
				wp_clear_scheduled_hook('nfsecupdates');
			}
			wp_schedule_event( time() + 15, $schedtype, 'nfsecupdates');
		}
		// Re-enable daily report, if needed :
		if (! empty($nfw_options['a_52']) ) {
			if ( wp_next_scheduled('nfdailyreport') ) {
				wp_clear_scheduled_hook('nfdailyreport');
			}
			nfw_get_blogtimezone();
			wp_schedule_event( strtotime( date('Y-m-d 00:00:05', strtotime("+1 day")) ), 'daily', 'nfdailyreport');
		}
		// Re-enable brute-force protection :
		if ( file_exists( NFW_LOG_DIR . '/nfwlog/cache/bf_conf_off.php' ) ) {
			rename(NFW_LOG_DIR . '/nfwlog/cache/bf_conf_off.php', NFW_LOG_DIR . '/nfwlog/cache/bf_conf.php');
		}

		// ...and whitelist the user if needed :
		if (! empty($nfw_options['ac_role']) && nfw_is_goodguy(0) ) {
			$_SESSION['nfw_goodguy'] = true;
		}
	}
}

register_activation_hook( __FILE__, 'nfw_activate' );

/* ================================================================== */	// i18n+

function nfw_deactivate() {

	// Block immediately if user is not allowed :
	nf_not_allowed( 'block', __LINE__ );

	// Disable the firewall (NinjaFirewall will keep running
	// in the background but will not do anything) :
	$nfw_options = nfw_get_option( 'nfw_options' );
	$nfw_options['enabled'] = 0;

	// If shmop is enabled, we close and delete it :
	if ( $nfw_options['shmop'] ) {
		nfw_shm_delete(0);
	}

	// Clear scheduled scan (if any) and its options :
	if ( wp_next_scheduled('nfscanevent') ) {
		wp_clear_scheduled_hook('nfscanevent');
	}
	// Clear auto updates (if any) :
	if ( wp_next_scheduled('nfsecupdates') ) {
		wp_clear_scheduled_hook('nfsecupdates');
	}
	// Clear daily report (if any) :
	if ( wp_next_scheduled('nfdailyreport') ) {
		wp_clear_scheduled_hook('nfdailyreport');
	}
	// and disable brute-force protection :
	if ( file_exists( NFW_LOG_DIR . '/nfwlog/cache/bf_conf.php' ) ) {
		rename(NFW_LOG_DIR . '/nfwlog/cache/bf_conf.php', NFW_LOG_DIR . '/nfwlog/cache/bf_conf_off.php');
	}

	nfw_update_option( 'nfw_options', $nfw_options);

}

register_deactivation_hook( __FILE__, 'nfw_deactivate' );

/* ================================================================== */ // i18n+

function nfw_upgrade() {

	// Only used when upgrading NinjaFirewall, sending alerts
	// exporting/downloading files and checking shared memory :

	// Return immediately if user is not allowed :
	if ( nf_not_allowed(0, __LINE__) ) { return; }

	// Check/update our shared memory block on shutdown :
	if ( defined( 'NFW_STATUS' ) ) {
		register_shutdown_function('nfw_shm_check');
	}

	$is_update = 0;

	$nfw_options = nfw_get_option( 'nfw_options' );
	$nfw_rules = nfw_get_option( 'nfw_rules' );

	// Export configuration :
	if ( isset($_POST['nf_export']) ) {
		if ( empty($_POST['nfwnonce']) || ! wp_verify_nonce($_POST['nfwnonce'], 'options_save') ) {
			wp_nonce_ays('options_save');
		}
		// Export login protection if it exists too :
		$nfwbfd_log = NFW_LOG_DIR . '/nfwlog/cache/bf_conf.php';
		if ( file_exists($nfwbfd_log) ) {
			$bd_data = serialize( file_get_contents($nfwbfd_log) );
		} else {
			$bd_data = '';
		}
		$data = serialize($nfw_options) . "\n:-:\n" . serialize($nfw_rules) . "\n:-:\n" . $bd_data;
		// Download :
		header('Content-Type: text/plain');
		header('Content-Length: '. strlen( $data ) );
		header('Content-Disposition: attachment; filename="nfwplus.' . NFW_ENGINE_VERSION . '.dat"');
		echo $data;
		exit;
	}

	// Download the firewall log:
	if ( isset($_GET['nfw_export']) && ! empty($_GET['nfw_logname']) ) {
		if ( empty($_GET['nfwnonce']) || ! wp_verify_nonce($_GET['nfwnonce'], 'log_select') ) {
			wp_nonce_ays('log_select');
		}
		$log = trim($_GET['nfw_logname']);
		if (! preg_match( '/^(firewall_\d{4}-\d\d(?:\.\d+)?\.)php$/', $log, $match ) ) {
			wp_nonce_ays('log_select');
		}
		$name = $match[1];
		if (! file_exists(NFW_LOG_DIR . '/nfwlog/' . $log) ) {
			wp_nonce_ays('log_select');
		}
		$data = file(NFW_LOG_DIR . '/nfwlog/' . $log);
		$res = "Date\tIncident\tLevel\tRule\tIP\tRequest\tEvent\tHost\n";
		$levels = array( '', 'medium', 'high', 'critical', 'error', 'upload', 'info', 'DEBUG_ON' );
		$severity = array( 0 => 0, 1 => 0, 2 => 0, 3 => 0, 4 => 0, 5 => 0, 6 => 0, 7 => 0);
		foreach( $data as $line ) {
			if ( preg_match( '/^\[(\d{10})\]\s+\[.+?\]\s+\[(.+?)\]\s+\[(#\d{7})(?:-|\]\s+\[)(\d+)\]\s+\[(\d)\]\s+\[([\d.:a-fA-F, ]+?)\]\s+\[.+?\]\s+\[(.+?)\]\s+\[(.+?)\]\s+\[(.+?)\]\s+\[(hex:)?(.+)\]$/', $line, $match ) ) {
				if ( empty( $match[4]) ) { $match[4] = '-'; }
				if ( $match[10] == 'hex:' ) { $match[11] = pack('H*', $match[11]); }
				$res .= date( 'd/M/y H:i:s', $match[1] ) . "\t" . $match[3] . "\t" .
				$levels[$match[5]] . "\t" . $match[4] . "\t" . $match[6] . "\t" .
				$match[7] . ' ' . $match[8] . "\t" .	$match[9] .
				' - [' . $match[11] . "]\t" . $match[2] . "\n";
			}
		}
		header('Content-Type: text/tab-separated-values');
		header('Content-Length: '. strlen( $res ) );
		header('Content-Disposition: attachment; filename="' . $name . 'tsv"');
		echo $res;
		exit;
	}

	// Download File Check modified files list :
	if ( isset($_POST['dlmods']) ) {
		if ( empty($_POST['nfwnonce']) || ! wp_verify_nonce($_POST['nfwnonce'], 'filecheck_save') ) {
			wp_nonce_ays('filecheck_save');
		}
		if (file_exists(NFW_LOG_DIR . '/nfwlog/cache/nfilecheck_diff.php') ) {
			$download_file = NFW_LOG_DIR . '/nfwlog/cache/nfilecheck_diff.php';
		} elseif (file_exists(NFW_LOG_DIR . '/nfwlog/cache/nfilecheck_diff.php.php') ) {
			$download_file = NFW_LOG_DIR . '/nfwlog/cache/nfilecheck_diff.php.php';
		} else {
			wp_nonce_ays('filecheck_save');
		}
		$stat = stat($download_file);
		$data = '== NinjaFirewall File Check (diff)'. "\n";
		$data.= '== ' . site_url() . "\n";
		$data.= '== ' . date_i18n('M d, Y @ H:i:s O', $stat['ctime']) . "\n\n";
		$data.= '[+] = ' . __('New file', 'nfwplus') .
					'      [-] = ' . __('Deleted file', 'nfwplus') .
					'      [!] = ' . __('Modified file', 'nfwplus') .
					"\n\n";
		$fh = fopen($download_file, 'r');
		while (! feof($fh) ) {
			$res = explode('::', fgets($fh) );
			if ( empty($res[1]) ) { continue; }
			// New file :
			if ($res[1] == 'N') {
				$data .= '[+] ' . $res[0] . "\n";
			// Deleted file :
			} elseif ($res[1] == 'D') {
				$data .= '[-] ' . $res[0] . "\n";
			// Modified file:
			} elseif ($res[1] == 'M') {
				$data .= '[!] ' . $res[0] . "\n";
			}
		}
		fclose($fh);
		$data .= "\n== EOF\n";

		// Download :
		header('Content-Type: text/plain');
		header('Content-Length: '. strlen( $data ) );
		header('Content-Disposition: attachment; filename="'. $_SERVER['SERVER_NAME'] .'_diff.txt"');
		echo $data;
		exit;
	}

	// Download File Check snapshot :
	if ( isset($_POST['dlsnap']) ) {
		if ( empty($_POST['nfwnonce']) || ! wp_verify_nonce($_POST['nfwnonce'], 'filecheck_save') ) {
			wp_nonce_ays('filecheck_save');
		}
		if (file_exists(NFW_LOG_DIR . '/nfwlog/cache/nfilecheck_snapshot.php') ) {
			$stat = stat(NFW_LOG_DIR . '/nfwlog/cache/nfilecheck_snapshot.php');
			$data = '== NinjaFirewall File Check (snapshot)'. "\n";
			$data.= '== ' . site_url() . "\n";
			$data.= '== ' . date_i18n('M d, Y @ H:i:s O', $stat['ctime']) . "\n\n";
			$fh = fopen(NFW_LOG_DIR . '/nfwlog/cache/nfilecheck_snapshot.php', 'r');
			while (! feof($fh) ) {
				$res = explode('::', fgets($fh) );
				if (! empty($res[0][0]) && $res[0][0] == '/') {
					$data .= $res[0] . "\n";
				}
			}
			fclose($fh);
			$data .= "\n== EOF\n";
			// Download :
			header('Content-Type: text/plain');
			header('Content-Length: '. strlen( $data ) );
			header('Content-Disposition: attachment; filename="'. $_SERVER['SERVER_NAME'] .'_snapshot.txt"');
			echo $data;
			exit;
		} else {
			wp_nonce_ays('filecheck_save');
		}
	}

	// update engine version number if needed :
	require( plugin_dir_path(__FILE__) . 'lib/nfw_upgrade.php' );

	if ( $is_update ) {
		$tmp_data = '';
		// up to v1.0.7  -------------------------------------------------
		if ( version_compare( $nfw_options['engine_version'], '1.0.8', '<' ) ) {
			// Check if we need to restore the log which was saved to the DB
			// before starting NinjaFirewall's update :
			if ( isset($nfw_options['nfw_tmp']) ) {
				unset( $nfw_options['nfw_tmp'] );
				// Fetch it, unpack it, and save it to disk...
				$log_file = NFW_LOG_DIR . '/nfwlog/firewall_' . date( 'Y-m' ) . '.php';
				if ( $tmp_data = @gzinflate( base64_decode( nfw_get_option('nfw_tmp') ) ) ) {
					file_put_contents( $log_file, $tmp_data );
				}
				// ... and clear it from the DB :
				nfw_delete_option('nfw_tmp');
			}
			if ( $tmp_data ) {
				// Try to re-create the widget stats file :
				$stat_file = NFW_LOG_DIR . '/nfwlog/stats_' . date( 'Y-m' ) . '.php';
				$nfw_stat = array('0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
				$stats_lines = explode( PHP_EOL, $tmp_data );
				foreach ( $stats_lines as $line ) {
					if (preg_match( '/^\[.+?\]\s+\[.+?\]\s+(?:\[.+?\]\s+){3}\[([0-9])\]/', $line, $match) ) {
						$nfw_stat[$match[1]]++;
					}
				}
				@file_put_contents( $stat_file, $nfw_stat[0] . ':' . $nfw_stat[1] . ':' .
					$nfw_stat[2] . ':' . $nfw_stat[3] . ':' . $nfw_stat[4] . ':' .
					$nfw_stat[5] . ':' . $nfw_stat[6] . ':' . $nfw_stat[7] . ':' .
					$nfw_stat[8] . ':' . $nfw_stat[9] );
			}
		}

		// Update options :
		nfw_update_option( 'nfw_options', $nfw_options);
	}

	// E-mail alert ?
	if ( defined( 'NFW_ALERT' ) ) {
		nfw_check_emailalert();
	}

	// Run the garbage collector if needed:
	nfw_garbage_collector();

	if (! empty($nfw_options['ac_role']) && nfw_is_goodguy(0) ) {
		$_SESSION['nfw_goodguy'] = true;
		if (! empty( $nfw_options['bf_enable'] ) && ! empty( $nfw_options['bf_rand'] ) ) {
			$_SESSION['nfw_bfd'] = $nfw_options['bf_rand'];
		}
		return;
	}
	if ( isset( $_SESSION['nfw_goodguy'] ) ) {
		unset( $_SESSION['nfw_goodguy'] );
	}
}

add_action('admin_init', 'nfw_upgrade' );

/* ================================================================== */ // i18n+

function nfw_login_hook( $user_login, $user ) {

	// Check if the user is an admin and if we must whitelist him/her :

	$nfw_options = nfw_get_option( 'nfw_options' );

	// Don't do anything if NinjaFirewall is disabled :
	if ( empty( $nfw_options['enabled'] ) ) { return; }

	if ( empty( $user->roles[0] ) ) {
		// This can occur in multisite mode, when the Super Admin logs in
		// to the admin console of a child site but is not in the users
		// list of that site :
		$whoami = '';
		$admin_flag = 1;
		$user->roles[0] = '';
	} elseif ( $user->roles[0] == 'administrator' ) {
		$whoami = 'administrator';
		$admin_flag = 2;
	} else {
		$whoami = $user->roles[0];
		$admin_flag = 0;
	}

	// Are we supposed to send an alert ?
	if (! empty($nfw_options['a_0']) ) {
		// User login:
		if ( ( ( $nfw_options['a_0'] == 1) && ( $admin_flag )  ) ||	( $nfw_options['a_0'] == 2 ) ) {
			nfw_send_loginemail( $user_login, $whoami );
			if (! empty($nfw_options['a_41']) ) {
				nfw_log2('Logged in user', $user_login .' ('. $whoami .')', 6, 0);
			}
		}
	}

	if (! empty($nfw_options['ac_role']) && nfw_is_goodguy( $user->roles[0] ) ) {
		// Set the goodguy flag :
		$_SESSION['nfw_goodguy'] = true;
		return;
	}

	if ( isset( $_SESSION['nfw_goodguy'] ) ) {
		unset( $_SESSION['nfw_goodguy'] );
	}
}

add_action( 'wp_login', 'nfw_login_hook', 10, 2 );

/* ================================================================== */ // i18n+

function nfw_logout_hook() {

	// Whoever it was, we clear the goodguy flag :
	if ( isset( $_SESSION['nfw_goodguy'] ) ) {
		unset( $_SESSION['nfw_goodguy'] );
	}
	// And the Live Log flag as well :
	if (isset($_SESSION['nfw_livelog']) ) {
		unset($_SESSION['nfw_livelog']);
	}
	if (isset($_SESSION['nfw_malscan']) ) {
		unset($_SESSION['nfw_malscan']);
	}
}

add_action( 'wp_logout', 'nfw_logout_hook' );

/* ================================================================== */

function ninjafirewall_admin_menu() {

	// Return immediately if user is not allowed :
	if ( nf_not_allowed( 0, __LINE__ ) ) { return; }

	// Display phpinfo for the installer :
	if (! empty($_REQUEST['nfw_act']) && $_REQUEST['nfw_act'] == 99) {
		if ( empty($_GET['nfwnonce']) || ! wp_verify_nonce($_GET['nfwnonce'], 'show_phpinfo') ) {
			wp_nonce_ays('show_phpinfo');
		}
		phpinfo(33);
		exit;
	}

	// Admin menu :

	if (! defined('NF_DISABLED') ) {
		is_nfw_enabled();
	}

	// Run the install process if not installed yet :
	if (NF_DISABLED == 10) {
		add_menu_page( 'NinjaFirewall', 'NinjaFirewall+', 'manage_options',
			'NinjaFirewall', 'nf_menu_install',	plugins_url() . '/nfwplus/images/nf_icon.png'
		);
		add_submenu_page( 'NinjaFirewall', __('Installation', 'nfwplus'), __('Installation', 'nfwplus'), 'manage_options',
			'NinjaFirewall', 'nf_menu_install' );
		return;
	}

	// Main menu :
	add_menu_page( 'NinjaFirewall', 'NinjaFirewall+', 'manage_options',
		'NinjaFirewall', 'nf_sub_main',	plugins_url() . '/nfwplus/images/nf_icon.png'
	);

	// Submenu :
	global $menu_hook;

	// Contextual help :
	require_once( plugin_dir_path(__FILE__) . 'help.php' );

	// Overview menu :
	$menu_hook = add_submenu_page( 'NinjaFirewall', __('NinjaFirewall: Overview', 'nfwplus'), __('Overview', 'nfwplus'), 'manage_options',
		'NinjaFirewall', 'nf_sub_main' );
	add_action( 'load-' . $menu_hook, 'help_nfsubmain' );

	// Stats menu :
	$menu_hook = add_submenu_page( 'NinjaFirewall', __('NinjaFirewall: Statistics', 'nfwplus'), __('Statistics', 'nfwplus'), 'manage_options',
		'nfsubstat', 'nf_sub_statistics' );
	add_action( 'load-' . $menu_hook, 'help_nfsubstat' );

	// Firewall options menu :
	$menu_hook = add_submenu_page( 'NinjaFirewall', __('NinjaFirewall: Firewall Options', 'nfwplus'), __('Firewall Options', 'nfwplus'), 'manage_options',
		'nfsubopt', 'nf_sub_options' );
	add_action( 'load-' . $menu_hook, 'help_nfsubopt' );

	// Firewall policies menu :
	$menu_hook = add_submenu_page( 'NinjaFirewall', __('NinjaFirewall: Firewall Policies', 'nfwplus'), __('Firewall Policies', 'nfwplus'), 'manage_options',
		'nfsubpolicies', 'nf_sub_policies' );
	add_action( 'load-' . $menu_hook, 'help_nfsubpolicies' );

	// Access Control menu :
	$menu_hook = add_submenu_page( 'NinjaFirewall', __('NinjaFirewall: Access Control', 'nfwplus'), __('Access Control', 'nfwplus'), 'manage_options',
		'nfsubaccess', 'nf_sub_access' );
	add_action( 'load-' . $menu_hook, 'help_nfsubaccesscontrol' );

	// File Guard menu :
	$menu_hook = add_submenu_page( 'NinjaFirewall', __('NinjaFirewall: File Guard', 'nfwplus'),  __('File Guard', 'nfwplus'), 'manage_options',
		'nfsubfileguard', 'nf_sub_fileguard' );
	add_action( 'load-' . $menu_hook, 'help_nfsubfileguard' );

	// File Check menu :
	$menu_hook = add_submenu_page( 'NinjaFirewall', __('NinjaFirewall: File Check', 'nfwplus'), __('File Check', 'nfwplus'), 'manage_options',
		'nfsubfilecheck', 'nf_sub_filecheck' );
	add_action( 'load-' . $menu_hook, 'help_nfsubfilecheck' );

	// Anti-Malware menu :
	$menu_hook = add_submenu_page( 'NinjaFirewall', __('NinjaFirewall: Anti-Malware', 'nfwplus'), __('Anti-Malware', 'nfwplus'), 'manage_options',
		'nfsubmalwarescan', 'nf_sub_malwarescan' );
	add_action( 'load-' . $menu_hook, 'help_nfsubmalwarescan' );

	// Web Filter menu :
	$menu_hook = add_submenu_page( 'NinjaFirewall', __('NinjaFirewall: Web Filter', 'nfwplus'), __('Web Filter', 'nfwplus'), 'manage_options',
		'nfsubwebfilter', 'nf_sub_webfilter' );
	add_action( 'load-' . $menu_hook, 'help_nfsubwebfilter' );

	// Network menu (multisite only) :
	$menu_hook = add_submenu_page( 'NinjaFirewall', __('NinjaFirewall: Network', 'nfwplus'), __('Network', 'nfwplus'), 'manage_network',
		'nfsubnetwork', 'nf_sub_network' );
	add_action( 'load-' . $menu_hook, 'help_nfsubnetwork' );

	// Event Notifications menu :
	$menu_hook = add_submenu_page( 'NinjaFirewall', __('NinjaFirewall: Event Notifications', 'nfwplus'), __('Event Notifications', 'nfwplus'), 'manage_options',
		'nfsubevent', 'nf_sub_event' );
	add_action( 'load-' . $menu_hook, 'help_nfsubevent' );

	// Login protection menu :
	$menu_hook = add_submenu_page( 'NinjaFirewall', __('NinjaFirewall: Log-in Protection', 'nfwplus'), __('Login Protection', 'nfwplus'), 'manage_options',
		'nfsubloginprot', 'nf_sub_loginprot' );
	add_action( 'load-' . $menu_hook, 'help_nfsublogin' );

	// Antispam menu :
	$menu_hook = add_submenu_page( 'NinjaFirewall', __('NinjaFirewall: Antispam', 'nfwplus'), __('Antispam', 'nfwplus'), 'manage_options',
		'nfsubantispam', 'nf_sub_antispam' );
	add_action( 'load-' . $menu_hook, 'help_nfsubantispam' );

	// Firewall log menu :
	$menu_hook = add_submenu_page( 'NinjaFirewall', __('NinjaFirewall: Firewall Log', 'nfwplus'), __('Firewall Log', 'nfwplus'), 'manage_options',
		'nfsublog', 'nf_sub_log' );
	add_action( 'load-' . $menu_hook, 'help_nfsublog' );

	// Centralized logging menu :
	$menu_hook = add_submenu_page( 'NinjaFirewall', __('NinjaFirewall: Centralized Logging', 'nfwplus'), __('Centralized Logging', 'nfwplus'), 'manage_options',
		'nfsubcentlog', 'nf_sub_centlog' );
	add_action( 'load-' . $menu_hook, 'help_nfsubcentlog' );

	// Live log menu :
	$menu_hook = add_submenu_page( 'NinjaFirewall', __('NinjaFirewall: Live Log', 'nfwplus'), __('Live Log', 'nfwplus'), 'manage_options',
		'nfsublive', 'nf_sub_live' );
	add_action( 'load-' . $menu_hook, 'help_nfsublivelog' );

	// Rules Editor menu :
	$menu_hook = add_submenu_page( 'NinjaFirewall', __('NinjaFirewall: Rules Editor', 'nfwplus'), __('Rules Editor', 'nfwplus'), 'manage_options',
		'nfsubeditor', 'nf_sub_editor' );
	add_action( 'load-' . $menu_hook, 'help_nfsubeditor' );

	// Updates menu :
	$menu_hook = add_submenu_page( 'NinjaFirewall', __('NinjaFirewall: Updates', 'nfwplus'), __('Updates', 'nfwplus'), 'manage_options',
		'nfsubupdates', 'nf_sub_updates' );
	add_action( 'load-' . $menu_hook, 'help_nfsubupdates' );

	// License menu :
	$menu_hook = add_submenu_page( 'NinjaFirewall', __('NinjaFirewall: License', 'nfwplus'), __('License', 'nfwplus'), 'manage_options',
		'nfsublicense', 'nf_sub_license' );
	add_action( 'load-' . $menu_hook, 'help_nfsublicense' );

	// About menu :
	$menu_hook = add_submenu_page( 'NinjaFirewall', __('NinjaFirewall: About', 'nfwplus'), __('About...', 'nfwplus'), 'manage_options',
		'nfsubabout', 'nf_sub_about' );
}

if (! is_multisite() )  {
	add_action( 'admin_menu', 'ninjafirewall_admin_menu' );
} else {
	add_action( 'network_admin_menu', 'ninjafirewall_admin_menu' );
}

/* ================================================================== */

function nf_admin_bar_status() {

	// Display the status icon to administrators (multi-site mode only) :
	if (! current_user_can( 'manage_options' ) ) {
		return;
	}

	$nfw_options = nfw_get_option( 'nfw_options' );
	// Disable it, unless this is the superadmin :
	if ( @$nfw_options['nt_show_status'] != 1 && ! current_user_can('manage_network') ) {
		return;
	}

	// Obviously, we don't put any icon if NinjaFirewall isn't running :
	if (! defined('NF_DISABLED') ) {
		is_nfw_enabled();
	}
	if (NF_DISABLED) { return; }

	global $wp_admin_bar;
	$wp_admin_bar->add_menu( array(
		'id'    => 'nfw_ntw1',
		'title' => '<img src="' . plugins_url() . '/nfwplus/images/ninjafirewall_20.png" ' .
				'style="vertical-align:middle;margin-right:5px" />',
	) );

	// Add sub menu link for Super Admin only :
	if ( current_user_can( 'manage_network' ) ) {
		$wp_admin_bar->add_menu( array(
			'parent' => 'nfw_ntw1',
			'id'     => 'nfw_ntw2',
			'title'  => __('NinjaFirewall Settings', 'nfwplus'),
			'href'   => network_admin_url() . 'admin.php?page=NinjaFirewall',
		) );
	// else, show status only (unless error) :
	} else {
		if ( defined('NFW_STATUS') ) {
			$wp_admin_bar->add_menu( array(
				'parent' => 'nfw_ntw1',
				'id'     => 'nfw_ntw2',
				'title'  => __('NinjaFirewall is enabled', 'nfwplus'),
			) );
		}
	}
}

if ( is_multisite() )  {
	add_action('admin_bar_menu', 'nf_admin_bar_status', 95);
}

/* ================================================================== */

function nf_menu_install() {

	// Installer :

	// Block immediately if user is not allowed :
	nf_not_allowed( 'block', __LINE__ );

	require_once( plugin_dir_path(__FILE__) . 'install.php' );
}

/* ================================================================== */

function nf_sub_main() {

	// Main menu (Overview) :
	require( plugin_dir_path(__FILE__) . 'lib/nf_sub_main.php' );

}

/* ================================================================== */

function nf_sub_statistics() {

	// Stats / benchmarks menu :
	require( plugin_dir_path(__FILE__) . 'lib/nf_sub_statistics.php' );

}

/* ================================================================== */

function nf_sub_options() {	// i18n

	// Firewall Options menu :
	require( plugin_dir_path(__FILE__) . 'lib/nf_sub_options.php' );

}

/* ================================================================== */

function nf_sub_policies() {

	// Firewall Policies menu :
	require( plugin_dir_path(__FILE__) . 'lib/nf_sub_policies.php' );

}

/* ================================================================== */

function nf_sub_access(){

	// Access Control
	require( plugin_dir_path(__FILE__) . 'lib/nf_sub_access.php' );

}

/* ================================================================== */

function nf_sub_fileguard(){

	// File Guard menu :
	require( plugin_dir_path(__FILE__) . 'lib/nf_sub_fileguard.php' );

}

/* ================================================================== */

function nf_sub_filecheck() {

	// File Check menu :
	require( plugin_dir_path(__FILE__) . 'lib/nf_sub_filecheck.php' );

}

add_action('nfscanevent', 'nfscando');

function nfscando() {

	define('NFSCANDO', 1);
	nf_sub_filecheck();
}

/* ================================================================== */

function nf_sub_malwarescan() {

	// Anti-Malware menu :
	require( plugin_dir_path(__FILE__) . 'lib/nf_sub_malwarescan.php' );

}

add_action('nfmalwarescan', 'nfmalwarescando');
function nfmalwarescando( $sigs ) {

	define('NFW_SCAN_SIGS', $sigs );
	define('NFMALWARESCANDO', 1);
	nf_sub_malwarescan();

}

// Anti-Malware ajax processing:
add_action( 'wp_ajax_nfw_msajax', 'nfw_msajax_callback' );
function nfw_msajax_callback() {

	@file_put_contents( NFW_LOG_DIR . '/nfwlog/cache/malscan.log', time() . ": [AX] Entering ajax callback\n" );

	// Check nonce:
	if ( check_ajax_referer( 'nfw_msajax_javascript', 'nfw_sc_nonce', false ) && ! empty( $_POST['sigs'] ) ){
		$sigs = rtrim( $_POST['sigs'], ':' );
		wp_schedule_single_event( time() - 10, 'nfmalwarescan', array( $sigs ) );
		$doing_wp_cron = sprintf( '%.22F', microtime( true ) );
		set_transient( 'doing_cron', $doing_wp_cron );
		$cron_request = apply_filters( 'cron_request', array(
			'url'  => add_query_arg( 'doing_wp_cron', $doing_wp_cron, site_url( 'wp-cron.php' ) ),
			'key'  => $doing_wp_cron,
			'args' => array(
//				'timeout'   => 0.01,
				'blocking'  => false,
				'sslverify' => apply_filters( 'https_local_ssl_verify', false )
			)
		), $doing_wp_cron );
		@file_put_contents( NFW_LOG_DIR . '/nfwlog/cache/malscan.log', time() . ": [AX] POSTing request to " . site_url( 'wp-cron.php' ) . "\n", FILE_APPEND );

		$res = wp_remote_post( $cron_request['url'], $cron_request['args'] );

		if ( is_wp_error( $res ) ) {
			@file_put_contents( NFW_LOG_DIR . '/nfwlog/cache/malscan.log', time() . ": [AX] ERROR: ". $res->get_error_message() . "\n", FILE_APPEND );
			echo htmlspecialchars( $res->get_error_message() );
		} else {
			echo 'OK';
		}
	} else {
		@file_put_contents( NFW_LOG_DIR . '/nfwlog/cache/malscan.log', time() . ": [AX] ERROR: security nonces do not match\n", FILE_APPEND );
		// Nonces do not match:
		echo '1';
	}
	wp_die();

}

/* ================================================================== */

function nf_sub_webfilter() {

	// Web Filter :
	require( plugin_dir_path(__FILE__) . 'lib/nf_sub_webfilter.php' );

}

/* ================================================================== */

function nf_sub_network() {

	// Network menu (multi-site only) :
	require( plugin_dir_path(__FILE__) . 'lib/nf_sub_network.php' );

}

/* ================================================================== */

function nf_sub_event() {

	// Event Notifications menu :
	require( plugin_dir_path(__FILE__) . 'lib/nf_sub_event.php' );

}

add_action('init', 'nf_check_dbdata', 1);

// Daily report cronjob :
add_action('nfdailyreport', 'nfdailyreportdo');

function nfdailyreportdo() {
	define('NFREPORTDO', 1);
	nf_sub_event();
}

/* ================================================================== */

function nf_sub_log() {

	// Firewall Log menu :
	require( plugin_dir_path(__FILE__) . 'lib/nf_sub_log.php' );

}
/* ================================================================== */

function nf_sub_centlog() {

	// Firewall Log menu :
	require( plugin_dir_path(__FILE__) . 'lib/nf_sub_centlog.php' );

}

/* ================================================================== */

function nf_sub_live() {

	// Firewall Log menu :
	require( plugin_dir_path(__FILE__) . 'lib/nf_sub_livelog.php' );

}

/* ================================================================== */

function nf_sub_loginprot() {

	// WordPress login form protection :
	require( plugin_dir_path(__FILE__) . 'lib/nf_sub_loginprot.php' );

}

/* ================================================================== */

// Antispam :
require( plugin_dir_path(__FILE__) . 'lib/nf_sub_antispam.php' );

/* ================================================================== */

function nfw_log2($loginfo, $logdata, $loglevel, $ruleid) {

	// Write incident to the firewall log :
	require( plugin_dir_path(__FILE__) . 'lib/nfw_log.php' );

}

/* ================================================================== */

function nf_sub_editor() {

	// Rules Editor menu :
	require( plugin_dir_path(__FILE__) . 'lib/nf_sub_editor.php' );

}

/* ================================================================== */

function nf_sub_updates() {

	// Updates

	require( plugin_dir_path(__FILE__) . 'lib/nf_sub_updates.php');

}

add_action('nfsecupdates', 'nfupdatesdo');

function nfupdatesdo() {
	define('NFUPDATESDO', 1);
	nf_sub_updates();
}

/* ================================================================== */

function nf_sub_license() {

	// License menu :
	require( plugin_dir_path(__FILE__) . 'lib/nf_sub_license.php' );

}

/* ================================================================== */

function nf_sub_about() {

	// About menu :
	require( plugin_dir_path(__FILE__) . 'lib/nf_sub_about.php' );

}

/* ================================================================== */

function ninjafirewall_settings_link( $links ) {

	if ( is_multisite() ) {	$net = 'network/'; } else { $net = '';	}

	$links[] = '<a href="'. get_admin_url(null, $net .'admin.php?page=NinjaFirewall') .'">'. __('Settings', 'nfwplus') .'</a>';
	$links[] = '<a href="https://nintechnet.com/referral/" target="_blank">'. __('Referral Program', 'nfwplus'). '</a>';
	unset($links['edit']);
   return $links;

}

if ( is_multisite() ) {
	add_filter( 'network_admin_plugin_action_links_' . plugin_basename(__FILE__), 'ninjafirewall_settings_link' );
} else {
	add_filter( 'plugin_action_links_' . plugin_basename(__FILE__), 'ninjafirewall_settings_link' );
}

/* ================================================================== */

function ninjafirewall_all_plugins( $plugins ) {

	// Cf https://blog.nintechnet.com/restricting-access-to-ninjafirewall-wp-edition-settings/
	if ( nf_not_allowed( 0, __LINE__ ) ) {

		if ( isset( $plugins['nfwplus/nfwplus.php'] ) ) {
			unset( $plugins['nfwplus/nfwplus.php'] );
		}
	}
	return $plugins;
}

add_filter( 'all_plugins', 'ninjafirewall_all_plugins' );

/* ================================================================== */

function nfw_dashboard_widgets() {

	// Add dashboard widgets
	require( plugin_dir_path(__FILE__) . 'lib/nfw_dashboard_widgets.php' );

}

if ( is_multisite() ) {
	add_action( 'wp_network_dashboard_setup', 'nfw_dashboard_widgets' );
} else {
	add_action( 'wp_dashboard_setup', 'nfw_dashboard_widgets' );
}

/* ================================================================== */

function nfw_shm_check() {

	if (! function_exists('shmop_open')) {
		return;
	}

	$nfw_options = nfw_get_option( 'nfw_options' );

	// Do nothing if shared memory or NF are disabled :
	if ( empty($nfw_options['shmop']) || empty($nfw_options['enabled']) ) {
		return;
	}

	$nfw_rules = nfw_get_option( 'nfw_rules' );

	$shm_update = 1;

	$nf_shm_key = ftok( dirname( __DIR__ ), 'N' );
	$nf_shm_data =	serialize($nfw_options) . $nf_shm_key . serialize($nfw_rules);

	if ( $shm_id = @shmop_open($nf_shm_key, "w", 0, 0) ) {
		// Compare checksum :
		if ( md5( shmop_read($shm_id, 0, shmop_size($shm_id))) != md5($nf_shm_data) ) {
			// Delete the current segment, it is outdated :
			shmop_delete($shm_id);
		} else {
			// Keep it :
			$shm_update = 0;
		}
		shmop_close($shm_id);
	}

	if ($shm_update ) {
		// Create a new one :
		if ( $shm_id = @shmop_open($nf_shm_key, "c", 0600, strlen($nf_shm_data)) ) {
			if (! $shm_nb = shmop_write( $shm_id, serialize($nfw_options) . $nf_shm_key . serialize($nfw_rules) , 0) ) {
				// Delete it if error :
				shmop_delete($shm_id);
			}
			// Ensure we wrote the right amount of bytes :
			if ( $shm_nb != strlen($nf_shm_data) ) {
				// Don't keep it :
				shmop_delete($shm_id);
			}
			// Close it :
			shmop_close($shm_id);
		}
	}
}

/* ================================================================== */

function nfw_shm_delete($nf_shm_key) {

	if (! function_exists('shmop_open')) {
		return;
	}

	if ( empty( $nf_shm_key ) ) {
		$nf_shm_key = ftok( dirname( __DIR__ ), 'N' );
	}
	if ( $shm_id = @shmop_open($nf_shm_key, "w", 0, 0) ) {
		shmop_delete($shm_id);
		shmop_close($shm_id);
		return 0;
	} else {
		// The block does not exist :
		return 1;
	}
}

/* ================================================================== */

add_filter('pre_set_site_transient_update_plugins', 'nfw_check_update');

function nfw_check_update( $transient ) {
	return;
	// Check update/version :

	if ( empty( $transient->checked['nfwplus/nfwplus.php']) ) {
		return $transient;
	}

	$args = array(
		'slug' => 'nfwplus',
		'version' => $transient->checked['nfwplus/nfwplus.php'],
	);

	if ( is_multisite() ) {
		$nfw_site_url = rtrim( strtolower( network_site_url('','http')), '/' );
	} else {
		$nfw_site_url = rtrim( strtolower(site_url('','http')), '/' );
	}

	global $wp_version;
	$nfw_options = nfw_get_option( 'nfw_options' );

	$request_string = array(
			'body' => array(
				'action' => 'version',
				'request'=> serialize( $args ),
				'ver'		=> NFW_ENGINE_VERSION,
				'host'	=> @strtolower( $_SERVER['HTTP_HOST'] ),
				'name'	=> @strtolower( $_SERVER['SERVER_NAME'] ),
				'lic' 	=> $nfw_options['lic']
			),
			'user-agent' => 'WordPress/' . $wp_version . '; ' . $nfw_site_url . '; ' . NFW_ENGINE_VERSION
		);

	if ( defined( 'NFW_DONT_USE_SSL' ) ) {
		$proto = "http";
	} else {
		$proto = "https";
	}
	$res = wp_remote_post( "{$proto}://wordpress.nintechnet.com/index.php", $request_string);

	if (! is_wp_error($res) && $res['response']['code'] == 200 ) {
		$response = unserialize($res['body']);
	}
	if ( ! empty($response) && is_object($response) ) {
		$transient->response['nfwplus/nfwplus.php'] = $response;
	}
	return $transient;
}

/* ================================================================== */

add_filter('plugins_api', 'nfw_check_plugin_info', 10, 3);

function nfw_check_plugin_info($def, $action, $args) {

	// Get plugin information :

	if (! isset($args->slug) || $args->slug != 'nfwplus' ) {
		return false;
	}
	$plugin_info = get_site_transient('update_plugins');
	$current_version = $plugin_info->checked['nfwplus/nfwplus.php'];
	$args->version = $current_version;

	if ( is_multisite() ) {
		$nfw_site_url = rtrim( strtolower( network_site_url('','http')), '/' );
	} else {
		$nfw_site_url = rtrim( strtolower(site_url('','http')), '/' );
	}

	global $wp_version;
	$nfw_options = nfw_get_option( 'nfw_options' );

	$request_string = array(
		'body' => array(
			'action' => $action,
			'request'=> serialize($args),
			'host'	=> @strtolower( $_SERVER['HTTP_HOST'] ),
			'name'	=> @strtolower( $_SERVER['SERVER_NAME'] ),
			'lic' 	=> $nfw_options['lic'],
			'ver'		=> NFW_ENGINE_VERSION
		),
		'user-agent' => 'WordPress/' . $wp_version . '; ' . $nfw_site_url . '; ' . NFW_ENGINE_VERSION
	);

	if ( defined( 'NFW_DONT_USE_SSL' ) ) {
		$proto = "http";
	} else {
		$proto = "https";
	}
	$res = wp_remote_post( "{$proto}://wordpress.nintechnet.com/index.php", $request_string);

	if (! is_wp_error($res) && $res['response']['code'] == 200 ) {
		return unserialize( $res['body'] );
	}
	return false;
}

/* ================================================================== */

function nfw_get_option( $option ) {

	if ( is_multisite() ) {
		return get_site_option($option);
	} else {
		return get_option($option);
	}
}

/* ================================================================== */

function nfw_update_option( $option, $new_value ) {

	// We need to update both, because the firewall part of NinjaFirewall
	// does not know what a multisite is and therefore will always use
	// the wp_options table:
	update_option( $option, $new_value );
	if ( is_multisite() ) {
		update_site_option( $option, $new_value );
	}
	return;

}
/* ================================================================== */

function nfw_delete_option( $option ) {

	delete_option( $option );
	if ( is_multisite() ) {
		delete_site_option( $option );
	}
	return;

}

/* ================================================================== */

function nf_not_allowed($block, $line = 0) {

	if ( is_multisite() ) {
		if ( current_user_can('manage_network') ) {
			return false;
		}
	} else {
		if ( current_user_can('manage_options') &&
		     current_user_can('unfiltered_html') ) {
			// Check if that admin is allowed to use NinjaFirewall
			// (see NFW_ALLOWED_ADMIN at http://nin.link/nfwaa ):
			if ( defined('NFW_ALLOWED_ADMIN') ) {
				$current_user = wp_get_current_user();
				$admins = explode(',', NFW_ALLOWED_ADMIN);
				foreach ($admins as $admin) {
					if ( trim($admin) == $current_user->user_login ) {
						return false;
					}
				}
			} else {
				return false;
			}
		}
	}

	if ($block) {
		die( '<br /><br /><br /><div class="error notice is-dismissible"><p>' .
			sprintf( __('You are not allowed to perform this task (%s).'), $line) .
			'</p></div>' );
	}
	return true;
}

/* ================================================================== */
// EOF //
